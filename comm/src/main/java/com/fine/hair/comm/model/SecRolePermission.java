package com.fine.hair.comm.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>${Description}</p>
 * @author mouseyCat
 * @date 2020/10/26 21:39
 */

/**
 * 角色权限关系表
 */
@ApiModel(value = "角色资源表")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "sec_role_permission")
public class SecRolePermission implements Serializable {
    /**
     * 角色主键
     */
    @TableId(value = "role_id", type = IdType.INPUT)
    @ApiModelProperty(value = "角色主键")
    private Long roleId;

    /**
     * 权限主键
     */
    @TableId(value = "permission_id", type = IdType.INPUT)
    @ApiModelProperty(value = "权限主键")
    private Long permissionId;

    private static final long serialVersionUID = 1L;

    public static final String COL_ROLE_ID = "role_id";

    public static final String COL_PERMISSION_ID = "permission_id";
}
