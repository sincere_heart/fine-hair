package com.fine.hair.comm.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/11/7 1:51
 */
@Data
@Builder
@ApiModel("当日业绩vo对象")
public class TodayPerformanceVo {
    @ApiModelProperty("当日接单客户")
    private Long orderCount;
    @ApiModelProperty("当日提成")
    private BigDecimal pm;
    @ApiModelProperty("奖励金额")
    private BigDecimal rewards;
    @ApiModelProperty("惩罚金额")
    private BigDecimal punishment;
    @ApiModelProperty("当日店均")
    private BigDecimal branchAllMoney;
    @ApiModelProperty("人均")
    private BigDecimal personAvg;
    @ApiModelProperty("个人当日收入")
    private BigDecimal selfIncome;
}
