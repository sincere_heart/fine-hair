package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/10/24 15:55
 */
@Data
@ApiModel("修改订单信息")
public class AlterReservationDto {
    @ApiModelProperty("项目id")
    private String itemId;
    @ApiModelProperty("订单id，修改时需要传，新增时不传")
    private String orderId;
    @ApiModelProperty("用户id")
    private String userId;
    @ApiModelProperty("技师id")
    private String technicianId;
    @ApiModelProperty("服务开始时间")
    private String executeTime;
    @ApiModelProperty("用户备注")
    private String remark;
}
