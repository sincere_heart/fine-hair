package com.fine.hair.comm.utils;

import java.util.HashMap;

/**
 * <p>
 *     数据类型池
 * </p>
 *
 */
public class TypePool {

    /**
     * 例子：
     *
     * 站点类型
     *
     * 使用：
     * List<MapType> typeList = new ArrayList<>();
     * TypePool.STATION_TYPE.forEach((key, value) -> typeList.add(new MapType(key, value)));
     * return typeList;
     */
    public static final HashMap<Integer, String> STATION_TYPE = new HashMap<Integer, String>() {
        {
            put(1, "公共");
            put(50, "个人");
            put(100, "公交(专用)");
            put(101, "环卫(专用)");
            put(102, "物流(专用)");
            put(103, "出租车(专用)");
            put(255, "其他");
        }
    };

    /**
     * 订单状态
     */
    public static final HashMap<Integer, String> ORDER_STATUS = new HashMap<Integer, String>() {
        {
            put(1, "未开始");
            put(2, "已完成");
            put(3, "已取消");
        }
    };

    /**
     * 审核状态
     */
    public static final HashMap<Integer, String> APPLY_STATUS = new HashMap<Integer, String>() {
        {
            put(1, "同意");
            put(2, "拒绝");
        }
    };

    /**
     * 消费类型
     */
    public static final HashMap<Integer, String> CONSUME_TYPE = new HashMap<Integer, String>() {
        {
            put(0, "消费");
            put(1, "充值");
        }
    };

    /**
     * 奖惩类型
     */
    public static final HashMap<Integer, String> REWARDS_PUNISHMENT_TYPE = new HashMap<Integer, String>() {
        {
            put(0, "奖励");
            put(1, "惩罚");
        }
    };

}
