package com.fine.hair.comm.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/11/9 23:32
 */
@Data
@ApiModel("")
public class StaffSearchClientOrderVo {
    @ApiModelProperty("订单id")
    private Long orderId;
    @ApiModelProperty("项目图片")
    private String itemImage;
    @ApiModelProperty("客户名称")
    private String clientName;
    @ApiModelProperty("客户手机号")
    private String clientPhone;
    @ApiModelProperty("门店名称")
    private String branchTitle;
    @ApiModelProperty("项目名称")
    private String itemTitle;
    @ApiModelProperty("服务状态 1正在服务 2未服务 3服务完成")
    private String serviceType;
}
