package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/11/9 23:49
 */
@Data
@ApiModel("编辑备注dto请求体")
public class EditRemarkDto {
    @ApiModelProperty("备注")
    private String remark;
    @ApiModelProperty("订单号")
    private String orderId;
}
