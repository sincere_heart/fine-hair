package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.List;

/**
 * @author liheng
 * @ClassName GdxhtjDTO
 * @Description
 * @date 2021-03-19 15:28
 */
@ApiModel("统计消耗dto")
@Data
public class GdxhtjDTO extends BasePage{
    private List<Long> branchIds;
    private String nickName="";
}
