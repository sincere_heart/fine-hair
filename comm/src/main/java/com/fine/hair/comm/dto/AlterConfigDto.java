package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/10/29 23:12
 */
@Data
@ApiModel("修改配置dto实体")
public class AlterConfigDto {
    @ApiModelProperty(value="订单自动确认时间,单位分钟")
    private Long orderAutoConfirmTime;
    @ApiModelProperty(value = "配置的id,可以不传")
    private Long id = 1L;
    @ApiModelProperty(value = "配置1")
    private Integer configFirst;
    @ApiModelProperty(value = "配置2")
    private Integer configSecond;
    @ApiModelProperty(value = "配置3")
    private Integer configThird;
    @ApiModelProperty(value = "配置4")
    private Integer configFourth;
}
