package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/10/24 17:47
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel("评论表")
public class EvaluateListDto extends BasePage {
    @ApiModelProperty("技师id")
    private String technicianId;
}
