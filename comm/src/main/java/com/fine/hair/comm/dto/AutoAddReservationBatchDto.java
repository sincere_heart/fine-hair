package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/1/10 12:29
 */
@Data
@ApiModel("自动批量新增订单")
public class AutoAddReservationBatchDto {
    @ApiModelProperty(value = "采用的配置 1 2 3 4 在 sysConfig 里拿 ",required = true)
    private Integer mode;
    @ApiModelProperty(value = "从哪天的几点开始 格式 yyyy-MM-dd HH:mm:ss",required = true)
    private String startTime;
    @ApiModelProperty(value = "用户id",required = true)
    private String userId;
    @ApiModelProperty(value = "门店id", required = true)
    private String branchId;
    @ApiModelProperty(value = "技师id",required = true)
    private String technicianId;
    @ApiModelProperty(value = "项目id",required = true)
    private String itemId;

    @ApiModelProperty("疗程持续的天数")
    @Min(value = 1,message = "不能小于1")
    private Integer date;

    @ApiModelProperty("周期，每个订单之间间隔的天数")
    private Integer cycle;
}
