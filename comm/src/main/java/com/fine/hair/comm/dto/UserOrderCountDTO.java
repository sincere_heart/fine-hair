package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/3/4 20:55
 */
@Data
@ApiModel("")
public class UserOrderCountDTO {
    private String date;
    private String userId;
}
