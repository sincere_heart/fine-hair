package com.fine.hair.comm.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/2/6 0:09
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel("")
public class InventoryPageDto extends BasePage{
    @JsonIgnore
    private String branchId;
}
