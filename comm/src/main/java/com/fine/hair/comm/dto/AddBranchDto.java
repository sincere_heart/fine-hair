package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/10/24 18:18
 */
@Data
@ApiModel("新增门店dto请求体")
public class AddBranchDto {
    @ApiModelProperty("门店id，新增时不传，编辑时要传")
    private String id;
    @ApiModelProperty("门店名称")
    private String title;
    @ApiModelProperty("门店图片，多图片则以逗号隔开")
    private String images;
    @ApiModelProperty("门店地址")
    private String address;
    @ApiModelProperty("门店热线")
    private String tel;
    @ApiModelProperty("营业时间")
    private String businessTime;
    @ApiModelProperty("经度")
    private BigDecimal lng;
    @ApiModelProperty("维度")
    private BigDecimal lat;
}
