package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/10/25 20:12
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel("员工订单信息")
public class StaffOrderInfoDto extends BasePage {
    @ApiModelProperty("员工id")
    private String staffId;
    @ApiModelProperty("开始时间")
    private String startTime;
    @ApiModelProperty("结束时间")
    private String endTime;
    @ApiModelProperty("订单状态 0全部 1未开始 2已完成 3已取消")
    private Integer orderStatus;

}
