package com.fine.hair.comm.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/2/23 21:42
 */
@Data
public class IncomeYearVo {
    @ApiModelProperty("")
    private String time;
    @ApiModelProperty("")
    private BigDecimal money;
}
