package com.fine.hair.comm.pojo.wx.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@EnableConfigurationProperties
@ConfigurationProperties("wx.mp")
public class WxMpProperty {

    private String appId;
    private String appSecret;
    private String token;
    private String aesKey;
}
