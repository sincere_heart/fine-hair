package com.fine.hair.comm.model;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>${Description}</p>
 * @author mouseyCat
 * @date 2020/10/26 21:39
 */

/**
 * 轮播图表
 */
@ApiModel(value = "轮播图表")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "banner")
public class Banner implements Serializable {
    /**
     * id
     */
    @TableId(value = "id", type = IdType.INPUT)
    @ApiModelProperty(value = "id")
    private Long id;

    /**
     * 图片链接
     */
    @TableField(value = "url")
    @ApiModelProperty(value = "图片链接")
    private String url;

    /**
     * 优先级，1>2>3>4
     */
    @TableField(value = "priority")
    @ApiModelProperty(value = "优先级，1>2>3>4")
    private Long priority;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    /**
     * 最近一次更新时间
     */
    @TableField(value = "last_update_time")
    @ApiModelProperty(value = "最近一次更新时间")
    private Date lastUpdateTime;

    /**
     * 是否删除 0否 1是
     */
    @TableField(value = "`status`")
    @TableLogic
    @ApiModelProperty(value = "是否删除 0否 1是")
    private Integer status;

    private static final long serialVersionUID = 1L;

    public static final String COL_ID = "id";

    public static final String COL_URL = "url";

    public static final String COL_PRIORITY = "priority";

    public static final String COL_CREATE_TIME = "create_time";

    public static final String COL_LAST_UPDATE_TIME = "last_update_time";

    public static final String COL_STATUS = "status";
}
