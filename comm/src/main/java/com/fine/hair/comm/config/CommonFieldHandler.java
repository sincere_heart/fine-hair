package com.fine.hair.comm.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * <p>
 * 通用字段填充
 * </p>
 *
 * @description: 通用字段填充
 * @author: june
 */

@Slf4j
@Component
public class CommonFieldHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        log.info("start insert fill ....");
        // 数据库字段名需要跟这个一致
        this.setFieldValByName("createTime", new Date(), metaObject);
        // 数据库字段名需要跟这个一致
        this.setFieldValByName("lastUpdateTime", new Date(), metaObject);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        log.info("start update fill ....");
        // 数据库字段名需要跟这个一致
        this.setFieldValByName("lastUpdateTime", new Date(), metaObject);
    }
}
