package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * @author liheng
 * @ClassName DateDto
 * @Description
 * @date 2021-04-19 10:41
 */
@ApiModel("时间范围查询")
@Data
public class DateDto {
    private String start;
    private String end;
}
