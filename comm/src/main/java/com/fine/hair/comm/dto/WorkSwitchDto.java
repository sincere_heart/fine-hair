package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/2/7 13:41
 */
@Data
@ApiModel("")
public class WorkSwitchDto {
    @ApiModelProperty("用户id")
    @NotNull(message = "用户id不得为空")
    private String userId;
    @ApiModelProperty("1,接单 2,暂停")
    @NotNull(message = "上下班状态不得为空")
    private Integer status;
}
