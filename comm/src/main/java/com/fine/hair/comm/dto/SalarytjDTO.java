package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.List;

/**
 * @author liheng
 * @ClassName SalarytjDTO
 * @Description
 * @date 2021-03-19 14:06
 */
@ApiModel("工资统计dto")
@Data
public class SalarytjDTO extends BasePage{
    private String nickName;
    private List<Long> branchIds;
}
