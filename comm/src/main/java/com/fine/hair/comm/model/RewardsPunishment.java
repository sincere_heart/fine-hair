package com.fine.hair.comm.model;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>${Description}</p>
 * @author mouseyCat
 * @date 2020/10/26 21:39
 */

/**
 * 奖惩表
 */
@ApiModel(value = "奖惩表")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "rewards_punishment")
public class RewardsPunishment implements Serializable {
    /**
     * id
     */
    @TableId(value = "id", type = IdType.INPUT)
    @ApiModelProperty(value = "id")
    private Long id;

    @TableId(value = "branch_id")
    @ApiModelProperty(value = "门店id")
    private String branchId;
    /**
     * 用户id
     */
    @TableField(value = "user_id")
    @ApiModelProperty(value = "用户id")
    private String userId;

    /**
     * 奖惩类型 0奖励 1惩罚
     */
    @TableField(value = "`type`")
    @ApiModelProperty(value = "奖惩类型 0奖励 1惩罚")
    private Integer type;

    /**
     * 金额
     */
    @TableField(value = "amount")
    @ApiModelProperty(value = "金额")
    private BigDecimal amount;

    /**
     * 充值金额
     */
    @TableField(value = "charge_amount")
    @ApiModelProperty(value = "充值金额")
    private BigDecimal chargeAmount;

    /**
     * 备注
     */
    @TableField(value = "remark")
    @ApiModelProperty(value = "备注")
    private String remark;

    /**
     * 录入信息的管理员id
     */
    @TableField(value = "operator_id")
    @ApiModelProperty(value = "录入信息的管理员id")
    private Long operatorId;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    /**
     * 最近一次更新时间
     */
    @TableField(value = "last_update_time")
    @ApiModelProperty(value = "最近一次更新时间")
    private Date lastUpdateTime;

    /**
     * 是否删除 0否 1是
     */
    @TableField(value = "`status`")
    @TableLogic
    @ApiModelProperty(value = "是否删除 0否 1是")
    private Integer status;

    private static final long serialVersionUID = 1L;

    public static final String COL_ID = "id";

    public static final String COL_USER_ID = "user_id";

    public static final String COL_TYPE = "type";

    public static final String COL_AMOUNT = "amount";

    public static final String COL_REMARK = "remark";

    public static final String COL_OPERATOR_ID = "operator_id";

    public static final String COL_CREATE_TIME = "create_time";

    public static final String COL_LAST_UPDATE_TIME = "last_update_time";

    public static final String COL_STATUS = "status";
}
