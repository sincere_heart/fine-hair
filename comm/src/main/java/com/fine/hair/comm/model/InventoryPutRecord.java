package com.fine.hair.comm.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>${Description}</p>
 * @author mouseyCat
 * @date 2021/2/5 23:58
 */

/**
    * 库存入库记录表
    */
@ApiModel(value="com-lingser-model-InventoryPutRecord")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "inventory_put_record")
public class InventoryPutRecord implements Serializable {
    /**
     * id
     */
    @TableId(value = "id", type = IdType.INPUT)
    @ApiModelProperty(value="id")
    private Long id;

    @TableId(value = "money")
    private BigDecimal money;

    /**
     * 本次入库数量
     */
    @TableField(value = "`count`")
    @ApiModelProperty(value="本次入库数量")
    private Integer count;

    /**
     * 本次入库的门店id
     */
    @TableField(value = "branch_id")
    @ApiModelProperty(value="本次入库的门店id")
    private Long branchId;

    /**
     * 本次入库的商品id
     */
    @TableField(value = "inventory_id")
    @ApiModelProperty(value="本次入库的商品id")
    private Long inventoryId;

    /**
     * 本次入库的备注
     */
    @TableField(value = "remark")
    @ApiModelProperty(value="本次入库的备注")
    private String remark;

    /**
     * 本次入库的总价格
     */
    @TableField(value = "total_money")
    @ApiModelProperty(value="本次入库的总价格")
    private BigDecimal totalMoney;

    /**
     * 领用人用户名
     */
    @TableField(value = "user_name")
    @ApiModelProperty(value="领用人用户名")
    private String userName;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    @ApiModelProperty(value="创建时间")
    private Date createTime;

    /**
     * 最近一次更新时间
     */
    @TableField(value = "last_update_time")
    @ApiModelProperty(value="最近一次更新时间")
    private Date lastUpdateTime;

    /**
     * 是否删除 0否 1是
     */
    @TableField(value = "`status`")
    @ApiModelProperty(value="是否删除 0否 1是")
    private Integer status;

    private static final long serialVersionUID = 1L;

    public static final String COL_ID = "id";

    public static final String COL_COUNT = "count";

    public static final String COL_BRANCH_ID = "branch_id";

    public static final String COL_INVENTORY_ID = "inventory_id";

    public static final String COL_REMARK = "remark";

    public static final String COL_TOTAL_MONEY = "total_money";

    public static final String COL_USER_NAME = "user_name";

    public static final String COL_CREATE_TIME = "create_time";

    public static final String COL_LAST_UPDATE_TIME = "last_update_time";

    public static final String COL_STATUS = "status";
}
