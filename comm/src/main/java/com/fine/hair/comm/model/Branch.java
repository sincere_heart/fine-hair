package com.fine.hair.comm.model;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>${Description}</p>
 * @author mouseyCat
 * @date 2020/10/26 21:39
 */

/**
 * 门店表
 */
@ApiModel(value = "门店表")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "branch")
public class Branch implements Serializable {
    /**
     * id
     */
    @TableId(value = "id", type = IdType.INPUT)
    @ApiModelProperty(value = "id")
    private Long id;

    /**
     * 门店名称
     */
    @TableField(value = "title")
    @ApiModelProperty(value = "门店名称")
    private String title;

    /**
     * 门店图片,多图片则逗号隔开
     */
    @TableField(value = "images")
    @ApiModelProperty(value = "门店图片,多图片则逗号隔开")
    private String images;

    /**
     * 门店地址
     */
    @TableField(value = "address")
    @ApiModelProperty(value = "门店地址")
    private String address;

    /**
     * 门店热线
     */
    @TableField(value = "tel")
    @ApiModelProperty(value = "门店热线")
    private String tel;

    /**
     * 营业时间
     */
    @TableField(value = "business_time")
    @ApiModelProperty(value = "营业时间")
    private String businessTime;

    /**
     * 经度
     */
    @TableField(value = "lng")
    @ApiModelProperty(value = "经度")
    private Double lng;

    /**
     * 维度
     */
    @TableField(value = "lat")
    @ApiModelProperty(value = "维度")
    private Double lat;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    /**
     * 最近一次更新时间
     */
    @TableField(value = "last_update_time")
    @ApiModelProperty(value = "最近一次更新时间")
    private Date lastUpdateTime;

    /**
     * 是否删除 0否 1是
     */
    @TableField(value = "`status`")
    @TableLogic
    @ApiModelProperty(value = "是否删除 0否 1是")
    private Integer status;

    private static final long serialVersionUID = 1L;

    public static final String COL_ID = "id";

    public static final String COL_TITLE = "title";

    public static final String COL_IMAGES = "images";

    public static final String COL_ADDRESS = "address";

    public static final String COL_TEL = "tel";

    public static final String COL_BUSINESS_TIME = "business_time";

    public static final String COL_LNG = "lng";

    public static final String COL_LAT = "lat";

    public static final String COL_CREATE_TIME = "create_time";

    public static final String COL_LAST_UPDATE_TIME = "last_update_time";

    public static final String COL_STATUS = "status";
}
