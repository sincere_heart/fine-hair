package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/10/24 18:05
 */
@Data
@ApiModel("")
public class BranchListDto extends BasePage {
    @ApiModelProperty("门店名称")
    private String branchName;
}
