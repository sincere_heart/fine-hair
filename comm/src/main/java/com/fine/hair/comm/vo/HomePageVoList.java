package com.fine.hair.comm.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/2/20 18:06
 */
@Data
@ApiModel("首页")
public class HomePageVoList {
    @ApiModelProperty("昨日")
    private BigDecimal yesterday;
    @ApiModelProperty("今日")
    private BigDecimal today;
    @ApiModelProperty("上月")
    private BigDecimal lastMonth;
    @ApiModelProperty("今年")
    private BigDecimal thisYear;
}
