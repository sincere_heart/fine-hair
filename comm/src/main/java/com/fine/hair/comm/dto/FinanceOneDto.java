package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/2/21 21:40
 */
@Data
@ApiModel("门店员工收入表")
public class FinanceOneDto {
    @ApiModelProperty("门店id，有则传")
    private String branchId;
    @ApiModelProperty("时间 格式 yyyy-MM")
    private String date;
}
