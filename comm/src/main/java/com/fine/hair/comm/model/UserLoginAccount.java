package com.fine.hair.comm.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>${Description}</p>
 * @author mouseyCat
 * @date 2020/10/26 21:39
 */

/**
 * 用户登陆表
 */
@ApiModel(value = "用户登陆表")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "user_login_account")
public class UserLoginAccount implements Serializable {
    public static final String COL_ACC_TYPE = "acc_type";
    /**
     * user id
     */
    @TableField(value = "user_id")
    @ApiModelProperty(value = "user id")
    private String userId;

    /**
     * 帐号类型. 0, 未知; 1, 微信小程序openId 2, 微信unionId; 3, 微信公众号openId;
     */
    @TableField(value = "wx_account_type")
    @ApiModelProperty(value = "帐号类型. 0, 未知; 1, 微信小程序openId 2, 微信unionId; 3, 微信公众号openId;")
    private Integer wxAccountType;

    /**
     * 账号
     */
    @TableField(value = "wx_account")
    @ApiModelProperty(value = "账号")
    private String wxAccount;

    /**
     * 小程序sessionKey
     */
    @TableField(value = "session_key")
    @ApiModelProperty(value = "小程序sessionKey")
    private String sessionKey;

    /**
     * 性别
     */
    @TableField(value = "sex")
    @ApiModelProperty(value = "性别")
    private String sex;

    /**
     * 地区
     */
    @TableField(value = "address")
    @ApiModelProperty(value = "地区")
    private String address;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    /**
     * 最近一次更新时间
     */
    @TableField(value = "last_update_time")
    @ApiModelProperty(value = "最近一次更新时间")
    private Date lastUpdateTime;

    /**
     * 是否删除 0否 1是
     */
    @TableField(value = "`status`")
    @TableLogic
    @ApiModelProperty(value = "是否删除 0否 1是")
    private Integer status;

    private static final long serialVersionUID = 1L;

    public static final String COL_USER_ID = "user_id";

    public static final String COL_WX_ACCOUNT_TYPE = "wx_account_type";

    public static final String COL_WX_ACCOUNT = "wx_account";

    public static final String COL_SESSION_KEY = "session_key";

    public static final String COL_SEX = "sex";

    public static final String COL_ADDRESS = "address";

    public static final String COL_CREATE_TIME = "create_time";

    public static final String COL_LAST_UPDATE_TIME = "last_update_time";

    public static final String COL_STATUS = "status";
}
