package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/2/20 14:32
 */
@Data
@ApiModel("首页")
public class HomePageDto {
    @ApiModelProperty("门店名称，搜索时传")
    private String branchName;
    @ApiModelProperty("门店id，当前为门店管理员时传")
    private String branchId;
}
