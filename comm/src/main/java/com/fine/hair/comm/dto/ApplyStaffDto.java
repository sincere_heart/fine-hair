package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/10/24 21:06
 */
@Data
@ApiModel("申请成为员工")
public class ApplyStaffDto {
    @ApiModelProperty("用户id")
    private String userId;
    @ApiModelProperty("手机号")
    private String phoneNumber;
    @ApiModelProperty("门店id")
    private String branchId;
    @ApiModelProperty("用户名字")
    private String nickName;
}
