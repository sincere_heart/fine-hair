package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/2/20 12:04
 */
@Data
@ApiModel("修改员工信息dto")
public class ModifyStaffInfoDto {
    @NotNull(message = "员工id不得为空")
    @ApiModelProperty("员工id")
    private String userId;
    @NotNull(message = "名称不得为空")
    @ApiModelProperty("名称")
    private String name;
    @NotNull(message = "手机号不得为空")
    @ApiModelProperty("手机号")
    private String phone;
    @NotNull(message = "性别不得为空")
    @ApiModelProperty("性别")
    private String sex;
    @NotNull(message = "生日不得为空")
    @ApiModelProperty("生日，格式 yyyy-MM-dd")
    private String birth;
}
