package com.fine.hair.comm.model;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>${Description}</p>
 * @author mouseyCat
 * @date 2020/10/26 21:39
 */

/**
 * 项目表
 */
@ApiModel(value = "项目表")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "item")
public class Item implements Serializable {
    /**
     * id
     */
    @TableId(value = "id", type = IdType.INPUT)
    @ApiModelProperty(value = "id")
    private Long id;

    @ApiModelProperty("门店id")
    @TableField(value = "branch_id")
    private Long branchId;

    @ApiModelProperty("库存")
    @TableField(value = "stock")
    private Integer stock=0;

    @ApiModelProperty(value = "服务类型 1护发服务 2养发服务 3发质服务")
    @TableId(value = "type")
    private Integer type;

    /**
     * 项目图片
     */
    @TableField(value = "image")
    @ApiModelProperty(value = "项目图片")
    private String image;

    /**
     * 项目名称
     */
    @TableField(value = "title")
    @ApiModelProperty(value = "项目名称")
    private String title;

    /**
     * 项目价格
     */
    @TableField(value = "price")
    @ApiModelProperty(value = "项目价格")
    private BigDecimal price;

    /**
     * 项目描述
     */
    @TableField(value = "content")
    @ApiModelProperty(value = "项目描述")
    private String content;

    /**
     * 项目执行时间
     */
    @TableField(value = "`time`")
    @ApiModelProperty(value = "项目执行所需时间 单位分钟")
    private Integer time;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    /**
     * 最近一次更新时间
     */
    @TableField(value = "last_update_time")
    @ApiModelProperty(value = "最近一次更新时间")
    private Date lastUpdateTime;

    /**
     * 是否删除 0否 1是
     */
    @TableField(value = "`status`")
    @TableLogic
    @ApiModelProperty(value = "是否删除 0否 1是")
    private Integer status;

    private static final long serialVersionUID = 1L;

    public static final String COL_ID = "id";

    public static final String COL_IMAGE = "image";

    public static final String COL_TITLE = "title";

    public static final String COL_PRICE = "price";

    public static final String COL_CONTENT = "content";

    public static final String COL_TIME = "time";

    public static final String COL_CREATE_TIME = "create_time";

    public static final String COL_LAST_UPDATE_TIME = "last_update_time";

    public static final String COL_STATUS = "status";
}
