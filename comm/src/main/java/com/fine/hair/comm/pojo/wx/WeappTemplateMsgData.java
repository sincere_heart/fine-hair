package com.fine.hair.comm.pojo.wx;

import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/9/7 20:57
 */
@Data
public class WeappTemplateMsgData {
    private WeappTemplateMsgDataContent keyword1;
    private WeappTemplateMsgDataContent keyword2;
}
