package com.fine.hair.comm.exception;

import com.fine.hair.comm.utils.BaseException;
import com.fine.hair.comm.utils.MyStatus;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 全局异常
 * </p>
 *
 * @description: 全局异常
 */
@EqualsAndHashCode(callSuper = true)
public class SecurityException extends BaseException {
    public SecurityException(MyStatus status) {
        super(status);
    }

    public SecurityException(MyStatus status, Object data) {
        super(status, data);
    }

    public SecurityException(Integer code, String message) {
        super(code, message);
    }

    public SecurityException(Integer code, String message, Object data) {
        super(code, message, data);
    }
}
