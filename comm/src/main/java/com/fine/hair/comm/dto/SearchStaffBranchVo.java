package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/11/8 14:49
 */
@Data
@ApiModel("查找员工门店(包括挂店)vo对象")
public class SearchStaffBranchVo {
    @ApiModelProperty("门店名称")
    private String branchTitle;
    @ApiModelProperty(value = "是否用户所在门店",notes = "true为用户当前门店，false为用户当前挂店门店")
    private boolean isCurrBranch;
    @ApiModelProperty("门店id")
    private String branchId;
}
