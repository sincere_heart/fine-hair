package com.fine.hair.comm.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/2/20 19:46
 */
@Data
@ApiModel("消费业绩")
public class ConsumeVo {
    @ApiModelProperty("名字")
    private String name;
    @ApiModelProperty("金额")
    private BigDecimal money;
}
