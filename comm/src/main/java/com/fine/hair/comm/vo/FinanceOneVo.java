package com.fine.hair.comm.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/2/21 21:44
 */
@Data
@ApiModel("")
public class FinanceOneVo {
    @ApiModelProperty("门店名称和时间")
    private String branchNameAndDate;
    @ApiModelProperty("列表")
    private List<FinanceVo> list;
}
