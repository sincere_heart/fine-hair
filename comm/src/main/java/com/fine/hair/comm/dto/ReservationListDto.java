package com.fine.hair.comm.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/11/16 22:46
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class ReservationListDto extends BasePage {
    private String startTime;
    private String endTime;
    private String orderStatus;
    private String branchId;
}
