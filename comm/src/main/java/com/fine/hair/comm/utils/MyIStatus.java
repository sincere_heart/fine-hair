package com.fine.hair.comm.utils;

/**
 * <p>
 * REST API 错误码接口
 * </p>
 *
 * @description: REST API 错误码接口
 */
public interface MyIStatus {

    /**
     * 状态码
     *
     * @return 状态码
     */
    Integer getCode();

    /**
     * 返回信息
     *
     * @return 返回信息
     */
    String getMessage();

}
