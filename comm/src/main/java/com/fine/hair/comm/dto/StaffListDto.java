package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/10/10 0:08
 */
@Data
@ApiModel("门店员工列表dto")
public class StaffListDto {
    @ApiModelProperty("手机号或名称")
    private String phoneOrName;
    @ApiModelProperty("当前页")
    private Integer pageCurr;
    @ApiModelProperty("尺寸")
    private Integer pageSize;
    @ApiModelProperty("门店id")
    private Long branchId;
}
