package com.fine.hair.comm.dto.rbac;

import lombok.Data;

/**
 * @author junelee
 * @date 2020/6/10 11:21
 */
@Data
public class ResetPasswordDto {
    private Long userId;
}
