package com.fine.hair.comm.pojo;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/9/7 20:55
 */
@Data
public class TxErrData {
    @SerializedName("errcode")
    private Integer errCode;
    @SerializedName("errmsg")
    private String errMsg;
}
