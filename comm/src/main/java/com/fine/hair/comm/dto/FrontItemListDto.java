package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/11/23 16:27
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel("项目列表")
public class FrontItemListDto extends BasePage {
    private String branchId;
    @ApiModelProperty("筛选条件 1护发 2养发 3发质")
    private Integer mode;
}
