package com.fine.hair.comm.model;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>${Description}</p>
 *
 * @author mouseyCat
 * @date 2020/10/26 21:39
 */

/**
 * 用户表，技师与用户在同一张表
 */
@ApiModel(value = "用户与员工共存表")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "`user`")
public class User implements Serializable {
    public static final String COL_BIRTHDAY = "birthday";
    /**
     * id
     */
    @TableId(value = "id", type = IdType.INPUT)
    @ApiModelProperty(value = "id")
    private String id;

    @ApiModelProperty("0-无状态 1-上班 2-下班")
    @TableField(value = "on_work")
    private Integer onWork;

    @TableField(value = "put_branch_id", strategy = FieldStrategy.IGNORED)
    @ApiModelProperty(value = "挂店id集合，逗号隔开")
    private String putBranchId;

    @TableField(exist = false)
    @ApiModelProperty(value = "所属门店名称")
    private String branchName;

    @TableId(value = "hair_status")
    @ApiModelProperty(value = "头发状况")
    private String hairStatus;

    @TableId(value = "open_id")
    @ApiModelProperty(value = "openid")
    private String openId;

    @TableField(value = "sort")
    @ApiModelProperty(value = "排序")
    private Integer sort;
    /**
     * 手机号
     */
    @TableField(value = "phone_number")
    @ApiModelProperty(value = "手机号")
    private String phoneNumber;

    /**
     * 备注
     */
    @TableField(value = "remark")
    @ApiModelProperty(value = "备注")
    private String remark;

    /**
     * 用户昵称
     */
    @TableField(value = "nick_name")
    @ApiModelProperty(value = "用户昵称")
    private String nickName;

    /**
     * 用户头像
     */
    @TableField(value = "head_photo")
    @ApiModelProperty(value = "用户头像")
    private String headPhoto;

    /**
     * 用户生日
     */
    @TableField(value = "birth")
    @ApiModelProperty(value = "用户生日")
    private String birth;

    /**
     * 剩余护发服务次数
     */
    @TableField(value = "hf_count")
    @ApiModelProperty(value = "剩余护发服务次数")
    private Integer hfCount;

    /**
     * 剩余养发服务次数
     */
    @TableField(value = "yf_count")
    @ApiModelProperty(value = "剩余养发服务次数")
    private Integer yfCount;

    /**
     * 剩余发质服务次数
     */
    @TableField(value = "fz_count")
    @ApiModelProperty(value = "剩余发质服务次数")
    private Integer fzCount;

    /**
     * 顶级调理
     */
    @TableField(value = "dj_count")
    @ApiModelProperty(value = "顶级调理")
    private Integer djCount;

    /**
     * 头部调理
     */
    @TableField(value = "tb_count")
    @ApiModelProperty(value = "头部调理")
    private Integer tbCount;

    /**
     * 优惠调理
     */
    @TableField(value = "yh_count")
    @ApiModelProperty(value = "优惠调理")
    private Integer yhCount;

    @TableField(value = "point")
    @ApiModelProperty("积分")
    private BigDecimal point;

    @TableField(value = "label_type")
    @ApiModelProperty("周期id")
    private Long labelType;

    @TableField(value = "call_time")
    @ApiModelProperty("打电话时间")
    private Date callTime;

    @TableField(value = "update_by")
    @ApiModelProperty("谁更新的")
    private String updateBy;

    /**
     * 个人简介
     */
    @TableField(value = "resume")
    @ApiModelProperty(value = "个人简介")
    private String resume;

    /**
     * 性别
     */
    @TableField(value = "sex")
    @ApiModelProperty(value = "性别")
    private String sex;

    /**
     * 地区
     */
    @TableField(value = "address")
    @ApiModelProperty(value = "地区")
    private String address;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    /**
     * 最近一次更新时间
     */
    @TableField(value = "last_update_time")
    @ApiModelProperty(value = "最近一次更新时间")
    private Date lastUpdateTime;

    /**
     * 是否删除 0否 1是
     */
    @TableField(value = "`status`")
    @TableLogic
    @ApiModelProperty(value = "是否删除 0否 1是")
    private Integer status;

    @TableField(value = "experience")
    @ApiModelProperty(value = "体验 0否 1是")
    private Boolean experience;

    @TableField(value = "member")
    @ApiModelProperty(value = "会员 0否 1是")
    private Boolean member;

    /**
     * 门店id(技师所属门店)
     */
    @TableField(value = "branch_id", strategy = FieldStrategy.IGNORED)
    @ApiModelProperty(value = "门店id(技师所属门店)")
    private Long branchId;

    private static final long serialVersionUID = 1L;

    public static final String COL_ID = "id";

    public static final String COL_PHONE_NUMBER = "phone_number";

    public static final String COL_NICK_NAME = "nick_name";

    public static final String COL_HEAD_PHOTO = "head_photo";

    public static final String COL_BIRTH = "birth";

    public static final String COL_HF_COUNT = "hf_count";

    public static final String COL_YF_COUNT = "yf_count";

    public static final String COL_FZ_COUNT = "fz_count";

    public static final String COL_RESUME = "resume";

    public static final String COL_SEX = "sex";

    public static final String COL_ADDRESS = "address";

    public static final String COL_CREATE_TIME = "create_time";

    public static final String COL_LAST_UPDATE_TIME = "last_update_time";

    public static final String COL_STATUS = "status";

}
