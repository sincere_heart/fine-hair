package com.fine.hair.comm.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/2/6 0:13
 */
@Data
@ApiModel("")
public class InventoryPutRecordVo {
    @ApiModelProperty("唯一id")
    private Long id;
    @ApiModelProperty("本次入库数量")
    private String count;
    @ApiModelProperty("本次入库订单总金额")
    private String totalMoney;
    @ApiModelProperty("本次入库门店名称")
    private String branchName;
    @ApiModelProperty("本次入库的用户名称")
    private String userName;
    @ApiModelProperty("本次入库的备注")
    private String remark;
}
