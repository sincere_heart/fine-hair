package com.fine.hair.comm.pojo.wx;

import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/9/7 20:58
 */
@Data
public class WxSendMsg1 {
    private String value;
}
