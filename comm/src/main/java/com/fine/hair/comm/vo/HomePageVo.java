package com.fine.hair.comm.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/2/20 19:31
 */
@Data
@ApiModel("")
public class HomePageVo {
    @ApiModelProperty("门店名称")
    private String branchName;
    @ApiModelProperty("数据统计列表")
    private List<HomePageVoList> homePageVoList;
}
