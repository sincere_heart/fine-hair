package com.fine.hair.comm.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/11/6 23:22
 */
@Data
@ApiModel("员工订单信息")
public class StaffOrderVo {
    @ApiModelProperty("门店名称")
    private String title;
    @ApiModelProperty("消费者名称")
    private String customerName;
    @ApiModelProperty("订单执行日期")
    private String date;
    @ApiModelProperty("订单执行时间")
    private String time;
    @ApiModelProperty("消费者手机号")
    private String customerPhone;
    @ApiModelProperty("技师头像")
    private String technicianAvatar;
    @ApiModelProperty("技师id")
    private String technicianId;
    @ApiModelProperty("技师昵称")
    private String technicianName;
    @ApiModelProperty("技师手机号")
    private String technicianPhone;
    @ApiModelProperty("订单状态 1未开始 2已完成 3已取消")
    private Integer orderStatus;
    @ApiModelProperty("项目名称")
    private String itemTitle;
    @ApiModelProperty("客户id")
    private String customerId;
    @ApiModelProperty("订单id")
    private String orderId;
    @ApiModelProperty("是否评论")
    private Boolean isComment;
}
