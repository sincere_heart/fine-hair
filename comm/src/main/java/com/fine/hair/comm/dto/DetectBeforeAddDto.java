package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/2/6 0:28
 */
@Data
@ApiModel("")
public class DetectBeforeAddDto {
    @ApiModelProperty("物品名称")
    @NotNull(message = "物品名称不得为空")
    private String name;
    @NotNull(message = "规格不得为空")
    @ApiModelProperty("规格")
    private String specification;
    @NotNull(message = "用户id不得为空")
    @ApiModelProperty("用户id")
    private Long userId;

    private String branchId;

}
