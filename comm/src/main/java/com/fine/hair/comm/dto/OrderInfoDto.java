package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/2/19 13:35
 */
@Data
@ApiModel("订单信息dto")
public class OrderInfoDto {
    @NotNull(message = "用户所在维度不能为空")
    @ApiModelProperty("当前用户维度")
    private Double lat;
    @NotNull(message = "用户所在经度不能为空")
    @ApiModelProperty("当前用户经度")
    private Double lng;
    @NotNull(message = "订单id不能为空")
    @ApiModelProperty("订单id")
    private Long orderId;
}
