package com.fine.hair.comm.enums;

import lombok.Getter;

/**
 * example
 *
 * @author junelee
 * @date 2020/4/24 10:35
 */
@Getter
public enum OrderStatus implements BaseType {

    /**
     * 未知
     */
    UNKNOWN(0, "未知"),
    /**
     * 未开始
     */
    WILL_COMPLETE(1, "未开始"),

    /**
     * 已完成
     */
    COMPLETED(2, "已完成"),

    /**
     * 已取消
     */
    CANCELED(3,"已取消"),
    /**
     * 已开始
     */
    STARTING(4,"已开始");

    /**
     * 状态码
     */
    private final Integer code;
    /**
     * 状态码信息
     */
    private final String message;

    public static OrderStatus fromCode(Integer code) {
        OrderStatus[] resultTypes = OrderStatus.values();
        for (OrderStatus resultType : resultTypes) {
            if (resultType.getCode()
                    .equals(code)) {
                return resultType;
            }
        }
        return UNKNOWN;
    }

    public static OrderStatus fromMessage(String message) {
        OrderStatus[] resultTypes = OrderStatus.values();
        for (OrderStatus resultType : resultTypes) {
            if (resultType.getMessage()
                    .equals(message)) {
                return resultType;
            }
        }
        return UNKNOWN;
    }

    OrderStatus(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

}
