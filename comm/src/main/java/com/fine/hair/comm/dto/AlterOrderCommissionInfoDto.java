package com.fine.hair.comm.dto;

import com.fine.hair.comm.pojo.CommissionPojo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.util.List;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/10/29 23:14
 */
@Data
@ApiModel("修改订单提成信息dto实体")
public class AlterOrderCommissionInfoDto {
    @ApiModelProperty("订单信息列表")
    private List<CommissionPojo> commissionList;
}
