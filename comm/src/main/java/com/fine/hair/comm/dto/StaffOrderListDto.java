package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/10/29 23:33
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel("员工订单列表dto请求体")
public class StaffOrderListDto extends BasePage {
    @ApiModelProperty("员工id，传userId即可")
    private String staffId;
    @ApiModelProperty("订单状态 1未开始 2已完成 4正在服务、已开始 3已取消  没有需求则不转")
    private Integer orderStatus;
    @ApiModelProperty("店铺名称模糊查询")
    private String branchTitle;
}
