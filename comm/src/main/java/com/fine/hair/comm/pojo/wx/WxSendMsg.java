package com.fine.hair.comm.pojo.wx;

import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/9/7 20:58
 */
@Data
public class WxSendMsg {
    private WxSendMsg1 amount1;
    private WxSendMsg2 thing4;
}
