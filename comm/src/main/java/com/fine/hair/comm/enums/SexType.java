package com.fine.hair.comm.enums;

import lombok.Getter;

/**
 * example
 *
 * @author junelee
 * @date 2020/4/24 10:35
 */
@Getter
public enum SexType implements BaseType {

    /**
     * 未知
     */
    UNKNOWN(0,"未知"),
    /**
     * 男
     */
    MALE(1, "男"),

    /**
     * 女
     */
    FEMALE(2, "女");


    /**
     * 状态码
     */
    private final Integer code;
    /**
     * 状态码信息
     */
    private final String message;

    public static SexType fromCode(Integer code) {
        SexType[] resultTypes = SexType.values();
        for (SexType resultType : resultTypes) {
            if (resultType.getCode()
                    .equals(code)) {
                return resultType;
            }
        }
        return UNKNOWN;
    }

    public static SexType fromMessage(String message) {
        SexType[] resultTypes = SexType.values();
        for (SexType resultType : resultTypes) {
            if (resultType.getMessage()
                    .equals(message)) {
                return resultType;
            }
        }
        return UNKNOWN;
    }

    SexType(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

}
