package com.fine.hair.comm.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/2/22 23:20
 */
@Data
@ApiModel("")
public class FlowStatVo {
    @ApiModelProperty("时间段")
    private String time;
    @ApiModelProperty("流量数")
    private Integer count;
}
