package com.fine.hair.comm.enums;

import lombok.Getter;

/**
 * example
 *
 * @author junelee
 * @date 2020/4/24 10:35
 */
@Getter
public enum ApplyStatus implements BaseType {

    /**
     * 待同意
     */
    WAIT(0, "待同意"),

    /**
     * 同意
     */
    AGREE(1, "同意"),

    /**
     * 拒绝
     */
    DENY(2, "拒绝");

    /**
     * 状态码
     */
    private final Integer code;
    /**
     * 状态码信息
     */
    private final String message;

    public static ApplyStatus fromCode(Integer code) {
        ApplyStatus[] resultTypes = ApplyStatus.values();
        for (ApplyStatus resultType : resultTypes) {
            if (resultType.getCode()
                    .equals(code)) {
                return resultType;
            }
        }
        return WAIT;
    }

    public static ApplyStatus fromMessage(String message) {
        ApplyStatus[] resultTypes = ApplyStatus.values();
        for (ApplyStatus resultType : resultTypes) {
            if (resultType.getMessage()
                    .equals(message)) {
                return resultType;
            }
        }
        return WAIT;
    }

    ApplyStatus(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

}
