package com.fine.hair.comm.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.util.Date;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/1/26 21:55
 */
@Data
@ApiModel("标签用户列表")
public class InfoListVO {
    @ApiModelProperty("头像")
    private String avatar;
    @ApiModelProperty("昵称")
    private String name;
    @ApiModelProperty("备注")
    private String remark;
    @ApiModelProperty("用户id")
    private String userId;
    @ApiModelProperty("手机号")
    private String phone;
    @ApiModelProperty("打电话时间")
    private Date callTime;
    @ApiModelProperty("谁更新的")
    private String updateBy;

    @ApiModelProperty("项目图片")
    private String itemImage;
    @ApiModelProperty("项目名称")
    private String itemTitle;
    @ApiModelProperty("项目金额")
    private String itemPrice;
    @ApiModelProperty("订单执行时间")
    private String executeTime;
    @ApiModelProperty("订单创建时间")
    private String createTime;
}
