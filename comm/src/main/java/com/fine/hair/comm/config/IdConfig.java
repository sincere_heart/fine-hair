package com.fine.hair.comm.config;

import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * <p>
 * 雪花主键生成器
 * </p>
 *
 * @description: 雪花主键生成器
 */
@Configuration
public class IdConfig {
    /**
     * 雪花生成器
     * 使用方式：注入
     *
     * @Autowired private Snowflake snowflake
     */
    @Bean
    public Snowflake snowflake() {
        return IdUtil.createSnowflake(1, 1);
    }

}
