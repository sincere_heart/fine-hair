package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/1/22 22:18
 */
@Data
public class UserCalendarDto {
    @ApiModelProperty("日期，格式 YYYY-mm-dd")
    private String date;
    @ApiModelProperty("用户id")
    private Long userId;
}
