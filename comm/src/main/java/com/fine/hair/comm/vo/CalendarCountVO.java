package com.fine.hair.comm.vo;

import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/1/26 21:50
 */
@Data
public class CalendarCountVO {
    private Long countOne;
    private Long countTwo;
    private Long countThree;
    private Long countFour;
}
