package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/2/7 21:52
 */
@Data
@ApiModel("员工工作信息dto")
public class StaffWorkInfoDto {
    @ApiModelProperty(value = "员工id",required = true)
    @NotNull(message = "员工id")
    private String userId;
}
