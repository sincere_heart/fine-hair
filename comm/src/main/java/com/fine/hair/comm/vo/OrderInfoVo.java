package com.fine.hair.comm.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/10/4 15:39
 */
@Data
@ApiModel("订单信息vo")
public class OrderInfoVo {
    @ApiModelProperty("门店名称")
    private String branchName;
    @ApiModelProperty("门店地址")
    private String address;
    @ApiModelProperty("门店经度")
    private Double lng;
    @ApiModelProperty("门店维度")
    private Double lat;
    @ApiModelProperty("门店距离")
    private Double distance;
    @ApiModelProperty("预约时间")
    private String businessTime;
    @ApiModelProperty("预约项目名称")
    private String itemName;
    @ApiModelProperty("预约者姓名")
    private String nickName;
    @ApiModelProperty("预约者电话")
    private String phoneNumber;
    @ApiModelProperty("预约者备注")
    private String remark;
    @ApiModelProperty("门店图片")
    private String branchImg;
}
