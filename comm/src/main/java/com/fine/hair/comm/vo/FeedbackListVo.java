package com.fine.hair.comm.vo;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/11/18 14:48
 */

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel("反馈列表vo实体")
public class FeedbackListVo {
    @ApiModelProperty("头像")
    private String avatar;
    @ApiModelProperty("昵称")
    private String nickName;
    @ApiModelProperty("性别")
    private String sex;
    @ApiModelProperty("反馈的描述信息")
    private String description;
    @ApiModelProperty("反馈id")
    private String feedbackId;
    @ApiModelProperty("技师名称")
    private String technicianName;
    @ApiModelProperty("技师所在门店")
    private String branchName;
    @ApiModelProperty("评分")
    private BigDecimal score;
}
