package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/2/7 17:15
 */
@Data
@ApiModel("将订单状态改为已开始dto")
public class StartOrderDto {
    @ApiModelProperty(value = "订单id", required = true)
    @NotNull(message = "订单id不得为空")
    private Long orderId;
}
