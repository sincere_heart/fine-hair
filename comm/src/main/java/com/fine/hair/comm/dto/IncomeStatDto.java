package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/10/25 23:02
 */
@Data
@ApiModel("商铺统计")
public class IncomeStatDto{
    @ApiModelProperty("时间 格式 yyyy-MM-dd")
    private String date;
    @ApiModelProperty("店铺id")
    private String branchId;
}
