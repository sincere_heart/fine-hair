package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/10/29 23:40
 */
@Data
@ApiModel("取消订单dto请求体")
public class StaffCancelOrderDto {
    @ApiModelProperty("订单id")
    private String orderId;
}
