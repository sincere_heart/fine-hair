package com.fine.hair.comm.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * @author liheng
 * @ClassName LeaveDto
 * @Description
 * @date 2021-03-08 16:02
 */
@Data
@ApiModel("请假实体")
public class LeaveDto {
    /**
     * 请假操作人
     */
    @ApiModelProperty(value = "当前登录用户id", required = true)
    @NotNull(message = "当前登录用户id不得为空")
    private Long operateUid;

    /**
     * (技师)id
     */
    @ApiModelProperty(value = "请假技师id", required = true)
    @NotNull(message = "请假技师id不得为空")
    private Long technicianId;

    /**
     * 请假时长，单位天
     */
    @ApiModelProperty(value = "请假天数，默认1天", required = true)
    @NotNull(message = "请假天数不得为空")
    private Integer leaveDay=1;

    /**
     * 请假开始日期
     */
    @NotNull(message = "请假开始日期不得为空")
    @ApiModelProperty("请假开始日期 格式 yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime startTime;

}
