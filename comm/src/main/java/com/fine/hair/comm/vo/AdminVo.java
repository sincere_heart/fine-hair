package com.fine.hair.comm.vo;

import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/12/20 22:00
 */
@Data
public class AdminVo {
    private String phone;
    private String password;
    private String branchId;
    private String roleName;
    private String createTime;
    private String id;
    private String branchName;
    private String username;

}
