package com.fine.hair.comm.utils;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/11/6 23:51
 */
public class DateUtils {
    /**
     * 基姆拉尔森计算公式根据日期判断周几
     * @param y 年
     * @param m 月
     * @param d 日
     */
    public static String calculateWeekDay(int y, int m, int d) {
        if(m < 1 || m >12){
            return "are you ok";
        }
        if (m == 1 || m == 2) {
            m += 12;
            y--;
        }
        int iWeek = (d + 2 * m + 3 * (m + 1) / 5 + y + y / 4 - y / 100 + y / 400) % 7;
        switch (iWeek) {
            case 0:
                return "周一";
            case 1:
                return "周二";
            case 2:
                return "周三";
            case 3:
                return "周四";
            case 4:
                return "周五";
            case 5:
                return "周六";
            case 6:
                return "周日";
            default:
                return "oh my god";
        }
    }

    public static String dayTime(int hour) {
        if (hour < 0 || hour > 24) {
            return "fuck you";
        }
        if (hour < 7) {
            return "凌晨";
        }
        if (hour < 10) {
            return "早晨";
        }
        if (hour < 12) {
            return "上午";
        }
        if (hour < 14) {
            return "中午";
        }
        if (hour < 18) {
            return "下午";
        }
        if (hour < 25) {
            return "晚上";
        }
        return "oh my god";
    }
}
