package com.fine.hair.comm.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/10/25 23:07
 */
@Data
@ApiModel("商铺统计门店员工list")
public class IncomeStat {
    @ApiModelProperty("用户昵称")
    private String nickName;
    @ApiModelProperty("手机号")
    private String phoneNumber;
    @ApiModelProperty("头像")
    private String avatar;
    @ApiModelProperty("订单总数")
    private Integer orderTotalCount;
    @ApiModelProperty("订单总金额")
    private BigDecimal orderTotalMoney;
}
