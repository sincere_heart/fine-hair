package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/10/8 21:29
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel("员工风采")
public class StaffMienDto extends BasePage {
    @ApiModelProperty(value = "区域、服务、店铺名称【默认传空为查询所有】")
    private String areaOrServiceOrBranchName;
    @ApiModelProperty("用户id")
    private Long userId;
}
