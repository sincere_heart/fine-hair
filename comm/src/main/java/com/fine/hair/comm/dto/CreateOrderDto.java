package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/10/24 17:12
 */
@Data
@ApiModel("创建订单dto")
public class CreateOrderDto {
    @ApiModelProperty("用户id")
    private String userId;
    @ApiModelProperty("门店id")
    private String branchId;
    @ApiModelProperty("技师id")
    private String technicianId;
    @ApiModelProperty("项目id")
    private String itemId;
    @ApiModelProperty("服务开始时间")
    @NotNull(message = "执行时间不得为空")
    private String executeTime;
    @ApiModelProperty("用户备注")
    private String remark;
    @ApiModelProperty("formId,用于下发服务消息")
    private String formId;
}
