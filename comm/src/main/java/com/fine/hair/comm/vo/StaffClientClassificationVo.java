package com.fine.hair.comm.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/12/4 0:20
 */
@Data
@ApiModel("员工客户类型页面vo")
public class StaffClientClassificationVo {
    @ApiModelProperty("服务次数")
    private Long countSerivce;
    @ApiModelProperty("门店名称")
    private String branchTitle;
}
