package com.fine.hair.comm.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/2/18 13:20
 */
@Data
@ApiModel("")
public class EvaluatePageVo {
    @ApiModelProperty("头像")
    private String avatar;
    @ApiModelProperty("名称")
    private String name;
    @ApiModelProperty("手机")
    private String phone;
    @ApiModelProperty("评分")
    private BigDecimal score;
}
