package com.fine.hair.comm.vo.rbac;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author junelee
 * @date 2020/7/6 16:21
 */
@Data
public class LoginSuccVo {
    @ApiModelProperty("权限名")
    private String nickName;
    @ApiModelProperty("用户名")
    private String username;
    @ApiModelProperty("jwt信息")
    private JwtResponse jwtResponse;

    @ApiModelProperty("所属门店id，为空时表示不是门店管理员")
    private String branchId;
    @ApiModelProperty("用户id")
    private String userId;
    @ApiModelProperty("门店名称")
    private String branchName;
    @ApiModelProperty("电话")
    private String phone;
}
