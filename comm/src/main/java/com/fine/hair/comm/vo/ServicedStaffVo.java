package com.fine.hair.comm.vo;

import com.fine.hair.comm.pojo.StaffSimpleInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.util.List;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/1/10 11:59
 */
@Data
@ApiModel("")
public class ServicedStaffVo {
    @ApiModelProperty("员工展示列表")
    private List<StaffSimpleInfo> staffList;
}
