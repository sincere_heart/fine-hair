package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/11/8 18:54
 */
@Data
@ApiModel("门店列表dto对象")
public class ItemListDto {
    @ApiModelProperty("门店id")
    private String branchId;
}
