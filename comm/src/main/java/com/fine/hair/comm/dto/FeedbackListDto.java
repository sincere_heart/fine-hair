package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/11/18 14:47
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel("反馈列表dto对象")
public class FeedbackListDto extends BasePage {

}
