package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/2/22 23:20
 */
@Data
@ApiModel("")
public class FlowStatDto {
    @ApiModelProperty("")
    private String branchId;
}
