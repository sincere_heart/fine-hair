package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/11/17 21:36
 */
@Data
@ApiModel("添加疗程列表")
public class AddReservationBatchList {
    @ApiModelProperty("不传")
    private String orderId;
    @ApiModelProperty("不传")
    private String userId;
    @ApiModelProperty("不传")
    private String branchId;
    @ApiModelProperty("不传")
    private String technicianId;
    @ApiModelProperty("不传")
    private String itemId;
    @ApiModelProperty("服务开始时间 若选了配置，则不传")
    private Date executeTime;
    @ApiModelProperty("用户备注 可不传")
    private String remark;
}
