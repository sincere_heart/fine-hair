package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/2/21 22:13
 */
@Data
@ApiModel("支出统计")
public class ExpendStatDto {
    @ApiModelProperty("门店id")
    private String branchId;
}
