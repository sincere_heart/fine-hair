package com.fine.hair.comm.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/11/10 9:23
 */
@Data
@ApiModel("获取用户类型vo对象")
public class UserTypeVo {
    @ApiModelProperty("是不是员工 true是 false不是")
    private Boolean isStaff;
    @ApiModelProperty("员工门店id")
    private Long branchId;
}
