package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/10/24 17:38
 */
@Data
@ApiModel("分页对象")
public class BasePage {
    @NotNull(message = "当前页不得为空")
    @ApiModelProperty(value = "当前页",required = true)
    private Integer pageCurr;
    @NotNull(message = "当前尺寸不得为空")
    @ApiModelProperty(value = "尺寸",required = true)
    private Integer pageSize;
}
