package com.fine.hair.comm.vo;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author junelee
 * @date 2020/6/27 22:24
 */
@Data
@Accessors(chain = true)
public class WxLiteUserInfo {
    private String openId;

    private String nickName;

    private String gender;

    private String city;

    private String language;

    private String province;

    private String country;

    private String avatarUrl;

    private String unionId;

    private String phone;

    private String phoneNumber;

    private Watermark watermark;
}
