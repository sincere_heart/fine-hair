package com.fine.hair.comm.enums;

/**
 * @author junelee
 * @date 2020/3/7 16:28
 */
public interface BaseType {
    /**
     * 返回 当前枚举类的 code 值
     * @return code
     */
    Integer getCode();

    /**
     * 返回当前枚举类的 message 值
     * @return message
     */
    String getMessage();
}
