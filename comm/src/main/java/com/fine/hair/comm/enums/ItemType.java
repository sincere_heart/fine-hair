package com.fine.hair.comm.enums;

import lombok.Getter;

/**
 * example
 *
 * @author junelee
 * @date 2020/4/24 10:35
 */
@Getter
public enum ItemType implements BaseType {

    /**
     * 初级调理
     */
    HF(1, "初级调理"),
    /**
     * 中级调理
     */
    YF(2, "中级调理"),
    /**
     * 高级调理
     */
    FZ(3, "高级调理"),
    /**
     * 顶级调理
     */
    DJ(4, "顶级调理"),
    /**
     * 头部调理
     */
    TB(5, "头部调理"),
    /**
     * 优惠调理
     */
    YH(6, "优惠调理");

    /**
     * 状态码
     */
    private final Integer code;
    /**
     * 状态码信息
     */
    private final String message;

    public static ItemType fromCode(Integer code) {
        ItemType[] resultTypes = ItemType.values();
        for (ItemType resultType : resultTypes) {
            if (resultType.getCode()
                    .equals(code)) {
                return resultType;
            }
        }
        return HF;
    }

    public static ItemType fromMessage(String message) {
        ItemType[] resultTypes = ItemType.values();
        for (ItemType resultType : resultTypes) {
            if (resultType.getMessage()
                    .equals(message)) {
                return resultType;
            }
        }
        return HF;
    }

    ItemType(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

}
