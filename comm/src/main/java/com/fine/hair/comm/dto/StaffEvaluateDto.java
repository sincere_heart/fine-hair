package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/10/4 16:41
 */
@Data
@ApiModel("员工评价dto")
public class StaffEvaluateDto {
    @ApiModelProperty("员工id")
    private Long userId;
    @ApiModelProperty("当前页")
    private Integer pageCurr;
    @ApiModelProperty("页数")
    private Integer pageSize;
}
