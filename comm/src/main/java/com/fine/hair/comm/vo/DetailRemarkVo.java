package com.fine.hair.comm.vo;

import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/12/1 0:50
 */
@Data
public class DetailRemarkVo {
    private String clientName;
    private String sex;
    private String phoneNumber;
    private String birth;
    private String remark;
}
