package com.fine.hair.comm.vo;

import com.fine.hair.comm.pojo.StaffSimpleInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/10/8 21:11
 */
@Data
@ApiModel("员工风采信息vo")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StaffMienVo {
    @ApiModelProperty("门店图片")
    private String branchImage;
    @ApiModelProperty("门店id")
    private String branchId;
    @ApiModelProperty("门店名称")
    private String branchName;
    @ApiModelProperty("门店地址")
    private String address;
    @ApiModelProperty("门店热线")
    private String tel;
    @ApiModelProperty("营业时间")
    private String businessTime;
    @ApiModelProperty("员工展示列表【不做分页】")
    private List<StaffSimpleInfo> staffList;
}
