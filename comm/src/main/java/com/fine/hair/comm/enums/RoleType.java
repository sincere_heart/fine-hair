package com.fine.hair.comm.enums;

import lombok.Getter;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/10/22 15:08
 */
@Getter
public enum RoleType {

    /**
     * 超级管理员
     */
    SUPER_ADMIN(1L, "超级管理员"),
    /**
     * 财务管理员
     */
    FINANCE_ADMIN(2L, "财务管理员"),
    /**
     * 门店管理员
     */
    BRANCH_ADMIN(3L, "门店管理员");


    /**
     * 状态码
     */
    private Long code;
    /**
     * 状态码信息
     */
    private String message;

    public static RoleType fromCode(Long code) {
        RoleType[] resultTypes = RoleType.values();
        for (RoleType resultType : resultTypes) {
            if (resultType.getCode()
                    .equals(code)) {
                return resultType;
            }
        }
        return null;
    }

    public static RoleType fromMessage(String message) {
        RoleType[] resultTypes = RoleType.values();
        for (RoleType resultType : resultTypes) {
            if (resultType.getMessage()
                    .equals(message)) {
                return resultType;
            }
        }
        return null;
    }

    RoleType(Long code, String message) {
        this.code = code;
        this.message = message;
    }
}
