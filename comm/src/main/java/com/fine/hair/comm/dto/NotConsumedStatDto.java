package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/2/23 19:56
 */
@Data
@ApiModel("")
public class NotConsumedStatDto {
    @ApiModelProperty("门店id")
    private String branchId;
}
