package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/11/8 13:38
 */
@Data
@ApiModel("添加员工到店dto实体")
public class AddStaffDto {
    @ApiModelProperty("员工id")
    private String userId;
    @ApiModelProperty("门店id")
    private String branchId;
}
