package com.fine.hair.comm.vo;

import com.fine.hair.comm.utils.PageInfo;
import com.fine.hair.comm.model.Item;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/12/2 12:46
 */
@Data
@ApiModel("项目列表VO")
public class ItemListVo {
    @ApiModelProperty("项目列表分页信息")
    private PageInfo<Item> list;
}
