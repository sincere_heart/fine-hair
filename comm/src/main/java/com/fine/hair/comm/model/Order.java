package com.fine.hair.comm.model;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>${Description}</p>
 * @author mouseyCat
 * @date 2020/10/26 21:39
 */

/**
 * 订单表
 */
@ApiModel(value = "订单表")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "`order`")
public class Order implements Serializable {

    @ApiModelProperty("消费者名称")
    @TableField(exist = false)
    private String customerName;
    @ApiModelProperty("消费者头像")
    @TableField(exist = false)
    private String customerAvatar;
    @ApiModelProperty("消费者生日")
    @TableField(exist = false)
    private String customerBirth;

    @ApiModelProperty("消费者生日")
    @TableField(exist = false)
    private String birth;

    @ApiModelProperty("消费者手机号")
    @TableField(exist = false)
    private String customerPhone;
    @ApiModelProperty("技师头像")
    @TableField(exist = false)
    private String technicianAvatar;
    @ApiModelProperty("技师名称")
    @TableField(exist = false)
    private String technicianName;
    @ApiModelProperty("技师手机号")
    @TableField(exist = false)
    private String technicianPhone;
    @ApiModelProperty("门店名称")
    @TableField(exist = false)
    private String branchTitle;
    @ApiModelProperty("订单状态数值 订单状态 1未开始 2已完成 3已取消")
    @TableField(exist = false)
    private Integer orderStatusInt;

    @TableField(exist = false)
    @ApiModelProperty(value = "订单评分")
    private String score;
    /**
     * id
     */
    @TableId(value = "id", type = IdType.INPUT)
    @ApiModelProperty(value = "id")
    private String id;

    @ApiModelProperty("项目所属服务类型 1护发服务 2养发服务 3发质服务")
    @TableId(value = "item_type")
    private String itemType;

    /**
     * 订单所属用户
     */
    @TableField(value = "user_id")
    @ApiModelProperty(value = "订单所属用户")
    private String userId;

    /**
     * 订单状态 1未开始 2已完成 3已取消
     */
    @TableField(value = "order_status")
    @ApiModelProperty(value = "订单状态 1未开始 2已完成 3已取消")
    private String orderStatus;

    /**
     * 门店id
     */
    @TableField(value = "branch_id")
    @ApiModelProperty(value = "门店id")
    private String branchId;

    /**
     * 技师id
     */
    @TableField(value = "technician_id")
    @ApiModelProperty(value = "技师id")
    private String technicianId;

    /**
     * 项目id
     */
    @TableField(value = "item_id")
    @ApiModelProperty(value = "项目id")
    private String itemId;

    /**
     * 服务开始时间
     */
    @TableField(value = "execute_time")
    @ApiModelProperty(value = "服务开始时间")
    private Date executeTime;

    /**
     * 用户备注
     */
    @TableField(value = "remark")
    @ApiModelProperty(value = "用户备注")
    private String remark;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    /**
     * 最近一次更新时间
     */
    @TableField(value = "last_update_time")
    @ApiModelProperty(value = "最近一次更新时间")
    private Date lastUpdateTime;

    /**
     * 是否删除 0否 1是
     */
    @TableField(value = "`status`")
    @TableLogic
    @ApiModelProperty(value = "是否删除 0否 1是")
    private Integer status;

    /**
     * 项目图片
     */
    @TableField(value = "item_image")
    @ApiModelProperty(value = "项目图片")
    private String itemImage;

    /**
     * 项目名称
     */
    @TableField(value = "item_title")
    @ApiModelProperty(value = "项目名称")
    private String itemTitle;

    /**
     * 订单价格
     */
    @TableField(value = "item_price")
    @ApiModelProperty(value = "订单价格")
    private BigDecimal itemPrice;

    /**
     * 项目描述
     */
    @TableField(value = "item_content")
    @ApiModelProperty(value = "项目描述")
    private String itemContent;

    /**
     * 项目执行所需时间
     */
    @TableField(value = "item_time")
    @ApiModelProperty(value = "项目执行所需时间--即时长（单位  分钟）")
    private Integer itemTime;

    private static final long serialVersionUID = 1L;

    public static final String COL_ID = "id";

    public static final String COL_USER_ID = "user_id";

    public static final String COL_ORDER_STATUS = "order_status";

    public static final String COL_BRANCH_ID = "branch_id";

    public static final String COL_TECHNICIAN_ID = "technician_id";

    public static final String COL_ITEM_ID = "item_id";

    public static final String COL_EXECUTE_TIME = "execute_time";

    public static final String COL_REMARK = "remark";

    public static final String COL_CREATE_TIME = "create_time";

    public static final String COL_LAST_UPDATE_TIME = "last_update_time";

    public static final String COL_STATUS = "status";

    public static final String COL_ITEM_IMAGE = "item_image";

    public static final String COL_ITEM_TITLE = "item_title";

    public static final String COL_ITEM_PRICE = "item_price";

    public static final String COL_ITEM_CONTENT = "item_content";
}
