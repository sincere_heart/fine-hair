package com.fine.hair.comm.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>${Description}</p>
 * @author mouseyCat
 * @date 2021/2/5 23:57
 */

/**
    * 库存表
    */
@ApiModel(value="com-lingser-model-Inventory")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "inventory")
public class Inventory implements Serializable {
    /**
     * id
     */
    @TableId(value = "id", type = IdType.INPUT)
    @ApiModelProperty(value="id")
    private Long id;

    @TableField(value = "branch_id")
    @ApiModelProperty(value="门店id")
    private String branchId;

    /**
     * 商品名称
     */
    @TableField(value = "`name`")
    @ApiModelProperty(value="商品名称")
    private String name;

    /**
     * 商品图片
     */
    @TableField(value = "img")
    @ApiModelProperty(value="商品图片")
    private String img;

    /**
     * 规格
     */
    @TableField(value = "specification")
    @ApiModelProperty(value="规格")
    private String specification;

    /**
     * 总数
     */
    @TableField(value = "`sum`")
    @ApiModelProperty(value="总数")
    private Integer sum;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    @ApiModelProperty(value="创建时间")
    private Date createTime;

    /**
     * 最近一次更新时间
     */
    @TableField(value = "last_update_time")
    @ApiModelProperty(value="最近一次更新时间")
    private Date lastUpdateTime;

    /**
     * 是否删除 0否 1是
     */
    @TableField(value = "`status`")
    @ApiModelProperty(value="是否删除 0否 1是")
    private Integer status;

    private static final long serialVersionUID = 1L;

    public static final String COL_ID = "id";

    public static final String COL_NAME = "name";

    public static final String COL_IMG = "img";

    public static final String COL_SPECIFICATION = "specification";

    public static final String COL_SUM = "sum";

    public static final String COL_CREATE_TIME = "create_time";

    public static final String COL_LAST_UPDATE_TIME = "last_update_time";

    public static final String COL_STATUS = "status";
}
