package com.fine.hair.comm.dto;

import com.fine.hair.comm.dto.wx.WxLoginDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/9/7 21:17
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class WxRegisterDto  extends WxLoginDto {
    @ApiModelProperty(value = "微信小程序openid", required = true)
    private String openId;

    @ApiModelProperty(value = "微信unionid,没有就不传")
    private String unionId;

    @ApiModelProperty(value = "微信昵称,没有就不传")
    private String nickname;

    @ApiModelProperty(value = "微信头像,没有就不传")
    private String headIconUrl;
}
