package com.fine.hair.comm.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/1/28 19:24
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class CalendarConfigListDTO extends BasePage{

}
