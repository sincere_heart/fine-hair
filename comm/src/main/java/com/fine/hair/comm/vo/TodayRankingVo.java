package com.fine.hair.comm.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/2/20 20:21
 */
@Data
@ApiModel("业绩排行榜")
public class TodayRankingVo {
    @ApiModelProperty("门店名称")
    private String branchName;
    @ApiModelProperty("消费列表")
    private List<ConsumeVo> consumeVoList;
    @ApiModelProperty("现金列表")
    private List<RechargeVo> rechargeVoList;
}
