package com.fine.hair.comm.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/2/21 22:12
 */
@Data
@ApiModel("支出统计")
public class ExpendStatVo {
    @ApiModelProperty("当日")
    private BigDecimal today = BigDecimal.ZERO;
    @ApiModelProperty("本月")
    private BigDecimal thisMonth = BigDecimal.ZERO;
    @ApiModelProperty("本季度")
    private BigDecimal thisQuarter = BigDecimal.ZERO;
    @ApiModelProperty("本年")
    private BigDecimal thisYear = BigDecimal.ZERO;
}
