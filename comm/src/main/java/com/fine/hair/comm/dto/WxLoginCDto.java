package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/9/9 14:02
 */
@Data
public class WxLoginCDto {
    @ApiModelProperty(value = "微信公众号获取到的 code", required = true)
    private String code;
}
