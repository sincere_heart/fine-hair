package com.fine.hair.comm.utils;

import lombok.Getter;

/**
 * <p>
 * 通用状态码
 * </p>
 *
 * @description: 通用状态码
 */
@Getter
public enum MyStatus implements MyIStatus {
    /**
     * 库存不足
     */
    STOCK_NOT_ENOUGH(0,"库存不足"),
    /**
     * 您已是员工，请勿重复提交
     */
    WAS_STAFF(0,"您已是员工，请勿重复提交"),
    /**
     * 该门店下还有未完成订单，拒绝删除
     */
    REJECT_DELETE_BRANCH(0,"该门店下还有未完成订单，拒绝删除"),
    /**
     * 手机号或用户名已存在
     */
    PHONE_OR_USERNAME_IS_EXIST(0,"手机号或用户名已存在!"),
    /**
     * 操作失败！
     */
    FAILED(0,"操作失败！"),
    /**
     * 操作成功！
     */
    SUCCESS(200, "操作成功！"),

    /**
     * 操作异常！
     */
    ERROR(0, "操作异常！"),

    /**
     * 退出成功！
     */
    LOGOUT(200, "退出成功！"),
    /**
     * 修改密码成功！
     */
    MODIFY_PASS(200,"修改密码成功！"),
       /**
     * 请先登录！
     */
    UNAUTHORIZED(401, "请先登录！"),

    /**
     * 暂无权限访问！
     */
    ACCESS_DENIED(403, "权限不足！"),

    /**
     * 请求不存在！
     */
    REQUEST_NOT_FOUND(404, "请求不存在！"),

    /**
     * 请求方式不支持！
     */
    HTTP_BAD_METHOD(405, "请求方式不支持！"),

    /**
     * 请求异常！
     */
    BAD_REQUEST(0, "请求异常！"),

    /**
     * 参数不匹配！
     */
    PARAM_NOT_MATCH(400, "参数不匹配！"),

    /**
     * 参数不能为空！
     */
    PARAM_NOT_NULL(400, "参数不能为空！"),

    /**
     * 当前用户已被锁定，请联系管理员解锁！
     */
    USER_DISABLED(403, "当前用户已被锁定，请联系管理员解锁！"),

    /**
     * 用户名或密码错误！
     */
    USERNAME_PASSWORD_ERROR(5001, "用户名或密码错误！"),

    /**
     * token 已过期，请重新登录！
     */
    TOKEN_EXPIRED(5002, "token 已过期，请重新登录！"),

    /**
     * token 解析失败，请尝试重新登录！
     */
    TOKEN_PARSE_ERROR(5002, "token 解析失败，请尝试重新登录！"),

    /**
     * 当前用户已在别处登录，请尝试更改密码或重新登录！
     */
    TOKEN_OUT_OF_CTRL(5003, "当前用户已在别处登录，请尝试更改密码或重新登录！"),

    /**
     * 无法手动踢出自己，请尝试退出登录操作！
     */
    KICKOUT_SELF(5004, "无法手动踢出自己，请尝试退出登录操作！"),

    /**
     * 该权限不存在，请刷新后尝试该操作！
     */
    ROLE_INEXISTENCE(5005,"该权限不存在，请刷新后尝试该操作！"),
    /**
     *
     */
    ILLEGAL_OPERATION(999,"非法操作！请别瞎搞");

    /**
     * 状态码
     */
    private Integer code;

    /**
     * 返回信息
     */
    private String message;

    MyStatus(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public static MyStatus fromCode(Integer code) {
        MyStatus[] statuses = MyStatus.values();
        for (MyStatus status : statuses) {
            if (status.getCode()
                    .equals(code)) {
                return status;
            }
        }
        return SUCCESS;
    }

    @Override
    public String toString() {
        return String.format(" Status:{code=%s, message=%s} ", getCode(), getMessage());
    }

}
