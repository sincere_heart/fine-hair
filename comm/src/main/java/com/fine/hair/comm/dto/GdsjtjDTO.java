package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @author liheng
 * @ClassName GdxhtjDTO
 * @Description
 * @date 2021-03-19 15:28
 */
@ApiModel("各店数据统计  dto")
@Data
public class GdsjtjDTO {
    private List<Long> branchIds;
    /**
     * latitude 纬度 年：year 月：month 日：day（默认值）
     */
    private String latitude = "day";
    /**
     * 日期格式
     * 开始日期同理
     * 年 yyyy 不得大于3年
     * 月 yyyy-MM  不得大于12月
     * 日 yyyy-MM-dd 不得大于31天
     * 结束日期同理
     *
     */
    @NotBlank(message = "开始日期不能为空")
    private String startTime;
    @NotBlank(message = "结束日期不能为空")
    private String endTime;
}
