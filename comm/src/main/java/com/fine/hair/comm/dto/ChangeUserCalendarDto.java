package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/2/7 18:23
 */
@Data
@ApiModel("改变用户周期dto")
public class ChangeUserCalendarDto {
    @ApiModelProperty(value = "用户id", required = true)
    @NotNull(message = "用户id不得为空")
    private String userId;
    @ApiModelProperty(value = "周期id", required = true)
    @NotNull(message = "日历id")
    private String calendarId;
}
