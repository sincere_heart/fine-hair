package com.fine.hair.comm.model;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 请假表
 * </p>
 *
 * @author liheng
 * @since 2021-03-08
 */
@TableName("`leave`")
public class Leave implements Serializable {

    private static final long serialVersionUID=1L;

      @TableId(value = "id",  type = IdType.INPUT)
    private Long id;

    /**
     * 请假操作人
     */
    @TableField("operate_uid")
    private Long operateUid;

    /**
     * (技师)id
     */
    @TableField("technician_id")
    private Long technicianId;

    /**
     * 请假时长，单位天
     */
    @TableField("leave_day")
    private Integer leaveDay;

    /**
     * 请假开始日期
     */
    @TableField("start_time")
    private LocalDateTime startTime;

    /**
     * 请假结束日期
     */
    @TableField("end_time")
    private LocalDateTime endTime;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    @ApiModelProperty(value="创建时间")
    private Date createTime;

    /**
     * 最近一次更新时间
     */
    @TableField(value = "last_update_time")
    @ApiModelProperty(value="最近一次更新时间")
    private Date lastUpdateTime;

    /**
     * 是否删除 0否 1是
     */
    @TableField("`status`")
    @TableLogic
    private Integer status;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOperateUid() {
        return operateUid;
    }

    public void setOperateUid(Long operateUid) {
        this.operateUid = operateUid;
    }

    public Long getTechnicianId() {
        return technicianId;
    }

    public void setTechnicianId(Long technicianId) {
        this.technicianId = technicianId;
    }

    public Integer getLeaveDay() {
        return leaveDay;
    }

    public void setLeaveDay(Integer leaveDay) {
        this.leaveDay = leaveDay;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(Date lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Leave{" +
        "id=" + id +
        ", operateUid=" + operateUid +
        ", technicianId=" + technicianId +
        ", leaveDay=" + leaveDay +
        ", startTime=" + startTime +
        ", endTime=" + endTime +
        ", createTime=" + createTime +
        ", lastUpdateTime=" + lastUpdateTime +
        ", status=" + status +
        "}";
    }
}
