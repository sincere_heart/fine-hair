package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/10/25 20:43
 */
@Data
@ApiModel("添加员工奖惩信息dto实体")
public class AddStaffRewardsPunishmentDto {
    @ApiModelProperty("用户id")
    private String userId;
    @ApiModelProperty("奖惩类型 0奖励 1惩罚 2提成 3现金提成")
    private String type;
    @ApiModelProperty("金额")
    private BigDecimal amount;
    @ApiModelProperty("充值金额")
    private BigDecimal chargeAmount;
    @ApiModelProperty("备注")
    private String remark;
    @ApiModelProperty("录入信息的管理员id")
    private String operatorId;
    @ApiModelProperty("门店id")
    private String branchId;
}
