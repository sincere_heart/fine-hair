package com.fine.hair.comm.utils;

/**
 * <p>
 * 常量池
 * </p>
 *
 * @description: 常量池
 */
public interface Consts {
    /**
     * JWT 在 Redis 中保存的key前缀
     */
    String REDIS_JWT_KEY_PREFIX = "security:jwt:";
    /**
     * 等号
     */
    String EQUAL_SIGN = "=";
    /**
     * 加上一天的时间
     */
    Integer ONE_DAY_MS = 86399999;
    /**
     * 逗号
     */
    String COMMA = ",";
    /**
     * token expire time
     */
    Integer REDIS_AUTHORIZATION_EXPIRE_TIME = 7200;

    /**
     * token
     */
    String REDIS_AUTHORIZATION = "redis.authorization";
    /**
     * QR
     */
    String HLHT = "hlht";
    /**
     * 冒号
     */
    String COLON = ":";
    /**
     * 双引号
     */
    String DOUBLE_QUOTATION_MARKS = "\"";
    /**
     * 点点
     */
    String DOT = ".";
    /**
     * 正斜杠
     */
    String FORWARD_SLASH = "/";
    /**
     *
     */
    String ALGORITHMSTR = "AES/CBC/PKCS5Padding";

    /**
     * token键名
     */
    String AUTHORIZATION = "Authorization";
    /**
     * token前缀
     */
    String BEARER = "Bearer ";
    /**
     * 启用
     */
    Integer ENABLE = 0;
    /**
     * 禁用
     */
    Integer DISABLE = 1;

    /**
     * 邮箱符号
     */
    String SYMBOL_EMAIL = "@";

    /**
     * 默认当前页码
     */
    Integer DEFAULT_CURRENT_PAGE = 1;

    /**
     * 默认每页条数
     */
    Integer DEFAULT_PAGE_SIZE = 10;

    /**
     * rest调用错误
     */
    int RES_CODE_SERVICE_ERROR = 1000;

    /**
     * rest调用成功
     */
    int RES_CODE_SUCCESS = 0;
    /**
     * 微信登录失败
     */
    int RES_CODE_WX_LOGIN_FAIL = 3010;
    /**
     * token
     */
    String TOKEN = "token";
    /**
     * 页面
     */
    Integer PAGE = 1;
    /**
     * 按钮
     */
    Integer BUTTON = 2;
}
