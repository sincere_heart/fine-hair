package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/10/4 15:07
 */
@Data
@ApiModel("提交评价dto")
public class SubmitEvaluateDto {
    @ApiModelProperty("评价对象id")
    private Long technicianId;
    @ApiModelProperty("评价者id")
    private Long evaluateId;
    @ApiModelProperty("满意一分，不满意0分,一个选项1分")
    private Integer score;
    //@ApiModelProperty("评价内容")
    @ApiModelProperty("选项  选项1-5 拼接 是或否1-0  格式 1:0,2:1,3:1,4:0")
    private String content;
    @ApiModelProperty("标签类型集合 1效果显著 2技术专业 3认真负责 4耐心负责 5环境优越")
    private List<String> label;
    @ApiModelProperty("订单id")
    private String orderId;
}
