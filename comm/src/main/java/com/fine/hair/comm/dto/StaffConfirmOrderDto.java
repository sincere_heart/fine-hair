package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/10/29 23:37
 */
@Data
@ApiModel("员工确认订单dto请求体")
public class StaffConfirmOrderDto {
    @ApiModelProperty("订单id")
    private String orderId;
}
