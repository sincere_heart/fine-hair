package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/1/21 20:54
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel("新-订单列表")
public class NewStaffOrderListDTO extends BasePage{
    @ApiModelProperty("时间格式 YYYY-mm-dd")
    private String date;
    @ApiModelProperty("用户id")
    private String userId;
    @ApiModelProperty("订单状态 1未开始 2已完成 3已取消 4已开始")
    private Integer orderStatus;
}
