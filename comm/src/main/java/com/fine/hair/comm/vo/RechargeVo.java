package com.fine.hair.comm.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/2/20 19:47
 */
@Data
@ApiModel("现金业绩")
public class RechargeVo {
    @ApiModelProperty("名字")
    private String name;
    @ApiModelProperty("现金")
    private BigDecimal money;
}
