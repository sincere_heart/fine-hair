package com.fine.hair.comm.pojo.wx;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/9/7 20:55
 */
@Data
@Configuration
@EnableConfigurationProperties
@ConfigurationProperties("wx.key")
public class WxLiteProperty {
    private String appId;
    private String secret;
    private String mchId;
    private String apiSecret;
}
