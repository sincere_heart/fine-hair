package com.fine.hair.comm.dto;

import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/9/9 14:26
 */
@Data
public class WxLoginDDto {
    private String accessToken;
    private String openId;
}
