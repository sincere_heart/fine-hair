package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Pattern;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/11/18 15:10
 */
@Data
@ApiModel("修改手机号dto实体")
public class AlterPhoneNumberDto {
    @ApiModelProperty("用户id")
    private Long userId;
    @ApiModelProperty("新手机号")
    @Pattern(regexp = "^1[3|4|5|7|8][0-9]{9}$",message = "手机号格式不正确")
    private String phoneNumber;
}
