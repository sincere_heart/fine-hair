package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/2/16 23:05
 */
@Data
@ApiModel("")
public class DelStaffDto {
    @ApiModelProperty("用户id")
    private Long userId;
}
