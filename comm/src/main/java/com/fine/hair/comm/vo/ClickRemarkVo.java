package com.fine.hair.comm.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/11/9 23:44
 */
@Data
@ApiModel("编辑备注vo实体")
public class ClickRemarkVo {
    @ApiModelProperty("客户名称")
    private String clientName;
    @ApiModelProperty("客户性别")
    private String clientSex;
    @ApiModelProperty("客户手机号")
    private String phone;
    @ApiModelProperty("客户生日")
    private String birthday;
    @ApiModelProperty("订单备注")
    private String remark;
    @ApiModelProperty("订单号")
    private Long orderId;
}
