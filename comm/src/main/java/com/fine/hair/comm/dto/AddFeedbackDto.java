package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/11/18 15:00
 */
@Data
@ApiModel("添加反馈信息dto实体")
public class AddFeedbackDto {
    @ApiModelProperty("用户id")
    private Long userId;
    @ApiModelProperty("反馈信息")
    private String description;
}
