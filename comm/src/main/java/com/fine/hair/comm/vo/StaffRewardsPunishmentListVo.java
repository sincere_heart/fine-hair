package com.fine.hair.comm.vo;

import com.fine.hair.comm.model.RewardsPunishment;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.util.List;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/10/30 0:09
 */
@Data
@ApiModel("用户查询自己的奖惩列表vo对象，不支持分页")
public class StaffRewardsPunishmentListVo {
    @ApiModelProperty("奖励")
    List<RewardsPunishment> rewardsList;
    @ApiModelProperty("惩罚")
    List<RewardsPunishment> punishmentList;
    @ApiModelProperty("提成")
    List<RewardsPunishment> proportionList;
}
