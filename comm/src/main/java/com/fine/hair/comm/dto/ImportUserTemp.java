package com.fine.hair.comm.dto;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/11/18 15:30
 */
@Data
public class ImportUserTemp {
    @Excel(name = "真实姓名")
    private String realName;
    @Excel(name = "手机号")
    private String phoneNumber;
    @Excel(name = "性别")
    private String sex;
    @Excel(name = "生日")
    private Date birth;
    @Excel(name = "年龄")
    private String age;
    @Excel(name = "初级调理次数")
    private Integer hfCount;
    @Excel(name = "中级调理次数")
    private Integer yfCount;
    @Excel(name = "高级调理次数")
    private Integer fzCount;
    @Excel(name = "顶级调理次数")
    private Integer djCount;
    @Excel(name = "头部调理次数")
    private Integer tbCount;
    @Excel(name = "优惠调理次数")
    private Integer yhCount;
    @Excel(name = "头发状态")
    private String hairStatus;

    public String getHairStatus() {
        return hairStatus;
    }

    public void setHairStatus(String hairStatus) {
        this.hairStatus = hairStatus;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Date getBirth() {
        return birth;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public Integer getHfCount() {
        return hfCount;
    }

    public void setHfCount(Integer hfCount) {
        this.hfCount = hfCount;
    }

    public Integer getYfCount() {
        return yfCount;
    }

    public void setYfCount(Integer yfCount) {
        this.yfCount = yfCount;
    }

    public Integer getFzCount() {
        return fzCount;
    }

    public void setFzCount(Integer fzCount) {
        this.fzCount = fzCount;
    }
}
