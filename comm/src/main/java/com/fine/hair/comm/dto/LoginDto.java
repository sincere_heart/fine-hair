package com.fine.hair.comm.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/10/22 15:01
 */
@Data
public class LoginDto {

    /**
     * 用户名或邮箱或手机号
     */
    @NotBlank(message = "用户名不能为空")
    private String usernameOrPhone;

    /**
     * 密码
     */
    @NotBlank(message = "密码不能为空")
    private String password;

    /**
     * 记住我
     */
    private Boolean rememberMe = true;
}
