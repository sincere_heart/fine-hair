package com.fine.hair.comm.vo.rbac;

import lombok.Data;

import java.util.List;

/**
 * @author junelee
 * @date 2020/7/18 14:50
 */
@Data
public class PermissionVo {
    private String url;
    private String icon;
    private String title;
    private List<PermissionVo> child;
}
