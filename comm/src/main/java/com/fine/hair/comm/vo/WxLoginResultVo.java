package com.fine.hair.comm.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * @author junelee
 * @date 2020/6/27 22:21
 */
@Data
@Accessors(chain = true)
@ToString(callSuper = true)
public class WxLoginResultVo {
    private String id;
    @ApiModelProperty("微信openid")
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String openId;

    @ApiModelProperty("微信unionid")
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String unionId;

    @ApiModelProperty("微信昵称")
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String nickName;

    @ApiModelProperty("微信头像")
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String headIcon;

    private String token;
}
