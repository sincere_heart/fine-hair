package com.fine.hair.comm.enums;

import lombok.Getter;

/**
 * example
 *
 * @author junelee
 * @date 2020/4/24 10:35
 */
@Getter
public enum AppType implements BaseType {

    /**
     * 未知
     */
    UNKNOWN(0, "未知"),

    /**
     * 微信小程序
     */
    WX_LITE(2, "微信小程序"),

    /**
     * 微信unionId
     */
    WX_UNION_ID(3,"微信unionId"),

    /**
     * 微信公众号
     */
    WX_MP(4, "微信公众号");

    /**
     * 状态码
     */
    private final Integer code;
    /**
     * 状态码信息
     */
    private final String message;

    public static AppType fromCode(Integer code) {
        AppType[] resultTypes = AppType.values();
        for (AppType resultType : resultTypes) {
            if (resultType.getCode()
                    .equals(code)) {
                return resultType;
            }
        }
        return UNKNOWN;
    }

    public static AppType fromMessage(String message) {
        AppType[] resultTypes = AppType.values();
        for (AppType resultType : resultTypes) {
            if (resultType.getMessage()
                    .equals(message)) {
                return resultType;
            }
        }
        return UNKNOWN;
    }

    AppType(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

}
