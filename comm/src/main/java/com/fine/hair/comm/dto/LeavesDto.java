package com.fine.hair.comm.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.time.LocalDate;

/**
 * @author liheng
 * @ClassName LeavesDto
 * @Description
 * @date 2021-03-08 16:14
 */
@Data
@ApiModel("请假列表实体")
public class LeavesDto extends BasePage {
    @ApiModelProperty(value = "请假技师id")
    private String id;

    /**
     * 请假时长，单位天
     */
    @ApiModelProperty(value = "请假天数，默认1天")
    private Integer leaveDay;

    /**
     * 请假开始日期
     */
    @ApiModelProperty("查詢开始日期 格式 yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private LocalDate startTime;

    @ApiModelProperty("查詢結束日期 格式 yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private LocalDate endTime;
}
