package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/11/7 0:17
 */
@Data
@ApiModel("员工挂店dto实体")
public class StaffPutBranchDto {
    @ApiModelProperty("员工id")
    private String staffId;
    @ApiModelProperty("门店id")
    private String branchId;
}
