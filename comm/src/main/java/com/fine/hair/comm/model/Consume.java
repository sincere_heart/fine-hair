package com.fine.hair.comm.model;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>${Description}</p>
 *
 * @author mouseyCat
 * @date 2020/10/26 21:39
 */

/**
 * 消费明细表
 */
@ApiModel(value = "消费明细表")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "consume")
public class Consume implements Serializable {
    /**
     * id
     */
    @TableId(value = "id", type = IdType.INPUT)
    @ApiModelProperty(value = "id")
    private Long id;

    /**
     * 用户id
     */
    @TableField(value = "user_id")
    @ApiModelProperty(value = "用户id")
    private Long userId;

    /**
     * 0消费 1充值
     */
    @TableField(value = "consume_type")
    @ApiModelProperty(value = "0消费 1充值")
    private Integer consumeType;

    /**
     * 次数(充值为+ 消费为-)
     */
    @TableField(value = "`count`")
    @ApiModelProperty(value = "次数(充值为+ 消费为-)")
    private Integer count;

    @TableField(value = "surplus_count")
    @ApiModelProperty(value = "剩余次数")
    private Integer surplusCount=0;

    @TableField(exist = false)
    @ApiModelProperty(value = "项目名")
    private String itemName;

    @TableField(value = "sevice_type")
    @ApiModelProperty(value = "服务类型  123456")
    private Integer seviceType;

    /**
     * 项目id 为0时表示充值
     */
    @TableField(value = "item_id")
    @ApiModelProperty(value = "项目id 为0时表示充值")
    private Long itemId;

    /**
     * 消费时间
     */
    @TableField(value = "`time`")
    @ApiModelProperty(value = "消费时间")
    private Date time;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    /**
     * 最近一次更新时间
     */
    @TableField(value = "last_update_time")
    @ApiModelProperty(value = "最近一次更新时间")
    private Date lastUpdateTime;

    /**
     * 是否删除 0否 1是
     */
    @TableLogic
    @TableField(value = "`status`")
    @ApiModelProperty(value = "是否删除 0否 1是")
    private Integer status;


    @TableField(exist = false)
    @ApiModelProperty(value = "项目图片")
    private String image;

    private static final long serialVersionUID = 1L;

    public static final String COL_ID = "id";

    public static final String COL_USER_ID = "user_id";

    public static final String COL_CONSUME_TYPE = "consume_type";

    public static final String COL_COUNT = "count";

    public static final String COL_ITEM_ID = "item_id";

    public static final String COL_TIME = "time";

    public static final String COL_CREATE_TIME = "create_time";

    public static final String COL_LAST_UPDATE_TIME = "last_update_time";

    public static final String COL_STATUS = "status";
}
