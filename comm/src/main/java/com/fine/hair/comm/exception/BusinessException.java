package com.fine.hair.comm.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 业务异常
 * @author liheng
 *
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class BusinessException extends RuntimeException {

    private static final long serialVersionUID = -232388151980021793L;

    /**
     * 自定义异常码
     */
    private Integer code = 0;
    private Object data;

    public BusinessException() {
        super();
    }

    /**
     * 将定义的异常信息赋给异常
     *
     * @param code
     * @param msg
     */
    public BusinessException(Integer code, String msg) {
        super(msg);
        this.code = code;
    }

    public BusinessException(String message, Throwable cause) {
        super(message, cause);
    }

    public BusinessException(String message) {
        super(message);
    }

    public BusinessException(Throwable cause) {
        super(cause);
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }


    @Override
    public String toString() {
        return "BusinessException{" +
                "code='" + code + '\'' +
                '}';
    }
}
