package com.fine.hair.comm.dto.rbac;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author junelee
 * @date 2020/6/10 9:55
 */
@Data
@ApiModel("新增管理员长啊后实体")
public class AddAdminDto {

    @ApiModelProperty("手机号")
    private String phone;
    @ApiModelProperty("登陆账号")
    private String username;
    @ApiModelProperty("密码")
    private String password;
    @ApiModelProperty("账号角色")
    private Long role;
    @ApiModelProperty("指定门店，指定role为门店管理员时需要提供参数,可多门店")
    private List<String> branchId;
}
