package com.fine.hair.comm.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author liheng
 * @since 2021-07-29
 */
@Data
@TableName("service_type_charge")
@ApiModel(value = "ServiceTypeCharge对象", description = "")
public class ServiceTypeCharge {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("充值的服务类型id")
    @TableField("service_type_id")
    private Integer serviceTypeId;

    @ApiModelProperty("充值的服务类型名称")
    @TableField(exist = false)
    private String serviceTypeName;

    @TableField("sec_name")
    @ApiModelProperty("充值人")
    private String secName;

    @TableField("user_id")
    @ApiModelProperty("被充值人")
    private Long userId;

    @ApiModelProperty("充值次数")
    @TableField("charge_count")
    private Integer chargeCount;

    @ApiModelProperty("充值时间")
    @TableField("`create_time`")
    private Date createTime;
    @TableField("`last_update_time`")
    private Date lastUpdateTime;
}
