package com.fine.hair.comm.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/12/23 19:45
 */
@Data
public class IncomeInfo {
    @ApiModelProperty("用户数")
    private String userCount;
    @ApiModelProperty("订单数")
    private String orderCount;
    @ApiModelProperty("充值次数")
    private String moneyCount;
    @ApiModelProperty("日期 格式 yyyy-MM-dd")
    private String date;
}
