package com.fine.hair.comm.model;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>${Description}</p>
 * @author mouseyCat
 * @date 2020/10/26 21:39
 */

/**
 * 申请表
 */
@ApiModel(value = "成为员工申请表")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "apply")
public class Apply implements Serializable {
    /**
     * id
     */
    @TableId(value = "id", type = IdType.INPUT)
    @ApiModelProperty(value = "id")
    private Long id;

    /**
     * 用户id
     */
    @TableField(value = "user_id")
    @ApiModelProperty(value = "用户id")
    private Long userId;

    /**
     * 姓名
     */
    @TableField(value = "nick_name")
    @ApiModelProperty(value = "姓名")
    private String nickName;

    /**
     * 手机号
     */
    @TableField(value = "phone_number")
    @ApiModelProperty(value = "手机号")
    private String phoneNumber;

    /**
     * 门店id
     */
    @TableField(value = "branch_id")
    @ApiModelProperty(value = "门店id")
    private Long branchId;

    /**
     * 0待同意 1同意 2拒绝
     */
    @TableField(value = "apply_status")
    @ApiModelProperty(value = "0待同意 1同意 2拒绝")
    private Integer applyStatus;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    /**
     * 最近一次更新时间
     */
    @TableField(value = "last_update_time")
    @ApiModelProperty(value = "最近一次更新时间")
    private Date lastUpdateTime;

    /**
     * 是否删除 0否 1是
     */
    @TableField(value = "`status`")
    @TableLogic
    @ApiModelProperty(value = "是否删除 0否 1是")
    private Integer status;

    private static final long serialVersionUID = 1L;

    public static final String COL_ID = "id";

    public static final String COL_USER_ID = "user_id";

    public static final String COL_NICK_NAME = "nick_name";

    public static final String COL_PHONE_NUMBER = "phone_number";

    public static final String COL_BRANCH_ID = "branch_id";

    public static final String COL_APPLY_STATUS = "apply_status";

    public static final String COL_CREATE_TIME = "create_time";

    public static final String COL_LAST_UPDATE_TIME = "last_update_time";

    public static final String COL_STATUS = "status";
}
