package com.fine.hair.comm.pojo.wx;

import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/9/7 20:59
 */
@Data
public class WxSendMsg2 {
    private String value;
}
