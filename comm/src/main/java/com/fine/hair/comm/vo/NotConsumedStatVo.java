package com.fine.hair.comm.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/2/23 21:29
 */
@Data
public class NotConsumedStatVo {
    @ApiModelProperty("月份")
    private String time;
    @ApiModelProperty("数量")
    private Long count;
}
