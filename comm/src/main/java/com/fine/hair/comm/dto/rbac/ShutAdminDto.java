package com.fine.hair.comm.dto.rbac;

import lombok.Data;

/**
 * @author junelee
 * @date 2020/6/10 11:29
 */
@Data
public class ShutAdminDto {
    private Long userId;
}
