package com.fine.hair.comm.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/1/11 23:55
 */
@Data
@ApiModel("日历-订单的年月日")
public class SearchCalendarVO {
    @ApiModelProperty("年")
    private Integer year;
    @ApiModelProperty("月")
    private Integer month;
    @ApiModelProperty("日")
    private Integer date;

    private Integer orderStatus;
}
