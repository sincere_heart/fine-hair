package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/10/4 15:24
 */
@Data
@ApiModel("订单列表dto")
public class OrderListDto extends BasePage {
    @ApiModelProperty("未开始1 已完成2")
    private Integer status;
    @ApiModelProperty("用户id")
    private Long userId;
}
