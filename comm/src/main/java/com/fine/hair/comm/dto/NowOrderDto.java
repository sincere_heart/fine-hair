package com.fine.hair.comm.dto;

import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/12/1 0:38
 */
@Data
public class NowOrderDto extends BasePage{
    private Long userId;
}
