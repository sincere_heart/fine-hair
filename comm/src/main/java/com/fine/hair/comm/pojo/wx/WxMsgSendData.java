package com.fine.hair.comm.pojo.wx;

import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/9/7 20:56
 */
@Data
public class WxMsgSendData {
    private String touser;
    private WeappTemplateMsg weapp_template_msg;

}
