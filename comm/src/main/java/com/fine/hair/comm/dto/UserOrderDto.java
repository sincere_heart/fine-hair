package com.fine.hair.comm.dto;

import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/3/3 19:23
 */
@Data
public class UserOrderDto extends BasePage{
    private String userId;
    private Integer orderStatus;
}
