package com.fine.hair.comm.pojo.wx;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author junelee
 * @date 2020/6/28 0:00
 * 小程序获取客户手机号的数据结构
 * <p>
 * https://developers.weixin.qq.com/miniprogram/dev/framework/open-ability/getPhoneNumber.html
 */
@Data
public class WxLiteUserPhoneNumber {

    @ApiModelProperty("用户绑定的手机号（国外手机号会有区号）")
    private String phoneNumber;

    @ApiModelProperty("没有区号的手机号")
    private String purePhoneNumber;

    @ApiModelProperty("区号")
    private String countryCode;

    private String nickName;
    private String avatarUrl;

    private Integer gender;
}
