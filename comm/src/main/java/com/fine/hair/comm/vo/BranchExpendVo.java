package com.fine.hair.comm.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/2/23 21:13
 */
@Data
public class BranchExpendVo {
    @ApiModelProperty("")
    private String time;
    @ApiModelProperty("")
    private BigDecimal amount;
}
