package com.fine.hair.comm.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/2/7 17:52
 */
@Data
@ApiModel("最新订单VO")
public class LatestOrderVO {
    @ApiModelProperty("项目图片")
    private String itemImage;
    @ApiModelProperty("项目名称")
    private String itemTitle;
    @ApiModelProperty("项目金额")
    private String itemPrice;
    @ApiModelProperty("订单执行时间")
    private String executeTime;
    @ApiModelProperty("订单创建时间")
    private String createTime;
}
