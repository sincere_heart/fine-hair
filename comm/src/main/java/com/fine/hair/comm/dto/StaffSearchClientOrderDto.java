package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/11/9 22:51
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel("员工查询客户订单dto实体")
public class StaffSearchClientOrderDto extends BasePage {
    @ApiModelProperty("1正在服务 2未服务 3已完成")
    private Integer mode;
    @ApiModelProperty("员工id")
    private String staffId;
}
