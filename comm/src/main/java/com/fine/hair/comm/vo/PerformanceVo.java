package com.fine.hair.comm.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/2/16 21:58
 */
@Data
@ApiModel("业绩实体")
public class PerformanceVo {
    @ApiModelProperty("第一列数据")
    private BigDecimal countOne;
    @ApiModelProperty("第二列数据")
    private BigDecimal countTwo;
    @ApiModelProperty("第三列数据")
    private BigDecimal countThree;
    @ApiModelProperty("第四列数据")
    private BigDecimal countFour;
    @ApiModelProperty("第五列数据")
    private BigDecimal countFive;
}
