package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/2/23 20:00
 */
@Data
@ApiModel("")
public class BranchExpendDto {
    @ApiModelProperty("门店id")
    private String branchId;
}
