package com.fine.hair.comm.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/10/29 23:16
 */
@Data
@ApiModel("订单提成信息pojo")
public class CommissionPojo {
    @ApiModelProperty(value = "id",notes = "列表有id(修改的情况)就直接传，没有id(新增的情况)就不传")
    private Long id;
    @ApiModelProperty(value="订单数量 区间-开始，一个区间包含开始与结束,比如 1-100，101-200", notes = "比如 1-30单，每单提成20 此处就是 1")
    private Long sectionStart;
    @ApiModelProperty(value="订单数量 区间-结束", notes = "比如 1-30单，每单提成20 此处就是 30")
    private Long sectionEnd;
    @ApiModelProperty(value="每单提成比例", notes = "比如每单提成 20% ，此处就填 10")
    private BigDecimal proportion;
}
