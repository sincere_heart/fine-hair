package com.fine.hair.comm.model;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>${Description}</p>
 * @author mouseyCat
 * @date 2020/10/26 21:39
 */

/**
 * 评价表
 */
@ApiModel(value = "评价表")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "evaluate")
public class Evaluate implements Serializable {
    /**
     * id
     */
    @TableId(value = "id", type = IdType.INPUT)
    @ApiModelProperty(value = "id")
    private Long id;

    @TableField(value = "label")
    private String label;

    /**
     * 评价对象id
     */
    @TableField(value = "technician_id")
    @ApiModelProperty(value = "评价对象id")
    private Long technicianId;

    /**
     * 评价者id
     */
    @TableField(value = "evaluate_id")
    @ApiModelProperty(value = "评价者id")
    private Long evaluateId;

    /**
     * 评价内容
     */
    @TableField(value = "content")
    @ApiModelProperty(value = "评价内容")
    private String content;

    /**
     * 综合满意 1-2-3-4-5 颗星
     */
    @TableField(value = "score")
    @ApiModelProperty(value = "综合满意 1-2-3-4-5 颗星")
    private BigDecimal score;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    /**
     * 最近一次更新时间
     */
    @TableField(value = "last_update_time")
    @ApiModelProperty(value = "最近一次更新时间")
    private Date lastUpdateTime;

    @ApiModelProperty(value = "订单id")
    @TableField(value = "order_id")
    private String orderId;
    /**
     * 是否删除 0否 1是
     */
    @TableField(value = "`status`")
    @TableLogic
    @ApiModelProperty(value = "是否删除 0否 1是")
    private Integer status;

    private static final long serialVersionUID = 1L;

    public static final String COL_ID = "id";

    public static final String COL_TECHNICIAN_ID = "technician_id";

    public static final String COL_EVALUATE_ID = "evaluate_id";

    public static final String COL_CONTENT = "content";

    public static final String COL_SCORE = "score";

    public static final String COL_CREATE_TIME = "create_time";

    public static final String COL_LAST_UPDATE_TIME = "last_update_time";

    public static final String COL_STATUS = "status";
}
