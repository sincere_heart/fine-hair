package com.fine.hair.comm.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>${Description}</p>
 * @author mouseyCat
 * @date 2020/10/26 21:39
 */

/**
 * 权限表
 */
@ApiModel(value = "资源表")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "sec_permission")
public class SecPermission implements Serializable {
    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.INPUT)
    @ApiModelProperty(value = "主键")
    private Long id;

    /**
     * 图标
     */
    @TableField(value = "icon")
    @ApiModelProperty(value = "图标")
    private String icon;

    /**
     * 名称
     */
    @TableField(value = "title")
    @ApiModelProperty(value = "名称")
    private String title;

    /**
     * 类型为页面时，代表前端路由地址，类型为按钮时，代表后端接口地址
     */
    @TableField(value = "url")
    @ApiModelProperty(value = "类型为页面时，代表前端路由地址，类型为按钮时，代表后端接口地址")
    private String url;

    /**
     * 权限类型，页面-1，按钮-2
     */
    @TableField(value = "`type`")
    @ApiModelProperty(value = "权限类型，页面-1，按钮-2")
    private Integer type;

    /**
     * 权限表达式
     */
    @TableField(value = "permission")
    @ApiModelProperty(value = "权限表达式")
    private String permission;

    /**
     * 后端接口访问方式
     */
    @TableField(value = "`method`")
    @ApiModelProperty(value = "后端接口访问方式")
    private String method;

    /**
     * 排序
     */
    @TableField(value = "sort")
    @ApiModelProperty(value = "排序")
    private Integer sort;

    /**
     * 父级id
     */
    @TableField(value = "parent_id")
    @ApiModelProperty(value = "父级id")
    private Long parentId;

    private static final long serialVersionUID = 1L;

    public static final String COL_ID = "id";

    public static final String COL_ICON = "icon";

    public static final String COL_TITLE = "title";

    public static final String COL_URL = "url";

    public static final String COL_TYPE = "type";

    public static final String COL_PERMISSION = "permission";

    public static final String COL_METHOD = "method";

    public static final String COL_SORT = "sort";

    public static final String COL_PARENT_ID = "parent_id";
}
