package com.fine.hair.comm.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/10/4 16:10
 */
@Data
@ApiModel("评价列表信息")
public class EvaluateList {
    @ApiModelProperty("评价者昵称")
    private String evaluateName;
    @ApiModelProperty("评价者头像")
    private String evaluateAvatar;
    @ApiModelProperty("评价者描述")
    private String content;
    @ApiModelProperty("评论者手机号")
    private String phoneNumber;
    @ApiModelProperty("评论时间")
    private String createTime;
}
