package com.fine.hair.comm.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/2/6 0:10
 */
@Data
@ApiModel("")
public class InventoryPageVo {
    @ApiModelProperty("唯一id")
    private Long id;
    @ApiModelProperty("名称")
    private String name;
    @ApiModelProperty("图片")
    private String img;
    @ApiModelProperty("规格")
    private String specification;
    @ApiModelProperty("总数")
    private Integer sum;
    @ApiModelProperty("最后一次入库时间")
    private String lastPutTime;
    @ApiModelProperty("门店名称")
    private String branchName;
}
