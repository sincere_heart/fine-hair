package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/2/20 12:15
 */
@Data
@ApiModel("修改密码")
public class ModifyPasswordDTO {
    @NotNull(message = "用户id不得为空")
    @ApiModelProperty("当前用户id")
    private String userId;
    @NotNull(message = "新密码不得为空")
    @ApiModelProperty("新密码")
    private String password;
}
