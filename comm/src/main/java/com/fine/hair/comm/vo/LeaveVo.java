package com.fine.hair.comm.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author liheng
 * @ClassName LeaveVo
 * @Description
 * @date 2021-03-08 16:17
 */
@Data
@ApiModel("请假VO")
public class LeaveVo {
    private Long id;

    /**
     * 请假操作人
     */
    @ApiModelProperty(value = "请假操作人用户id")
    private Long operateUid;

    /**
     * (技师)id
     */
    @ApiModelProperty(value = "请假技师id")
    private Long technicianId;

    @ApiModelProperty(value = "请假技师姓名")
    private Long technicianName;

    /**
     * 请假时长，单位天
     */
    @ApiModelProperty(value = "请假天数，默认1天")
    private Integer leaveDay;

    /**
     * 请假开始日期
     */
    @ApiModelProperty("请假开始日期 格式 yyyy-MM-dd HH:mm:ss")
    private Date startTime;

    /**
     * 请假结束日期
     */
    @ApiModelProperty("请假结束日期 格式 yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    @ApiModelProperty("请假申请日期 格式 yyyy-MM-dd HH:mm:ss")
    private Date createTime;
}
