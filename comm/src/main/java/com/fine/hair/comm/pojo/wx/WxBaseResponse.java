package com.fine.hair.comm.pojo.wx;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class WxBaseResponse {


    @JsonProperty("errcode")
    private Integer errorCode;

    @JsonProperty("errmsg")
    private String errorMsg;
}
