package com.fine.hair.comm.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/2/21 21:33
 */
@Data
@ApiModel("")
public class FinanceVo {
    @ApiModelProperty("员工名称")
    private String name;
    @ApiModelProperty("销售")
    private BigDecimal sale;
    @ApiModelProperty("销售提成")
    private BigDecimal saleCommission;
    @ApiModelProperty("手工提成")
    private BigDecimal chargeAmountCommission;
    @ApiModelProperty("奖励")
    private BigDecimal reward;
    @ApiModelProperty("惩罚")
    private BigDecimal punishment;
    @ApiModelProperty("合计")
    private BigDecimal total;

}
