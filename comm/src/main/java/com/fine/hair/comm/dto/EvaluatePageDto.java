package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/2/18 13:12
 */
@Data
@ApiModel("")
public class EvaluatePageDto {
    @ApiModelProperty("用户")
    private String userId;
}
