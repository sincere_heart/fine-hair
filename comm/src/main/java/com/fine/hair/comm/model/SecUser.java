package com.fine.hair.comm.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>${Description}</p>
 * @author mouseyCat
 * @date 2020/10/26 21:39
 */

/**
 * 管理员用户表
 */
@ApiModel(value = "管理员表")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "sec_user")
public class SecUser implements Serializable {
    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.INPUT)
    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "门店名称")
    @TableField(exist = false)
    private String branchTitles;

    @ApiModelProperty(value = "角色id")
    @TableField(exist = false)
    private Integer roleId;

    @ApiModelProperty(value = "角色名称")
    @TableField(exist = false)
    private String roleName;

    /**
     * 门店管理员管理的门店id,为空则不是门店管理员
     */
    @TableField(value = "branch_id")
    @ApiModelProperty(value = "门店管理员管理的门店id,为空则不是门店管理员,多门店时逗号隔开")
    private String branchId;

    /**
     * 用户名
     */
    @TableField(value = "username")
    @ApiModelProperty(value = "用户名")
    private String username;

    /**
     * 密码
     */
    @TableField(value = "`password`")
    @ApiModelProperty(value = "密码")
    private String password;

    /**
     * 手机
     */
    @TableField(value = "phone")
    @ApiModelProperty(value = "手机")
    private String phone;

    /**
     * (申请时间)创建时间
     */
    @TableField(value = "create_time")
    @ApiModelProperty(value = "(申请时间)创建时间")
    private Date createTime;

    /**
     * 最近一次更新时间
     */
    @TableField(value = "last_update_time")
    @ApiModelProperty(value = "最近一次更新时间")
    private Date lastUpdateTime;

    /**
     * 是否删除 0否 1是
     */
    @TableField(value = "`status`")
    @ApiModelProperty(value = "是否删除 0否 1是")
    private Integer status;

    private static final long serialVersionUID = 1L;

    public static final String COL_ID = "id";

    public static final String COL_BRANCH_ID = "branch_id";

    public static final String COL_USERNAME = "username";

    public static final String COL_PASSWORD = "password";

    public static final String COL_PHONE = "phone";

    public static final String COL_CREATE_TIME = "create_time";

    public static final String COL_LAST_UPDATE_TIME = "last_update_time";

    public static final String COL_STATUS = "status";
}
