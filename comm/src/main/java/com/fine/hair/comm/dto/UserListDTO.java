package com.fine.hair.comm.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import springfox.documentation.annotations.ApiIgnore;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/2/7 18:08
 */
@Data
@ApiModel("用户列表")
public class UserListDTO extends BasePage{
    @ApiModelProperty("手机号或名称")
    private String phoneOrName;

    @ApiModelProperty(value = "最近7天的用户生日")
    private Boolean birthBySevenDay = Boolean.FALSE;

    @ApiModelProperty("门店id,门店管理员查询时传这个字段")
    private String branchId;


    @JsonIgnore
    private String birthStartTime;

    @JsonIgnore
    private String birthEndTime;
}
