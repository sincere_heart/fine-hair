package com.fine.hair.comm.pojo.wx;

import com.fine.hair.comm.pojo.TxErrData;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/9/7 20:57
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class WxAccessTokenResponse extends TxErrData {
    @SerializedName("access_token")
    private String accessToken;
    @SerializedName("expires_in")
    private Integer expiresIn;

}
