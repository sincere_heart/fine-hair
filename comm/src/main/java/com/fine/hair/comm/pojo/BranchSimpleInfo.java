package com.fine.hair.comm.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/10/9 23:50
 */
@Data
@ApiModel("门店简单信息")
public class BranchSimpleInfo {
    private String branchId;
    @ApiModelProperty("图片")
    private String images;
    @ApiModelProperty("地址")
    private String address;
    @ApiModelProperty("标题")
    private String title;
    @ApiModelProperty("门店热线")
    private String tel;
    @ApiModelProperty("营业时间")
    private String bussinessTime;
    @ApiModelProperty("距离")
    private Double distance;
    @ApiModelProperty("经度")
    private Double lng;
    @ApiModelProperty("维度")
    private Double lat;
}
