package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/1/26 21:58
 */
@Data
@ApiModel("")
public class ModifyRemarkDTO {
    @ApiModelProperty("用户id")
    private String userId;
    @ApiModelProperty("备注")
    private String remark;
}
