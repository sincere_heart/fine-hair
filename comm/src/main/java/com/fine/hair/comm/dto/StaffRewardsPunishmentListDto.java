package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/10/30 0:03
 */
@Data
@ApiModel("用户查询自己的奖惩列表dto请求，不支持分页")
public class StaffRewardsPunishmentListDto {
    @ApiModelProperty("员工id，传userId即可")
    private String staffId;
}
