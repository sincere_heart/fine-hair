package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/10/24 16:31
 */
@Data
@ApiModel("项目dto信息")
public class ItemDto {
    @ApiModelProperty("项目id,修改时传，新增不传")
    private Long itemId;
    @ApiModelProperty("图片")
    private String image;
    @ApiModelProperty("项目名称")
    private String title;
    @ApiModelProperty("项目价格")
    private BigDecimal price;
    @ApiModelProperty("项目描述")
    private String content;
    @ApiModelProperty("项目执行时间")
    private Integer time;
    @ApiModelProperty("项目所属服务类型 1护发服务 2养发服务 3发质服务")
    private Integer type;
    @ApiModelProperty("门店id，所有门店忽略此字段")
    private String branchId;
    @ApiModelProperty("库存")
    private Integer stock;
}
