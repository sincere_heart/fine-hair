package com.fine.hair.comm.pojo.wx;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.ToString;

/**
 * @author junelee
 * @date 2020/9/7 20:51
 *
 * 参考文档
 * https://developers.weixin.qq.com/miniprogram/dev/api-backend/open-api/login/auth.code2Session.html
 */
@Data
@ToString(callSuper = true)
public class WxCode2SessionResponse {
    @SerializedName("openid")
    private String openId;

    @SerializedName("session_key")
    private String sessionKey;

    @SerializedName("unionid")
    private String unionId;

    @SerializedName("errcode")
    private Integer errorCode;

    @SerializedName("errmsg")
    private String errorMsg;
}
