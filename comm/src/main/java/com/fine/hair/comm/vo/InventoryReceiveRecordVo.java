package com.fine.hair.comm.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/2/6 0:20
 */
@Data
@ApiModel("")
public class InventoryReceiveRecordVo {
    @ApiModelProperty("唯一id")
    private Long id;
    @ApiModelProperty("本次领用的本店名称")
    private String branchName;
    @ApiModelProperty("本次领用的用户名")
    private String userName;
    @ApiModelProperty("本次领用数量")
    private Integer  count;
    @ApiModelProperty("领用备注")
    private String remark;
}
