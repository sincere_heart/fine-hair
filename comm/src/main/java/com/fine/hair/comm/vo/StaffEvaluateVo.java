package com.fine.hair.comm.vo;

import com.fine.hair.comm.pojo.EvaluateList;
import com.fine.hair.comm.utils.PageInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/10/4 16:07
 */
@Data
@ApiModel("评价信息vo")
public class StaffEvaluateVo {
    @ApiModelProperty("技师分数")
    private String score;
    @ApiModelProperty("技师id")
    private Long technicianId;
    @ApiModelProperty("技师头像")
    private String technicianAvatar;
    @ApiModelProperty("技师昵称")
    private String technicianName;
    @ApiModelProperty("门店图片")
    private String branchImage;
    @ApiModelProperty("技师个人简介")
    private String resume;
    @ApiModelProperty("技师手机号")
    private String phoneNumber;
    @ApiModelProperty("评价列表")
    private PageInfo<EvaluateList> pageInfo;
}
