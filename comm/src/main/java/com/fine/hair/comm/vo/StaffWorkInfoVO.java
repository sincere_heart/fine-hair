package com.fine.hair.comm.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/2/7 21:54
 */
@Data
@ApiModel("员工工作信息vo")
public class StaffWorkInfoVO {
    @ApiModelProperty("所属门店名称")
    private String selfBranchName;
    @ApiModelProperty("当前挂店名称")
    private String currentPutBranchName;
    @ApiModelProperty("当前工作状态")
    private String currentWorkStatus;
}
