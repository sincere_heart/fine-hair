package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/11/17 21:28
 */
@Data
@ApiModel("批量预约dto实体")
public class AddReservationBatchDto {
    @ApiModelProperty(value = "采用的配置 1 2 3 4 在 sysConfig 里拿 ",required = true)
    private Integer mode;
    @ApiModelProperty(value = "从哪天的几点开始 格式 yyyy-MM-dd HH:mm:ss",required = true)
    private String startTime;
    @ApiModelProperty(value = "用户id",required = true)
    private String userId;
    @ApiModelProperty(value = "门店id", required = true)
    private String branchId;
    @ApiModelProperty(value = "技师id",required = true)
    private String technicianId;
    @ApiModelProperty(value = "项目id",required = true)
    private String itemId;
    @ApiModelProperty("订单列表（疗程）")
    private List<AddReservationBatchList> orderList;
}
