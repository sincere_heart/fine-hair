package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/10/25 21:57
 */
@Data
@ApiModel("添加banner的dto请求体")
public class AddBannerDto {
    @ApiModelProperty("网链")
    private String url;
    @ApiModelProperty("优先级 1>2>3>4")
    private Long priority;
}
