package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/2/4 20:04
 */
@Data
@ApiModel("修改手机号dto")
public class ModifyPhoneDTO {
    @ApiModelProperty("当前用户id")
    private String userId;
    @ApiModelProperty("新手机号")
    private String phone;
}
