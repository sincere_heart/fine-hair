package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/1/26 21:45
 */
@Data
@ApiModel("周期信息dto")
public class InfoListDTO {
    @ApiModelProperty("传周期id")
    private Long configCode;
    @ApiModelProperty("备注关键字")
    private String remarks;
}
