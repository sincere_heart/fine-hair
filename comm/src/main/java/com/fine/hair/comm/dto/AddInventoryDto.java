package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/2/25 22:51
 */
@Data
@ApiModel("")
public class AddInventoryDto {
    @ApiModelProperty("门店id")
    private String branchId;
    @ApiModelProperty("物品数量")
    private Integer count;
    @ApiModelProperty("物品id")
    private String inventoryId;
}
