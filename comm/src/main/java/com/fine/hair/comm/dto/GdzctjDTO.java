package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.List;

/**
 * @author liheng
 * @ClassName GdzctjDTO
 * @Description
 * @date 2021-03-18 17:23
 */
@ApiModel("统计dto")
@Data
public class GdzctjDTO extends BasePage{
    private List<Long> branchIds;
}
