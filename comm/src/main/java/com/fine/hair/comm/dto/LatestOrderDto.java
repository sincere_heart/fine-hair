package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/2/7 17:44
 */
@Data
@ApiModel("最新订单实体")
public class LatestOrderDto {
    @ApiModelProperty(value = "用户id", required = true)
    @NotNull(message = "用户id不得为空")
    private String userId;
}
