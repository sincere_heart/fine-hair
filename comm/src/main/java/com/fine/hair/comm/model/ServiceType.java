package com.fine.hair.comm.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * <p>
 *
 * </p>
 *
 * @author liheng
 * @since 2021-07-29
 */
@Data
@TableName("service_type")
@ApiModel(value = "ServiceType对象", description = "")
public class ServiceType {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField("service_name")
    private String serviceName;

    @TableField("imgae_url")
    private String imgaeUrl;

    @TableField("experience")
    private Boolean experience;
    @TableField("`key`")
    private String key;
}
