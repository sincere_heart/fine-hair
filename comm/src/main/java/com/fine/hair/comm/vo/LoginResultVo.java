package com.fine.hair.comm.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/9/7 21:13
 */
@Data
@Accessors(chain = true)
@ToString(callSuper = true)
public class LoginResultVo {
    @ApiModelProperty("验登录token")
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String token;

    private Long id;
}
