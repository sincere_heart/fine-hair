package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/2/6 0:24
 */
@Data
@ApiModel("")
public class InventoryAddDto {
    private String inventoryId;
    @NotNull(message = "物品名名称不得为空")
    @ApiModelProperty("物品名称")
    private String name;
    @NotNull(message = "物品规格不得为空")
    @ApiModelProperty("物品规格")
    private String specification;
    @ApiModelProperty("物品图片")
    private String img;
    @NotNull(message = "物品总数不得为空")
    @ApiModelProperty("物品总数")
    private Integer sum;
    //@NotNull(message = "总金额不得为空")
    //@ApiModelProperty("总金额")
    //private BigDecimal totalMoney;
    @NotNull(message = "物品单价不得为空")
    @ApiModelProperty("物品单价")
    private BigDecimal money;
    @NotNull(message = "当前用户id不得为空")
    @ApiModelProperty("用户id")
    private Long userId;
    @ApiModelProperty("门店id")
    private String branchId;
    @NotNull(message = "备注不得为空")
    @ApiModelProperty("备注")
    private String remark;
}
