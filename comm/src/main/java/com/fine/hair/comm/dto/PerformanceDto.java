package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/2/16 20:55
 */
@Data
@ApiModel("")
public class PerformanceDto {
    @ApiModelProperty("用户id")
    private Long userId;
    @ApiModelProperty("查询的年份，格式 yyyy")
    private Integer year;
}
