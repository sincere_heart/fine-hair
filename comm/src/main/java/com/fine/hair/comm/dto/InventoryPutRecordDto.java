package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/2/6 0:12
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel("")
public class InventoryPutRecordDto extends BasePage{
    @ApiModelProperty(value = "物品id",required = true)
    @NotNull(message = "物品id不得为空")
    private Long id;
}
