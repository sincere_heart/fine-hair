package com.fine.hair.comm.enums;

import lombok.Getter;

/**
 * example
 *
 * @author junelee
 * @date 2020/4/24 10:35
 */
@Getter
public enum LabelType implements BaseType {

    /**
     * 未知
     */
    UNKNOWN(0, "未知"),

    /**
     * 1效果显著
     */
    ONE(1, "效果显著"),

    /**
     * 2技术专业
     */
    TWO(2,"技术专业"),
    /**
     * 3认真负责
     */
    THREE(3,"认真负责"),
    /**
     * 4耐心负责
     */
    FOUR(4,"耐心负责"),

    /**
     * 5环境优越
     */
    FIVE(5, "环境优越");

    /**
     * 状态码
     */
    private final Integer code;
    /**
     * 状态码信息
     */
    private final String message;

    public static LabelType fromCode(Integer code) {
        LabelType[] resultTypes = LabelType.values();
        for (LabelType resultType : resultTypes) {
            if (resultType.getCode()
                    .equals(code)) {
                return resultType;
            }
        }
        return UNKNOWN;
    }

    public static LabelType fromMessage(String message) {
        LabelType[] resultTypes = LabelType.values();
        for (LabelType resultType : resultTypes) {
            if (resultType.getMessage()
                    .equals(message)) {
                return resultType;
            }
        }
        return UNKNOWN;
    }

    LabelType(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

}
