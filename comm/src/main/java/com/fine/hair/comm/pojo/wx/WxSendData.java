package com.fine.hair.comm.pojo.wx;

import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/9/7 20:58
 */
@Data
public class WxSendData {
    private String access_token;
    private String touser;
    private String template_id;
    private WxSendMsg data;
}
