package com.fine.hair.comm.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/12/3 15:36
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class ConsumeListDto extends BasePage {
    private Long userId;
}
