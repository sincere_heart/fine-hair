package com.fine.hair.comm.utils;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import java.io.Serializable;
import java.util.List;

/**
 * @Author liheng
 * @Date 2019/08/26 10:28 AM
 * @Description
 */
public class PageInfo<T> extends Page<T> implements Serializable {
    private static final long serialVersionUID = 1L;
    private boolean hasNextPage;
    private boolean hasPrevPage;
    private long startIndex;

    public PageInfo(long currentPage, long pageShowCount) {
        super(currentPage, pageShowCount);
        this.startIndex = super.offset();
    }

    @Override
    public PageInfo<T> setRecords(List<T> records) {
        super.setRecords(records);
        this.hasNextPage = super.hasNext();
        this.hasPrevPage = super.hasPrevious();
        return this;
    }

    public boolean getHasNextPage() {
        return hasNextPage;
    }

    public void setHasNextPage(boolean hasNextPage) {
        this.hasNextPage = hasNextPage;
    }

    public boolean getHasPrevPage() {
        return hasPrevPage;
    }

    public void setHasPrevPage(boolean hasPrevPage) {
        this.hasPrevPage = hasPrevPage;
    }

    private boolean hasNextPage(long currentPage, long totalPage) {
        return currentPage >= totalPage || totalPage == 0 ? false : true;
    }

    private static boolean hasPrevPage(long currentPage) {
        return currentPage == 1 ? false : true;
    }

    public long getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(long startIndex) {
        this.startIndex = startIndex;
    }

}
