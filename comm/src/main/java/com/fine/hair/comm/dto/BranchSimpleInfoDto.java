package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/10/9 23:11
 */
@Data
@ApiModel("门店简单信息列表dto请求体")
public class BranchSimpleInfoDto {
    @NotNull(message = "经度不得为空")
    @ApiModelProperty("经度")
    private Double branchLng;
    @NotNull(message = "维度不得为空")
    @ApiModelProperty("维度")
    private Double branchLat;
    @NotNull(message = "页数不得为空")
    @ApiModelProperty("页数")
    private Integer pageCurr;
    @NotNull(message = "条数不得为空")
    @ApiModelProperty("展示条数")
    private Integer pageSize;
}
