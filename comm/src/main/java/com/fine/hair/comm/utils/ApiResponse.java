package com.fine.hair.comm.utils;

import com.fine.hair.comm.exception.BusinessException;
import com.fine.hair.comm.exception.SecurityException;
import lombok.Data;
import java.io.Serializable;

/**
 * <p>
 * 通用的 API 接口封装
 * </p>
 *
 * @description: 通用的 API 接口封装
 */
@Data
public class ApiResponse<T> implements Serializable {
    private static final long serialVersionUID = 8993485788201922830L;

    /**
     * 状态码
     */
    private Integer code;

    /**
     * 返回内容
     */
    private String message;

    /**
     * 返回数据
     */
    private T data;

    /**
     * 无参构造函数
     */
    private ApiResponse() {

    }

    /**
     * 全参构造函数
     *
     * @param code    状态码
     * @param message 返回内容
     * @param data    返回数据
     */
    private ApiResponse(Integer code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    /**
     * 构造一个自定义的API返回
     *
     * @param code    状态码
     * @param message 返回内容
     * @param data    返回数据
     * @return ApiResponse
     */
    public static ApiResponse of(Integer code, String message, Object data) {
        return new ApiResponse(code, message, data);
    }

    /**
     * 构造一个成功且不带数据的API返回
     *
     * @return ApiResponse
     */
    public static ApiResponse ofSuccess() {
        return ofSuccess(null);
    }

    /**
     * 构造一个成功且带数据的API返回
     *
     * @param data 返回数据
     * @return ApiResponse
     */
    public static ApiResponse ofSuccess(Object data) {
        return ofStatus(MyStatus.SUCCESS, data);
    }

    /**
     * 构造一个成功且自定义消息的API返回
     *
     * @param message 返回内容
     * @return ApiResponse
     */
    public static ApiResponse ofMessage(String message) {
        return of(MyStatus.SUCCESS.getCode(), message, null);
    }

    /**
     * 构造一个失败且自定义消息的API返回
     *
     * @param message 返回内容
     * @return ApiResponse
     */
    public static ApiResponse ofFailed(String message) {
        return of(MyStatus.FAILED.getCode(), message, null);
    }

    /**
     * 构造一个有状态的API返回
     *
     * @param status 状态 {@link MyStatus}
     * @return ApiResponse
     */
    public static ApiResponse ofStatus(MyStatus status) {
        return ofStatus(status, null);
    }

    /**
     * 构造一个有状态且带数据的API返回
     *
     * @param status 状态 {@link MyIStatus}
     * @param data   返回数据
     * @return ApiResponse
     */
    public static ApiResponse ofStatus(MyIStatus status, Object data) {
        return of(status.getCode(), status.getMessage(), data);
    }

    /**
     * 构造一个异常的API返回
     *
     * @param t   异常
     * @param <T> {@link BaseException} 的子类
     * @return ApiResponse
     */
    public static <T extends BaseException> ApiResponse ofException(T t) {
        return of(t.getCode(), t.getMessage(), t.getData());
    }

    /**
     * 返回自定义异常信息
     *
     * @param e 自定义异常
     * @return ApiResponse
     */
    public static ApiResponse ofError(Throwable e) {
        if (e instanceof SecurityException) {
            SecurityException exception = (SecurityException) e;
            return of(exception.getCode(), exception.getMessage(), null);
        }
        if (e instanceof BaseException) {
            BaseException exception = (BaseException) e;
            return of(exception.getCode(), exception.getMessage(), null);
        }
        if (e instanceof BusinessException) {
            BusinessException exception = (BusinessException) e;
            return of(exception.getCode(), exception.getMessage(), null);
        }

        return of(-1, "请求失败", null);
    }

}
