package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/1/10 11:59
 */
@Data
@ApiModel("服务过用户的员工列表")
public class ServicedStaffDto extends BasePage {
    @ApiModelProperty("用户id")
    private String userId;
    @ApiModelProperty("门店id")
    private String branchId;
}
