package com.fine.hair.comm.dto.rbac;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author junelee
 * @date 2020/7/6 17:23
 */
@Data
public class ModifyPasswordDto {
    @NotNull(message = "新密码不得为空")
    private String newPassword;
}
