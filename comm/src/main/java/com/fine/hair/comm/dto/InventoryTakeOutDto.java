package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/2/6 0:32
 */
@Data
@ApiModel("")
public class InventoryTakeOutDto {
    @NotNull(message = "被领用的物品id不得为空")
    @ApiModelProperty("被领用的物品id")
    private Long id;
    @NotNull(message = "用户id不得为空")
    @ApiModelProperty("用户id")
    private Long userId;
    @NotNull(message = "本次领用的门店id不得为空")
    @ApiModelProperty("本次领用的门店id")
    private Long branchId;
    @NotNull(message = "本次领用的数量不得为空")
    @ApiModelProperty("本次领用的数量")
    private Integer count;
    @NotNull(message = "本次领用的备注不得为空")
    @ApiModelProperty("本次领用的备注")
    private String remark;
}
