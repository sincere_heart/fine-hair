package com.fine.hair.comm.dto.wx;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author junelee
 * @date 2020/8/7 14:44
 */
@Data
@ApiModel("微信登陆实体")
public class WxLoginDto {
    @ApiModelProperty(value = "微信小程序获取到的加密数据,必传", required = true)
    private String encData;
    @ApiModelProperty(value = "微信小程序获取到的加密算法的初始向量,必传", required = true)
    private String iv;
    @ApiModelProperty(value = "微信小程序登录后获取到的 js_code,必传", required = true)
    private String code;
    @ApiModelProperty(value = "小程序用户 openId,必传", required = true)
    private String openId;
}
