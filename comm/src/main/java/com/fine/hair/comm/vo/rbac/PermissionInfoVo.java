package com.fine.hair.comm.vo.rbac;

import lombok.Data;

import java.util.List;

/**
 * @author junelee
 * @date 2020/7/18 14:39
 */
@Data
public class PermissionInfoVo {
    private Long id;
    private String title;
    private String icon;
    private String url;
    private Long parentId;
    private List<PermissionInfoVo> child;
}
