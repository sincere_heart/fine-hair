package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/1/26 22:00
 */
@Data
@ApiModel("")
public class AddUserDTO {
    @ApiModelProperty("用户id")
    private String userId;
    @ApiModelProperty("传周期id")
    private Long configCode;
}
