package com.fine.hair.comm.enums;

import lombok.Getter;

/**
 * example
 *
 * @author junelee
 * @date 2020/4/24 10:35
 */
@Getter
public enum StatusType implements BaseType {

    /**
     * 未删除
     */
    NORMAL(0, "未删除"),

    /**
     * 删除
     */
    ABNORMAL(1, "删除");

    /**
     * 状态码
     */
    private final Integer code;
    /**
     * 状态码信息
     */
    private final String message;

    public static StatusType fromCode(Integer code) {
        StatusType[] resultTypes = StatusType.values();
        for (StatusType resultType : resultTypes) {
            if (resultType.getCode()
                    .equals(code)) {
                return resultType;
            }
        }
        return ABNORMAL;
    }

    public static StatusType fromMessage(String message) {
        StatusType[] resultTypes = StatusType.values();
        for (StatusType resultType : resultTypes) {
            if (resultType.getMessage()
                    .equals(message)) {
                return resultType;
            }
        }
        return ABNORMAL;
    }

    StatusType(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

}
