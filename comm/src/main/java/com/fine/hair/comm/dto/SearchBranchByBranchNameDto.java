package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/2/7 18:33
 */
@Data
@ApiModel("根据门店名称查询门店dto")
public class SearchBranchByBranchNameDto {
    @ApiModelProperty("门店名称")
    private String branchName;
}
