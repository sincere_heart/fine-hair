package com.fine.hair.comm.pojo.wx;

import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/9/7 20:56
 */
@Data
public class WeappTemplateMsg {
    private String template_id;
    private String page;
    private String form_id;
    private WeappTemplateMsgData data;
    private String emphasis_keyword;
}
