package com.fine.hair.comm.dto;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/10/25 20:11
 */

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel("用户奖惩信息列表")
public class StaffRewardsPunishmentInfoDto extends BasePage {
    @ApiModelProperty("用户id")
    private String userId;
    @ApiModelProperty("0奖励 1惩罚 2销售提成 3现金提成")
    private Integer type;
    @ApiModelProperty("开始时间")
    private String startTime;
    @ApiModelProperty("结束时间")
    private String endTime;
}
