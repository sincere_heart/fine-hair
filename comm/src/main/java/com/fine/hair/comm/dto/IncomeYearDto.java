package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/2/23 21:35
 */
@Data
public class IncomeYearDto {
    @ApiModelProperty("")
    private String branchId;
}
