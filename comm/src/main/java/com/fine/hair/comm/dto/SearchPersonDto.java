package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/11/8 13:27
 */
@Data
@ApiModel("查询用户")
public class SearchPersonDto {
    @ApiModelProperty("手机号或微信昵称")
    private String phoneOrWechatName;
}
