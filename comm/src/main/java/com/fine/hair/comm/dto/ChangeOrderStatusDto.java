package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/12/21 21:33
 */
@Data
@ApiModel("更改订单状态dto")
public class ChangeOrderStatusDto {
    @ApiModelProperty("订单id")
    private String orderId;
    @ApiModelProperty("订单状态 订单状态 1未开始 2已完成 3已取消 4已开始，此处传 2和4即可，不考虑1和3")
    private String orderStatus;
}
