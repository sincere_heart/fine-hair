package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/12/4 0:03
 */
@Data
@ApiModel("查询日历dto")
public class SearchCalendarDto {
    @ApiModelProperty("日期，格式 yyyy-MM")
    private String date;
    @ApiModelProperty("员工id")
    private Long userId;

    private Long staffId;
    private Long customerId;
}
