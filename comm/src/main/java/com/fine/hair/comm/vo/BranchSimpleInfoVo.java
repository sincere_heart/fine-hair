package com.fine.hair.comm.vo;

import com.fine.hair.comm.pojo.BranchSimpleInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/10/9 23:21
 */
@Data
@ApiModel("简单信息vo")
public class BranchSimpleInfoVo {
    @ApiModelProperty("门店列表")
    private List<BranchSimpleInfo> list;
    @ApiModelProperty("总条数")
    private Integer total;
}
