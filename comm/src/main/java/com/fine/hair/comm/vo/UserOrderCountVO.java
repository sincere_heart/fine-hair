package com.fine.hair.comm.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/3/3 19:28
 */
@Data
public class UserOrderCountVO {
    @ApiModelProperty("未开始数量")
    private Integer willStartCount;
    @ApiModelProperty("已开始数量")
    private Integer startingCount;
    @ApiModelProperty("已完成数量")
    private Integer completedCount;
    @ApiModelProperty("已取消数量")
    private Integer cancelCount;
}
