package com.fine.hair.comm.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/10/8 21:15
 */
@Data
@ApiModel("技师简单信息")
public class StaffSimpleInfo {
    @ApiModelProperty("技师头像")
    private String technicianAvatar;
    @ApiModelProperty("技师手机号")
    private String technicianPhoneNumber;
    @ApiModelProperty("技师昵称")
    private String technicianName;
    @ApiModelProperty("技师简介")
    private String resume;
    @ApiModelProperty("技师分数")
    private String score;
    @ApiModelProperty("技师id")
    private String technicianId;
}
