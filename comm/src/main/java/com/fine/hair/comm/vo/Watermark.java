package com.fine.hair.comm.vo;

import lombok.Data;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/1/13 22:58
 */
@Data
public class Watermark {
    private String timestamp;
    private String appid;
}
