package com.fine.hair.comm.model;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>${Description}</p>
 * @author mouseyCat
 * @date 2020/10/29 23:48
 */

/**
 * 订单提成表
 */
@ApiModel(value = "com-lingser-model-Commission")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "commission")
public class Commission implements Serializable {
    /**
     * id
     */
    @TableId(value = "id", type = IdType.INPUT)
    @ApiModelProperty(value = "id")
    private Long id;

    /**
     * 区间-开始，一个区间包含开始与结束,比如 1-100，101-200
     */
    @TableField(value = "section_start")
    @ApiModelProperty(value = "区间-开始，一个区间包含开始与结束,比如 1-100，101-200")
    private Long sectionStart;

    /**
     * 区间-结束
     */
    @TableField(value = "section_end")
    @ApiModelProperty(value = "区间-结束")
    private Long sectionEnd;

    /**
     * 每单提成比例 填 10 就是 每单的 10%
     */
    @TableField(value = "proportion")
    @ApiModelProperty(value = "每单提成比例 填 10 就是 每单的 10%")
    private BigDecimal proportion;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    /**
     * 最近一次更新时间
     */
    @TableField(value = "last_update_time")
    @ApiModelProperty(value = "最近一次更新时间")
    private Date lastUpdateTime;

    /**
     * 是否删除 0否 1是
     */
    @TableField(value = "`status`")
    @TableLogic
    @ApiModelProperty(value = "是否删除 0否 1是")
    private Integer status;

    private static final long serialVersionUID = 1L;

    public static final String COL_ID = "id";

    public static final String COL_SECTION_START = "section_start";

    public static final String COL_SECTION_END = "section_end";

    public static final String COL_PROPORTION = "proportion";

    public static final String COL_CREATE_TIME = "create_time";

    public static final String COL_LAST_UPDATE_TIME = "last_update_time";

    public static final String COL_STATUS = "status";
}
