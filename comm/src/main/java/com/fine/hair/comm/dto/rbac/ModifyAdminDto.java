package com.fine.hair.comm.dto.rbac;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author junelee
 * @date 2020/6/10 10:32
 */
@Data
@ApiModel("编辑管理员信息")
public class ModifyAdminDto {
    @ApiModelProperty("角色id")
    @NotNull(message = "角色id不得为空")
    private Long role;
    @ApiModelProperty("管理员id")
    @NotNull(message = "管理员不得为空")
    private Long secUserId;
    @ApiModelProperty("门店id")
    @NotNull(message = "门店id不得为空")
    private List<String> branchId;
}
