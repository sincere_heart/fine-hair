package com.fine.hair.comm.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/10/25 23:22
 */
@Data
@ApiModel("商铺统计vo对象")
public class IncomeStatVo {
    @ApiModelProperty("门店员工列表")
    private List<IncomeStat> list;
    @ApiModelProperty("店铺总收入")
    private BigDecimal totalIncome;
}
