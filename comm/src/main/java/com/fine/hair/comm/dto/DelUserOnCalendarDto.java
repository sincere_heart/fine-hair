package com.fine.hair.comm.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/2/7 18:01
 */
@Data
@ApiModel("删除用户日历")
public class DelUserOnCalendarDto {
    @ApiModelProperty(value = "用户id",required = true)
    @NotNull(message = "用户id不得为空")
    private Long userId;
}
