package com.fine.hair.comm.enums;

import lombok.Getter;

/**
 * example
 *
 * @author junelee
 * @date 2020/4/24 10:35
 */
@Getter
public enum RewardsPunishmentType implements BaseType {

    /**
     * 奖励
     */
    AWARD(0, "奖励"),

    /**
     * 惩罚
     */
    PUNISHMENT(1, "惩罚"),

    /**
     * 提成
     */
    PROPORTION(2, "提成"),
    /**
     * 现金提成
     */
    REAL_MONEY_PROPORTION(3, "现金提成");

    /**
     * 状态码
     */
    private final Integer code;
    /**
     * 状态码信息
     */
    private final String message;

    public static RewardsPunishmentType fromCode(Integer code) {
        RewardsPunishmentType[] resultTypes = RewardsPunishmentType.values();
        for (RewardsPunishmentType resultType : resultTypes) {
            if (resultType.getCode()
                    .equals(code)) {
                return resultType;
            }
        }
        return AWARD;
    }

    public static RewardsPunishmentType fromMessage(String message) {
        RewardsPunishmentType[] resultTypes = RewardsPunishmentType.values();
        for (RewardsPunishmentType resultType : resultTypes) {
            if (resultType.getMessage()
                    .equals(message)) {
                return resultType;
            }
        }
        return AWARD;
    }

    RewardsPunishmentType(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

}
