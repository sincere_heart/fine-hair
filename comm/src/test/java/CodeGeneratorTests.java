
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.po.LikeTable;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import org.junit.Test;

/**
 * 通过指定 @SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE) 可达到加速的效果。
 * 这时测试类启动时就只会初始化 Spring 上下文，不再启动 Tomcat 容器
 */
public class CodeGeneratorTests {

    private static String JDBC_URL = "jdbc:mysql://132.232.100.229:3306/hqs?useSSL=false&useUnicode=true&characterEncoding=UTF-8&allowPublicKeyRetrieval=true&serverTimezone=GMT%2B8";
    private static String DRIVER_NAME = "com.mysql.cj.jdbc.Driver";
    private static String USER_NAME = "root";
    private static String PASSWORD = "Haoqingsi@12105";
    private static String BASE_PACKAGE = "com.fine.hair.comm";

    @Test
    public void contextLoads() {
        // 代码生成器
        AutoGenerator mpg = new AutoGenerator();

        // 全局配置
        GlobalConfig gc = new GlobalConfig();
        String projectPath = System.getProperty("user.dir");
        gc.setOutputDir(projectPath + "/src/main/java");
        gc.setAuthor("liheng");
        gc.setFileOverride(true);
        gc.setServiceName("%sService");
        gc.setServiceImplName("%sServiceImpl");
        gc.setOpen(false);//当代码生成完成之后是否打开代码所在的文件夹
        // gc.setSwagger2(true); 实体属性 Swagger2 注解
        gc.setFileOverride(true);//是否覆盖
        //  gc.setActiveRecord(true);
        gc.setEnableCache(false);// XML 二级缓存
        gc.setBaseResultMap(true);// XML ResultMap
        gc.setBaseColumnList(true);// XML columList
        mpg.setGlobalConfig(gc);

        // 数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setUrl(JDBC_URL);
        // dsc.setSchemaName("public");
        dsc.setDriverName(DRIVER_NAME);
        dsc.setUsername(USER_NAME);
        dsc.setPassword(PASSWORD);
        mpg.setDataSource(dsc);

        // 包配置
        PackageConfig pc = new PackageConfig();
        //  pc.setModuleName(scanner("模块名"));
        // pc.setModuleName("sys");
        pc.setParent(BASE_PACKAGE);//controller entity  service  service.impl
        // pc.setController("controller");
        pc.setEntity("domain");
        pc.setMapper("mapper");
        pc.setService("service");
        pc.setServiceImpl("service.impl");
        pc.setXml("mapping");
        mpg.setPackageInfo(pc);

        // 配置模板
        // 不生成controller
//        TemplateConfig templateConfig = new TemplateConfig();
//        templateConfig.setController(null);
//        mpg.setTemplate(templateConfig);

        // 策略配置
        StrategyConfig strategy = new StrategyConfig();
        //设置字段和表名的是否把下划线完成驼峰命名规则
        strategy.setNaming(NamingStrategy.underline_to_camel);
        strategy.setEntitySerialVersionUID(true);
        strategy.setEntityColumnConstant(false);

        //设置生成的实体类继承的父类
        // strategy.setSuperEntityClass("com.sxt.BaseEntity");
        strategy.setEntityLombokModel(false);//是否启动lombok
        strategy.setRestControllerStyle(true);//是否生成resetController
//        strategy.setLogicDeleteFieldName("status");
        // 公共父类
        // strategy.setSuperControllerClass("com.sxt.BaseController");
        // 写于父类中的公共字段
        // strategy.setSuperEntityColumns("person_id","person_name");
        //要设置生成哪些表 如果不设置就是生成所有的表
        // strategy.setInclude(scanner("表名，多个英文逗号分割").split(","));
        strategy.setControllerMappingHyphenStyle(true);
        strategy.setEntityTableFieldAnnotationEnable(true);
        // strategy.setTablePrefix(pc.getModuleName() + "_");
        strategy.setLikeTable(new LikeTable("service_type"));
        // strategy.setTablePrefix("sys_");
        mpg.setStrategy(strategy);
        mpg.execute();
    }

}
