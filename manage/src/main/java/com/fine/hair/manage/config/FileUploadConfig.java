package com.fine.hair.manage.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author junelee
 * @date 2020/5/11 13:42
 */
@Data
@Component
@ConfigurationProperties(prefix = "file.upload")
public class FileUploadConfig {
    private String allowExt;
    private String virtualAccessPath;
    private String location;
}
