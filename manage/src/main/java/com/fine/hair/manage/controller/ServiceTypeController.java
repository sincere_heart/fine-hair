package com.fine.hair.manage.controller;


import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fine.hair.comm.model.ServiceType;
import com.fine.hair.comm.model.ServiceTypeCharge;
import com.fine.hair.comm.model.User;
import com.fine.hair.comm.utils.ApiResponse;
import com.fine.hair.manage.dto.ServiceChargeDTO;
import com.fine.hair.manage.mapper.SecUserMapper;
import com.fine.hair.manage.mapper.ServiceTypeChargeMapper;
import com.fine.hair.manage.mapper.ServiceTypeMapper;
import com.fine.hair.manage.mapper.UserMapper;
import com.fine.hair.manage.util.JwtUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author liheng
 * @since 2021-07-29
 */
@Api(tags = {"服务类型相关接口"})
@RestController
@RequestMapping("/service-type/")
public class ServiceTypeController {
    @Autowired
    private ServiceTypeMapper serviceTypeMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private SecUserMapper secUserMapper;
    @Autowired
    private ServiceTypeChargeMapper serviceTypeChargeMapper;
    @Autowired
    private JwtUtil jwtUtil;

    @ApiOperation("获取服务类型")
    @GetMapping("dist")
    public ApiResponse<ServiceType> list() {
        return ApiResponse.ofSuccess(serviceTypeMapper.selectList(Wrappers.emptyWrapper()));
    }

    @ApiOperation("更新服务类型")
    @PostMapping("update")
    public ApiResponse update(@RequestBody ServiceType serviceType) {
        serviceTypeMapper.updateById(serviceType);
        return ApiResponse.ofSuccess();
    }

    @ApiOperation("服务类型次数充值")
    @PostMapping("charge")
    @Transactional(rollbackFor = Exception.class)
    public ApiResponse charge(HttpServletRequest request, @RequestBody ServiceChargeDTO dto) {
        String username = jwtUtil.getUsernameFromJWT(jwtUtil.getJwtFromRequest(request));
        User user = userMapper.selectById(dto.getUserId());
        User newUser = new User();
        newUser.setId(dto.getUserId());
        newUser.setHfCount((user.getHfCount() == null ? 0 : user.getHfCount()) + dto.getHf_count());
        newUser.setYfCount((user.getYfCount() == null ? 0 : user.getYfCount()) + dto.getYf_count());
        newUser.setFzCount((user.getFzCount() == null ? 0 : user.getFzCount()) + dto.getFz_count());
        newUser.setDjCount((user.getDjCount() == null ? 0 : user.getDjCount()) + dto.getDj_count());
        newUser.setTbCount((user.getTbCount() == null ? 0 : user.getTbCount()) + dto.getTb_count());
        newUser.setYhCount((user.getYhCount() == null ? 0 : user.getYhCount()) + dto.getYh_count());
        userMapper.updateById(newUser);
        ServiceTypeCharge serviceTypeCharge = new ServiceTypeCharge();
        serviceTypeCharge.setUserId(Long.valueOf(dto.getUserId()));
       System.out.println(Objects.nonNull(user.getHfCount()));
        if (Objects.nonNull(user.getHfCount()) && newUser.getHfCount() != 0) {
            serviceTypeCharge.setServiceTypeId(1);
            serviceTypeCharge.setChargeCount(dto.getHf_count());
        }
        if (Objects.nonNull(user.getYfCount()) && newUser.getYfCount() != 0) {
            serviceTypeCharge.setServiceTypeId(2);
            serviceTypeCharge.setChargeCount(dto.getYf_count());
        }
        if (Objects.nonNull(user.getFzCount()) && newUser.getFzCount() != 0) {
            serviceTypeCharge.setServiceTypeId(3);
            serviceTypeCharge.setChargeCount(dto.getFz_count());
        }
        if (Objects.nonNull(user.getDjCount()) && newUser.getDjCount() != 0) {
            serviceTypeCharge.setServiceTypeId(4);
            serviceTypeCharge.setChargeCount(dto.getDj_count());
        }
        if (Objects.nonNull(user.getTbCount()) && newUser.getTbCount() != 0) {
            serviceTypeCharge.setServiceTypeId(5);
            serviceTypeCharge.setChargeCount(dto.getTb_count());
        }
        if (Objects.nonNull(user.getYhCount()) && newUser.getYhCount() != 0) {
            serviceTypeCharge.setServiceTypeId(6);
            serviceTypeCharge.setChargeCount(dto.getYh_count());
        }
        serviceTypeCharge.setSecName(username);
        serviceTypeChargeMapper.insert(serviceTypeCharge);
        // 充值记录
        return ApiResponse.ofSuccess();
    }
}

