package com.fine.hair.manage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fine.hair.comm.dto.InventoryReceiveRecordDto;
import com.fine.hair.comm.model.InventoryReceiveRecord;
import com.fine.hair.comm.utils.PageInfo;
import com.fine.hair.comm.vo.InventoryReceiveRecordVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>${Description}</p>
 *
 * @author mouseyCat
 * @date 2021/2/5 23:58
 */
@Mapper
public interface InventoryReceiveRecordMapper extends BaseMapper<InventoryReceiveRecord> {
    List<InventoryReceiveRecordVo> list(@Param("dto") InventoryReceiveRecordDto dto,
                                        @Param("objectPageInfo") PageInfo<InventoryReceiveRecordVo> objectPageInfo);

    List<InventoryReceiveRecord> selectTodayBranch(Long branchId);


    List<InventoryReceiveRecord> selectThisYearBranch(Long branchId);

    List<InventoryReceiveRecord> selectByStartAndEndTime(@Param("startTime") String startTime,
                                                         @Param("endTime") String endTime,
                                                         @Param("branchId") String branchId);

    List<InventoryReceiveRecord> selectListByDate(@Param("date") String date, @Param("branchId") String branchId);
}
