package com.fine.hair.manage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fine.hair.comm.model.Item;
import com.fine.hair.comm.utils.PageInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>${Description}</p>
 *
 * @author mouseyCat
 * @date 2020/10/26 21:39
 */
@Mapper
public interface ItemMapper extends BaseMapper<Item> {
    /**
     *
     * @return
     */
    List<Item> selectListDistinct();
}
