package com.fine.hair.manage.service.impl;

import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fine.hair.comm.dto.wx.WxLoginDto;
import com.fine.hair.comm.enums.AppType;
import com.fine.hair.comm.exception.BusinessException;
import com.fine.hair.comm.model.User;
import com.fine.hair.comm.model.UserLoginAccount;
import com.fine.hair.comm.pojo.wx.WxCode2SessionResponse;
import com.fine.hair.comm.redis.RedisService;
import com.fine.hair.comm.utils.ApiResponse;
import com.fine.hair.comm.vo.LoginResultVo;
import com.fine.hair.comm.vo.WxLiteUserInfo;
import com.fine.hair.comm.vo.WxLoginResultVo;
import com.fine.hair.comm.wx.WxClient;
import com.fine.hair.manage.mapper.UserLoginAccountMapper;
import com.fine.hair.manage.mapper.UserMapper;
import com.fine.hair.manage.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import java.util.UUID;

/**
 * <p>${Description}</p>
 *
 * @author mouseyCat
 * @date 2020/9/7 20:44
 */
@Slf4j
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Autowired
    private WxClient wxClient;

    @Resource
    private UserLoginAccountMapper userLoginAccountMapper;


    @Autowired
    private Snowflake snowflake;


    @Override
    public ApiResponse<String> getOpenId(String code) {

        // 在 userLoginAccount 创建新的数据
        WxCode2SessionResponse wxCodeRes = wxClient.code2Session(code);

        // 查询是否存在
        LambdaQueryWrapper<UserLoginAccount> uQw = new LambdaQueryWrapper<>();
        uQw.eq(UserLoginAccount::getWxAccount, wxCodeRes.getOpenId());
        UserLoginAccount userLoginAccount1 = userLoginAccountMapper.selectOne(uQw);
        if (userLoginAccount1 == null) {

            // 保存 sessionKey openId
            UserLoginAccount userLoginAccount = new UserLoginAccount();
            userLoginAccount.setWxAccountType(AppType.WX_LITE.getCode());
            userLoginAccount.setUserId(snowflake.nextIdStr());
            userLoginAccount.setWxAccount(wxCodeRes.getOpenId());
            userLoginAccount.setSessionKey(wxCodeRes.getSessionKey());
            userLoginAccountMapper.insert(userLoginAccount);
            return ApiResponse.ofSuccess(wxCodeRes.getOpenId());
        }
        userLoginAccount1.setSessionKey(wxCodeRes.getSessionKey());
        LambdaQueryWrapper<UserLoginAccount> uQw2 = new LambdaQueryWrapper<>();
        uQw2.eq(UserLoginAccount::getWxAccount, wxCodeRes.getOpenId());
        userLoginAccountMapper.update(userLoginAccount1, uQw2);

        return ApiResponse.ofSuccess(wxCodeRes.getOpenId());
    }



    @Override
    public void isNull(User user) {
        if (ObjectUtil.isNull(user)) {
            throw new BusinessException("当前用户不存在");
        }
    }

}




