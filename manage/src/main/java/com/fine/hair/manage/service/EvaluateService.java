package com.fine.hair.manage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fine.hair.comm.dto.StaffEvaluateDto;
import com.fine.hair.comm.model.Evaluate;
import com.fine.hair.comm.vo.StaffEvaluateVo;

/**
 * <p>${Description}</p>
 *
 * @author mouseyCat
 * @date 2020/10/4 15:12
 */
public interface EvaluateService extends IService<Evaluate> {

    StaffEvaluateVo staffEvaluate(StaffEvaluateDto dto);
}

