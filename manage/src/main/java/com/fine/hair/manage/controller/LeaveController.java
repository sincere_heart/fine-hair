package com.fine.hair.manage.controller;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.lang.Snowflake;
import com.fine.hair.comm.dto.LeaveDto;
import com.fine.hair.comm.dto.LeavesDto;
import com.fine.hair.comm.utils.ApiResponse;
import com.fine.hair.comm.utils.PageInfo;
import com.fine.hair.comm.vo.LeaveVo;
import com.fine.hair.comm.model.Leave;
import com.fine.hair.manage.mapper.LeaveMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 请假表 前端控制器
 * </p>
 *
 * @author liheng
 * @since 2021-03-08
 */
@Api(tags = {"请假相关接口"})
@RestController
@RequestMapping("/leave/")
public class LeaveController {
    @Autowired
    private Snowflake snowflake;
    @Autowired
    private LeaveMapper leaveMapper;

    @PostMapping("list")
    @ApiOperation("请假条列表")
    public ApiResponse addLeave(@RequestBody LeavesDto dto) {
        PageInfo<LeaveVo> pageInfo = new PageInfo<>(dto.getPageCurr(), dto.getPageSize());
        List<LeaveVo> leaveVos = leaveMapper.selectLeaveList(dto,pageInfo);
        return ApiResponse.ofSuccess(pageInfo.setRecords(leaveVos));
    }

    @PostMapping("add")
    @ApiOperation("新增请假条")
    public ApiResponse addLeave(@RequestBody LeaveDto dto) {
        Leave leave = new Leave();
        leave.setId(snowflake.nextId());
        BeanUtil.copyProperties(dto, leave);
        leave.setEndTime(leave.getStartTime().plusDays(dto.getLeaveDay()));
        leaveMapper.insert(leave);
        return ApiResponse.ofSuccess();
    }

    @DeleteMapping("del/{leaveId}")
    @ApiOperation("删除请假条")
    public ApiResponse delLeave(@PathVariable Long leaveId) {
        leaveMapper.deleteById(leaveId);
        return ApiResponse.ofSuccess();
    }
}

