package com.fine.hair.manage;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author mouseyCat
 * @date 2020-08-07 11:20
 */
@SpringBootApplication(scanBasePackages = {"com.fine.hair"})
@MapperScan(basePackages = {
        "com.fine.hair.manage.mapper"
})
@EnableTransactionManagement
@EnableSwagger2
public class AdminApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(AdminApiApplication.class, args);
    }

}
