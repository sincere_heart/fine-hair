package com.fine.hair.manage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fine.hair.comm.dto.GdxhtjDTO;
import com.fine.hair.comm.model.Branch;
import com.fine.hair.comm.utils.PageInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>${Description}</p>
 *
 * @author mouseyCat
 * @date 2020/10/26 21:39
 */
@Mapper
public interface BranchMapper extends BaseMapper<Branch> {
    /**
     * @param branchName
     * @return
     */
    List<Branch> searchBranchByBranchName(@Param("branchName") String branchName);

    Branch selectByBranchName(@Param("branchName") String branchName, @Param("branchId") String branchId);

    Branch selectRandomOne();

    List<Map<String, Object>> selectGdxhtj(@Param("dto")GdxhtjDTO dto,@Param("pageInfo") PageInfo<Map<String, Object>> pageInfo);

    List<Map<String, Object>> selectGdxhygtj(@Param("dto")GdxhtjDTO dto,@Param("pageInfo")  PageInfo<Map<String, Object>> pageInfo);

    List<Map<String, Object>> selectGdxhxmtj(@Param("dto")GdxhtjDTO dto, @Param("pageInfo")PageInfo<Map<String, Object>> pageInfo);

    BigDecimal selectGdszdbtj(@Param("branchIds")List<Long> branchIds, @Param("date")String date,@Param("type")Integer type);

    Integer selectGdxmlltj(@Param("branchIds")List<Long> branchIds, @Param("date")String date);

    BigDecimal selectGdzbxhtj(@Param("branchIds")List<Long> branchIds, @Param("date")String date);

    BigDecimal selectGdxmxhtj(@Param("branchIds")List<Long> branchIds,  @Param("date")String date);
}
