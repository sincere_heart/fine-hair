package com.fine.hair.manage.controller;

import com.fine.hair.manage.service.impl.OrderTaskServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>订单定时任务</p>
 *
 * @author mouseyCat
 * @date 2020/10/27 21:37
 */
@Slf4j
@Configuration
@EnableScheduling   // 2.开启定时任务
public class OrderTask {


    @Autowired
    private OrderTaskServiceImpl orderTaskServiceImpl;

    /**
     * 两分钟执行一次
     */
    @Scheduled(fixedRate = 120000)
    @Transactional(rollbackFor = Exception.class)
    public void syncOrderInfo() {
        orderTaskServiceImpl.syncOrderInfo();
    }


}
