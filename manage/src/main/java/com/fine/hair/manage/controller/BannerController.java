package com.fine.hair.manage.controller;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.lang.Snowflake;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.fine.hair.comm.dto.AddBannerDto;
import com.fine.hair.comm.dto.AlterBannerDto;
import com.fine.hair.comm.utils.ApiResponse;
import com.fine.hair.comm.model.Banner;
import com.fine.hair.manage.mapper.BannerMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;

/**
 * <p>轮播图控制层</p>
 *
 * @author mouseyCat
 * @date 2020/10/25 21:38
 */
@Api(tags = {"轮播图相关接口"})
@Data
@RestController
@RequestMapping("/banner/")
public class BannerController {

    @Resource
    private BannerMapper bannerMapper;

    @Autowired
    private Snowflake snowflake;

    @GetMapping("banner-list")
    @ApiOperation("轮播图列表")
    public ApiResponse<List<Banner>> bannerList() {
        return ApiResponse.ofSuccess(bannerMapper.selectList(new LambdaQueryWrapper<>()));
    }

    @PutMapping("alter-banner")
    @ApiOperation("编辑轮播图")
    public ApiResponse alterBanner(@RequestBody AlterBannerDto dto) {
        Banner banner = bannerMapper.selectById(dto.getId());
        BeanUtil.copyProperties(dto, banner);
        bannerMapper.updateById(banner);
        return ApiResponse.ofSuccess();
    }

    @PostMapping("add-banner")
    @ApiOperation("添加轮播图")
    public ApiResponse addBanner(@RequestBody AddBannerDto dto) {
        Banner banner = new Banner();
        BeanUtil.copyProperties(dto, banner);
        banner.setId(snowflake.nextId());
        bannerMapper.insert(banner);
        return ApiResponse.ofSuccess();
    }

    @DeleteMapping("del-banner/{bannerId}")
    @ApiOperation("删除轮播图")
    public ApiResponse delBanner(@PathVariable String bannerId ) {
        bannerMapper.deleteById(bannerId);
        return ApiResponse.ofSuccess();
    }
}
