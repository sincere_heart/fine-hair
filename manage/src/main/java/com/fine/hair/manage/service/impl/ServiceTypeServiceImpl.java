package com.fine.hair.manage.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fine.hair.comm.model.ServiceType;
import com.fine.hair.manage.mapper.ServiceTypeMapper;
import com.fine.hair.manage.service.ServiceTypeService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liheng
 * @since 2021-07-29
 */
@Service
public class ServiceTypeServiceImpl extends ServiceImpl<ServiceTypeMapper, ServiceType> implements ServiceTypeService {

}
