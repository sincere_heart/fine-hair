package com.fine.hair.manage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fine.hair.comm.model.ServiceType;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liheng
 * @since 2021-07-29
 */
public interface ServiceTypeService extends IService<ServiceType> {

}
