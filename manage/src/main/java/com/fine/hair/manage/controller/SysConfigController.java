package com.fine.hair.manage.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.fine.hair.comm.dto.AlterConfigDto;
import com.fine.hair.comm.dto.AlterOrderCommissionInfoDto;
import com.fine.hair.comm.exception.BusinessException;
import com.fine.hair.comm.pojo.CommissionPojo;
import com.fine.hair.comm.utils.ApiResponse;
import com.fine.hair.manage.config.AuthService;
import com.fine.hair.comm.model.Commission;
import com.fine.hair.comm.model.SysConfig;
import com.fine.hair.manage.mapper.CommissionMapper;
import com.fine.hair.manage.mapper.SysConfigMapper;
import com.fine.hair.manage.service.CommissionService;
import com.google.common.collect.Lists;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>系统配置控制类</p>
 *
 * @author mouseyCat
 * @date 2020/10/27 22:05
 */
@Api(tags = {"系统配置控制类"})
@RestController
@RequestMapping("/sys-config/")
public class SysConfigController {

    @Resource
    private SysConfigMapper sysConfigMapper;

    @Resource
    private CommissionMapper commissionMapper;

    @Autowired
    private CommissionService commissionService;

    @Autowired
    private Snowflake snowflake;

    @Autowired
    private AuthService authService;

    @ApiOperation("所有配置信息")
    @GetMapping("config")
    public ApiResponse<SysConfig> config() {
        return ApiResponse.ofSuccess(sysConfigMapper.selectOne(new LambdaQueryWrapper<>()));
    }

    @ApiOperation("修改配置信息")
    @PostMapping("alter-config")
    public ApiResponse alterConfig(@RequestBody AlterConfigDto dto) {
        SysConfig sysConfig = new SysConfig();
        BeanUtil.copyProperties(dto,sysConfig);
        sysConfigMapper.updateById(sysConfig);
        return ApiResponse.ofSuccess();
    }

    @ApiOperation("订单提成配置信息")
    @GetMapping("order-commission-info")
    public ApiResponse<List<Commission>> orderCommissionInfo() {
        return ApiResponse.ofSuccess(commissionMapper.selectList(new LambdaQueryWrapper<Commission>().orderByAsc(Commission::getSectionStart)));
    }

    @ApiOperation("修改订单提成配置信息")
    @PostMapping("alter-order-commission-info")
    public ApiResponse alterOrderCommissionInfo(@RequestBody AlterOrderCommissionInfoDto dto) {
        CommissionPojo commissionPojo = dto.getCommissionList().get(0);
        if (commissionPojo.getSectionStart() > commissionPojo.getSectionEnd()) {
            throw new BusinessException("区间的开始不能小于结束");
        }
        List<Commission> list = commissionService.list(new LambdaQueryWrapper<Commission>().orderByAsc(Commission::getId));

        for (Long i = commissionPojo.getSectionStart(); i <= commissionPojo.getSectionEnd() ; i++) {
            for (Commission commission : list) {
                if (ObjectUtil.isNotNull(commissionPojo.getId())) {
                    if (commission.getId().equals(commissionPojo.getId())) {
                        break;
                    }
                }
                if (i >= commission.getSectionStart() && i <= commission.getSectionEnd()) {
                    throw new BusinessException("当前区间与其它区间重复！");
                }
            }
        }
        ArrayList<Commission> commissionList = Lists.newArrayList();
        for (int i = 0; i < dto.getCommissionList().size(); i++) {
            Commission commission = new Commission();
            BeanUtil.copyProperties(dto.getCommissionList().get(i), commission);
            if (commission.getId() == null) {
                commission.setId(snowflake.nextId());
            }
            commissionList.add(commission);
        }
        commissionService.saveOrUpdateBatch(commissionList);

        return ApiResponse.ofSuccess();
    }

    @ApiOperation("删除订单提成配置信息")
    @DeleteMapping("del-order-commission-info/{commissionId}")
    public ApiResponse delOrderCommissionInfo(@PathVariable String commissionId) {
        commissionMapper.deleteById(commissionId);
        return ApiResponse.ofSuccess();
    }


}
