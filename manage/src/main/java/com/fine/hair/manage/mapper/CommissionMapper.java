package com.fine.hair.manage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fine.hair.comm.model.Commission;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>${Description}</p>
 *
 * @author mouseyCat
 * @date 2020/10/29 23:48
 */
@Mapper
public interface CommissionMapper extends BaseMapper<Commission> {
}
