package com.fine.hair.manage.controller;

import com.fine.hair.comm.dto.FeedbackListDto;
import com.fine.hair.comm.enums.OrderStatus;
import com.fine.hair.comm.model.*;
import com.fine.hair.comm.utils.ApiResponse;
import com.fine.hair.comm.utils.PageInfo;
import com.fine.hair.comm.vo.FeedbackListVo;
import com.fine.hair.manage.mapper.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>用户反馈控制层</p>
 *
 * @author mouseyCat
 * @date 2020/11/18 14:43
 */
@Api(tags = {"用户反馈相关接口"})
@RestController
@RequestMapping("/feedback/")
public class FeedbackController {

    @Resource
    private FeedbackMapper feedbackMapper;

    @Autowired
    private EvaluateMapper evaluateMapper;

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private BranchMapper branchMapper;
    @Autowired
    private ServiceTypeMapper serviceTypeMapper;
    @Autowired
    private ItemMapper itemMapper;

    @PostMapping("feedback-list")
    @ApiOperation("获取反馈信息")
    public ApiResponse<PageInfo<FeedbackListVo>> feedbackList(@RequestBody FeedbackListDto dto) {
        //PageInfo<FeedbackListVo> pageInfo = new PageInfo<>(dto.getPageCurr(), dto.getPageSize());
        //List<FeedbackListVo> list = feedbackMapper.selectFeedbackList(pageInfo);
        //pageInfo.setRecords(list);

        PageInfo<FeedbackListVo> pageInfo = new PageInfo<>(dto.getPageCurr(), dto.getPageSize());
        List<FeedbackListVo> list = evaluateMapper.selectFeedbackList(pageInfo);
        pageInfo.setRecords(list);

        return ApiResponse.ofSuccess(pageInfo);
    }

    @DeleteMapping("del-feedback/{feedbackId}")
    @ApiOperation("删除一条反馈信息")
    public ApiResponse delFeedBack(@PathVariable Long feedbackId) {
        evaluateMapper.deleteById(feedbackId);
        //feedbackMapper.deleteById(feedbackId);
        return ApiResponse.ofSuccess();
    }


    @GetMapping("get-feedback/{feedbackId}")
    @ApiOperation("获取一条反馈信息")
    public ApiResponse getFeedBack(@PathVariable Long feedbackId) {
        // 评价信息
        Evaluate evaluate = evaluateMapper.selectById(feedbackId);
        // 订单信息
        Order order = orderMapper.selectById(evaluate.getOrderId());
        Map<String, Object> orderMap = new HashMap<>(8);
        orderMap.put("orderStatus", OrderStatus.fromCode(Integer.valueOf(order.getOrderStatus())));
        orderMap.put("price", order.getItemPrice());
        orderMap.put("remark", order.getRemark());
        orderMap.put("executeTime", order.getExecuteTime());
        orderMap.put("itemTime", order.getItemTime());
        Branch branch = branchMapper.selectById(order.getBranchId());
        orderMap.put("branchName", branch.getTitle());
        ServiceType serviceType = serviceTypeMapper.selectById(order.getItemType());
        orderMap.put("serviceTypeName", serviceType.getServiceName());
        Item item = itemMapper.selectById(order.getItemId());
        orderMap.put("itemName", item.getTitle());
        // 技师信息
        User user = userMapper.selectById(evaluate.getEvaluateId());
        Map<String, Object> userMap = new HashMap<>(4);
        userMap.put("nickName", user.getNickName());
        userMap.put("phoneNumber", user.getPhoneNumber());
        userMap.put("sex", user.getSex());
        userMap.put("birth", user.getBirth());
        // 技师信息
        User technicianUser = userMapper.selectById(evaluate.getTechnicianId());
        Map<String, Object> technicianMap = new HashMap<>(1);
        technicianMap.put("technicianName", technicianUser.getNickName());

        Map<String, Object> resultMap = new HashMap<>(4);
        resultMap.put("order",orderMap);
        resultMap.put("user",userMap);
        resultMap.put("technicianUser",technicianMap);
        resultMap.put("evaluate",evaluate);
        //feedbackMapper.deleteById(feedbackId);
        return ApiResponse.ofSuccess(resultMap);
    }

}
