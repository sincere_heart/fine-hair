package com.fine.hair.manage.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fine.hair.comm.dto.StaffEvaluateDto;
import com.fine.hair.comm.model.Evaluate;
import com.fine.hair.comm.pojo.EvaluateList;
import com.fine.hair.comm.utils.PageInfo;
import com.fine.hair.comm.vo.StaffEvaluateVo;
import com.fine.hair.manage.mapper.EvaluateMapper;
import com.fine.hair.manage.service.EvaluateService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>${Description}</p>
 *
 * @author mouseyCat
 * @date 2020/10/4 15:12
 */
@Service
public class EvaluateServiceImpl extends ServiceImpl<EvaluateMapper, Evaluate> implements EvaluateService {

    @Resource
    private EvaluateMapper evaluateMapper;

    @Override
    public StaffEvaluateVo staffEvaluate(StaffEvaluateDto dto) {
        // 获取 技师信息 和 门店图片
        StaffEvaluateVo vo = evaluateMapper.staffEvaluate(dto.getUserId());
        if (vo.getScore() == null) {
            vo.setScore("暂无");
        }
        if (vo.getResume() == null) {
            vo.setResume("暂无");
        }
        // 处理门店图片,只取第一张
        String[] split = vo.getBranchImage().split(Constants.COMMA);
        vo.setBranchImage(split[0]);

        // 获取评论列表
        PageInfo<EvaluateList> pageInfo = new PageInfo<>(dto.getPageCurr(), dto.getPageSize());
        List<EvaluateList> list = evaluateMapper.selectEvaluateList(dto.getUserId(), pageInfo);
        pageInfo.setRecords(list);
        vo.setPageInfo(pageInfo);
        return vo;
    }
}

