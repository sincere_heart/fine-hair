package com.fine.hair.manage.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author liheng
 * @version 1.0
 * @date 2019/8/4 0:21
 * swagger2的配置参数
 * 并设定默认开关，@ConfigurationProperties
 */
@Data
@Component
@PropertySource(value = "classpath:/swagger.properties")
@ConfigurationProperties(prefix = "web.swagger")
public class SwaggerProperties {

    /**
     * 是否启动Swagger2,默认开启
     */
    private boolean enabled = true;
    /**
     * Swagger2扫描的包，默认为：com
     */
    private String basePackage = "com";
    /**
     * 基础映射路径,默认当前项目根路径
     */
    private String pathMapping = "/";
    /**
     * 标题
     */
    private String title="未定义标题";

    /**
     * 描述
     */
    private String description;
    /**
     * 服务地址
     */
    private String serviceUrl;
    /**
     * 证书 eg: The Apache License, Version 2.0
     */
    private String license;
    /**
     * 证书地址 eg: http://www.apache.org/licenses/LICENSE-2.0.html
     */
    private String licenseUrl;
    /**
     * 联系：默认
     */
    @NestedConfigurationProperty
    private Contact contact = new Contact();
    /**
     * 版本，默认为：1.0.0
     */
    private String version = "1.0.0";
    /**
     * 文档需要填充的固定参数，例如token之类的
     * 填充示例
     * web.swagger.reqFixedParameters[0].paramKey=token
     * web.swagger.reqFixedParameters[0].description=description
     * web.swagger.reqFixedParameters[0].required=fasle
     * web.swagger.reqFixedParameters[1].paramKey=token
     * web.swagger.reqFixedParameters[1].description=description
     * web.swagger.reqFixedParameters[1].required=true
     * ---------------------
     */
    private List<ReqFixedParameter> reqFixedParameters;
    /**
     * 联系内部类===作者信息
     */
    @Data
    static class Contact {

        /**
         * 联系名
         */
        private String name;
        /**
         * 网址
         */
        private String url;
        /**
         * Email地址
         */
        private String email;
    }

    /**
     * 联系内部类===请求头部固定参数类
     */
    @Data
    static class ReqFixedParameter {

        /**
         * 参数名
         */
        private String paramKey;
        /**
         * 描述
         */
        private String description;
        /**
         * 是否必填，默认非必填
         */
        private boolean required = false;
    }

}
