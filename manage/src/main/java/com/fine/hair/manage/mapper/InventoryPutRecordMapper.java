package com.fine.hair.manage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fine.hair.comm.dto.GdzctjDTO;
import com.fine.hair.comm.dto.InventoryPutRecordDto;
import com.fine.hair.comm.model.InventoryPutRecord;
import com.fine.hair.comm.utils.PageInfo;
import com.fine.hair.comm.vo.InventoryPutRecordVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>${Description}</p>
 *
 * @author mouseyCat
 * @date 2021/2/5 23:58
 */
@Mapper
public interface InventoryPutRecordMapper extends BaseMapper<InventoryPutRecord> {
    List<InventoryPutRecordVo> list(@Param("dto") InventoryPutRecordDto dto,
                                    @Param("objectPageInfo") PageInfo<InventoryPutRecordVo> objectPageInfo);

    InventoryPutRecord selectOneRecord(Long inventoryId);

    List<InventoryPutRecord> selectListByDate(@Param("date") String date, @Param("branchId") String branchId);

     List<Map<String,Object>> selectGdzctj(@Param("dto")GdzctjDTO dto,@Param("pageInfo") PageInfo<Map<String,Object>> pageInfo);
}
