package com.fine.hair.manage.config.security;

import com.fine.hair.comm.utils.MyStatus;
import com.fine.hair.comm.utils.ResponseUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.web.access.AccessDeniedHandler;

/**
 * <p>
 * Security 结果处理配置
 * </p>
 *
 * @description: Security 结果处理配置
 */
@Configuration
public class SecurityHandlerConfig {

    @Bean
    public AccessDeniedHandler accessDeniedHandler() {
        return (request, response, accessDeniedException) -> ResponseUtils.renderJson(response, MyStatus.ACCESS_DENIED, null);
    }

}
