package com.fine.hair.manage.config;

import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.ArrayList;
import java.util.List;

/**
 * swagger2 的spring-boot启动器
 *
 * @author liheng
 */
@Configuration
@EnableKnife4j
public class SwaggerAutoConfiguration implements WebMvcConfigurer {
    @Autowired
    private SwaggerProperties properties;

    @Bean
    public Docket docket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .enable(properties.isEnabled())
                .apiInfo(apiInfo())
                .select()
                //加了ApiOperation注解的类，才生成接口文档
                .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                //此包下的类，才生成接口文档，默认com包
                .apis(RequestHandlerSelectors.basePackage(properties.getBasePackage()))
                .paths(PathSelectors.any())
                .build()
                .pathMapping(properties.getPathMapping())
                .globalOperationParameters(setFixedParameter());
    }

    /**
     * 设置文档基本信息
     *
     * @return
     */
    private ApiInfo apiInfo() {
        SwaggerProperties.Contact contact = properties.getContact();
        return new ApiInfoBuilder()
                .title(properties.getTitle())
                .description(properties.getDescription())
                .termsOfServiceUrl(properties.getServiceUrl())
                .contact(new Contact(contact.getName(), contact.getUrl(), contact.getEmail()))
                .version(properties.getVersion())
                .license(properties.getLicense())
                .licenseUrl(properties.getLicenseUrl())
                .build();
    }

    /**
     * 设置请求头部固定参数
     *
     * @return
     */
    private List<Parameter> setFixedParameter() {
        List<Parameter> pars = new ArrayList<>();
        List<SwaggerProperties.ReqFixedParameter> reqFixedParameters = properties.getReqFixedParameters();
        ParameterBuilder parameterBuilder = null;
        for (SwaggerProperties.ReqFixedParameter reqFixedParameter : reqFixedParameters) {
            parameterBuilder = new ParameterBuilder();
            parameterBuilder.name(reqFixedParameter.getParamKey())
                    .description(reqFixedParameter.getDescription())
                    .modelRef(new ModelRef("string"))
                    .parameterType("header")
                    .required(reqFixedParameter.isRequired()).build();
            pars.add(parameterBuilder.build());
        }
        return pars;
    }

    /**
     * 接口请求拦截配置
     *
     * @param registry
     */
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addRedirectViewController("swagger", "swagger-ui.html");
    }

}
