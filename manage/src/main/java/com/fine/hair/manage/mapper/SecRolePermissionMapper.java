package com.fine.hair.manage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fine.hair.comm.model.SecRolePermission;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>${Description}</p>
 *
 * @author mouseyCat
 * @date 2020/10/26 21:39
 */
@Mapper
public interface SecRolePermissionMapper extends BaseMapper<SecRolePermission> {
    /**
     * @param permissionId
     * @param roleId
     * @return
     */
    Long selectByPermissionId(@Param("permissionId") Long permissionId, @Param("roleId") Long roleId);
}
