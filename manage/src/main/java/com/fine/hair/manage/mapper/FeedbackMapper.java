package com.fine.hair.manage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fine.hair.comm.model.Feedback;
import com.fine.hair.comm.utils.PageInfo;
import com.fine.hair.comm.vo.FeedbackListVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>${Description}</p>
 *
 * @author mouseyCat
 * @date 2020/11/18 14:42
 */
@Mapper
public interface FeedbackMapper extends BaseMapper<Feedback> {
    /**
     * @param pageInfo
     * @return
     */
    List<FeedbackListVo> selectFeedbackList(@Param("pageInfo") PageInfo<FeedbackListVo> pageInfo);
}
