package com.fine.hair.manage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fine.hair.comm.dto.DetectBeforeAddDto;
import com.fine.hair.comm.dto.InventoryAddDto;
import com.fine.hair.comm.dto.InventoryPageDto;
import com.fine.hair.comm.model.Inventory;
import com.fine.hair.comm.utils.PageInfo;
import com.fine.hair.comm.vo.InventoryPageVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>${Description}</p>
 *
 * @author mouseyCat
 * @date 2021/2/5 23:57
 */
@Mapper
public interface InventoryMapper extends BaseMapper<Inventory> {

    Inventory selectOneLikeName(@Param("dto") DetectBeforeAddDto dto);

    List<InventoryPageVo> list(@Param("dto") InventoryPageDto dto, @Param("branchIds")  List<String> branchIds,
                               @Param("objectPageInfo") PageInfo<InventoryPageVo> objectPageInfo);
}
