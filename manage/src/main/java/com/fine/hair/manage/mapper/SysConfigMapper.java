package com.fine.hair.manage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fine.hair.comm.model.SysConfig;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>${Description}</p>
 *
 * @author mouseyCat
 * @date 2020/10/29 23:25
 */
@Mapper
public interface SysConfigMapper extends BaseMapper<SysConfig> {
}
