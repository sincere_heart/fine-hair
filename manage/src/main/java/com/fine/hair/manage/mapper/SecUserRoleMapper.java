package com.fine.hair.manage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fine.hair.comm.model.SecUserRole;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>${Description}</p>
 *
 * @author mouseyCat
 * @date 2020/10/26 21:39
 */
@Mapper
public interface SecUserRoleMapper extends BaseMapper<SecUserRole> {
}
