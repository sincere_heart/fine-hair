package com.fine.hair.manage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fine.hair.comm.dto.LeavesDto;
import com.fine.hair.comm.model.Leave;
import com.fine.hair.comm.utils.PageInfo;
import com.fine.hair.comm.vo.LeaveVo;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 请假表 Mapper 接口
 * </p>
 *
 * @author liheng
 * @since 2021-03-08
 */
public interface LeaveMapper extends BaseMapper<Leave> {

    List<LeaveVo> selectLeaveList(@Param("dto") LeavesDto dto,
                                  @Param("pageInfo") PageInfo<LeaveVo> pageInfo);

   int selectLeaveCountByOrderTime(@Param("orderTime") LocalDateTime orderTime,
                                   @Param("technicianId") Long technicianId);
}
