package com.fine.hair.manage.controller;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.fine.hair.comm.dto.*;
import com.fine.hair.comm.enums.OrderStatus;
import com.fine.hair.comm.exception.BusinessException;
import com.fine.hair.comm.utils.ApiResponse;
import com.fine.hair.comm.vo.LatestOrderVO;
import com.fine.hair.comm.model.Branch;
import com.fine.hair.comm.model.Calendar;
import com.fine.hair.comm.model.Order;
import com.fine.hair.comm.model.User;
import com.fine.hair.manage.mapper.BranchMapper;
import com.fine.hair.manage.mapper.CalendarMapper;
import com.fine.hair.manage.mapper.OrderMapper;
import com.fine.hair.manage.mapper.UserMapper;
import com.google.common.collect.Lists;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>门店相关接口</p>
 *
 * @author mouseyCat
 * @date 2020/10/9 22:59
 */
@Api(tags = {"2月份新增的接口'"})
@RestController
@RequestMapping("/new/")
public class NewChangeController {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private CalendarMapper calendarMapper;

    @Autowired
    private BranchMapper branchMapper;

    @ApiOperation(value = "获取当前员工的上下班状态",notes = "")
    @PostMapping("workStatus")
    public ApiResponse name(@RequestBody   @Validated WorkStatusDto dto) {
        User user = userMapper.selectById(dto.getUserId());
        if (ObjectUtil.isNull(user)) {
            throw new BusinessException("当前用户不存在");
        }
        Integer onWork = user.getOnWork();
        if (onWork.equals(1)) {
            // 1 上班
            //List<Order> orders = orderMapper.selectOrderByDate(DateUtil.formatDate(new Date()));
            //if (orders.size() > 0) {
            //    throw new BusinessException("下班失败，今天有未完成订单");
            //}
            return ApiResponse.ofSuccess(true);
        } else {
            // 0，2 下班
            return ApiResponse.ofSuccess(false);
        }
    }

    @ApiOperation(value = "上下班按钮", notes = "1-a")
    @PostMapping("workSwitch")
    public ApiResponse name(@RequestBody  @Validated WorkSwitchDto dto) {
        User user = userMapper.selectById(dto.getUserId());
        if (ObjectUtil.isNull(user)) {
            throw new BusinessException("当前用户不存在");
        }
        if (dto.getStatus() == 1) {
            //上班
            user.setOnWork(1);
        } else {
            //下班
            user.setOnWork(2);
        }
        userMapper.updateById(user);
        return ApiResponse.ofSuccess();
    }



    @ApiOperation(value = "获取当前用户所属周期", notes = "2")
    @PostMapping("getCurrentUserCalendar")
    public ApiResponse name(@RequestBody  @Validated CurrentUserCalendarDto dto) {
        User user = userMapper.selectById(dto.getUserId());
        if (ObjectUtil.isNull(user)) {
            throw new BusinessException("当前用户不存在");
        }
        if (ObjectUtil.isNull(user.getLabelType())) {
            return ApiResponse.ofSuccess("暂无周期安排");
        } else {
            Calendar calendar = calendarMapper.selectById(user.getLabelType());
            if (ObjectUtil.isNull(calendar)) {
                throw new BusinessException("当前周期配置不存在");
            }
            return ApiResponse.ofSuccess(calendar.getCount() + "天一次");
        }
    }
    @ApiOperation(value = "将订单改为《已开始》状态",
            notes = "订单为未开始时，显示【开始订单】按钮；订单为已开始时，显示【完成订单】按钮；")
    @PostMapping("startOrder")
    public ApiResponse name(@RequestBody  @Validated StartOrderDto dto) {
        Order order = orderMapper.selectById(dto.getOrderId());
        if (ObjectUtil.isNull(order)) {
            throw new BusinessException("当前订单不存在");
        }
        order.setOrderStatus(OrderStatus.STARTING.getCode().toString());
        orderMapper.updateById(order);
        return ApiResponse.ofSuccess();
    }

    @ApiOperation(value = "用户的最近一次养护记录", notes = "9-a")
    @PostMapping("latestOrder")
    public ApiResponse<List<LatestOrderVO>> name(@RequestBody @Validated LatestOrderDto dto) {
        User user = userMapper.selectById(dto.getUserId());
        if (ObjectUtil.isNull(user)) {
            throw new BusinessException("当前用户不存在");
        }
        List<Order> orders = orderMapper.selectList(new LambdaQueryWrapper<Order>().eq(Order::getUserId,
                user.getId()).orderByDesc(Order::getCreateTime));
        LatestOrderVO latestOrderVO = new LatestOrderVO();
        ArrayList<LatestOrderVO> latestOrderVOList = Lists.newArrayList();
        if (orders.size() > 0) {
            Order order = orders.get(orders.size() - 1);
            latestOrderVO.setCreateTime(DateUtil.formatDateTime(order.getCreateTime()));
            latestOrderVO.setExecuteTime(DateUtil.formatDateTime(order.getExecuteTime()));
            latestOrderVO.setItemImage(order.getItemImage());
            latestOrderVO.setItemPrice(order.getItemPrice().toString());
            latestOrderVO.setItemTitle(order.getItemTitle());
            latestOrderVOList.add(latestOrderVO);
            return ApiResponse.ofSuccess(latestOrderVOList);
        } else {
            return ApiResponse.ofSuccess(latestOrderVOList);
        }
    }





    @ApiOperation("根据名字搜索门店列表")
    @PostMapping("searchBranchByBranchName")
    public ApiResponse<List<Branch>> name(@RequestBody @Validated SearchBranchByBranchNameDto dto) {
        List<Branch> list = branchMapper.searchBranchByBranchName(dto.getBranchName());
        return ApiResponse.ofSuccess(list);
    }

    //@ApiOperation("")
    //@PostMapping("")
    //public ApiResponse name(@RequestBody Dto dto) {
    //
    //}
    //@ApiOperation("")
    //@PostMapping("")
    //public ApiResponse name(@RequestBody Dto dto) {
    //
    //}
    //@ApiOperation("")
    //@PostMapping("")
    //public ApiResponse name(@RequestBody Dto dto) {
    //
    //}
    //@ApiOperation("")
    //@PostMapping("")
    //public ApiResponse name(@RequestBody Dto dto) {
    //
    //}

}
