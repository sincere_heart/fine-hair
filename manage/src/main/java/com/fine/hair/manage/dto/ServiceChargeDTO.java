package com.fine.hair.manage.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author liheng
 * @ClassName ServiceChargeDTO
 * @Description
 * @date 2021-07-29 18:36
 */
@Data
public class ServiceChargeDTO {
    @ApiModelProperty(value = "用户id")
    private String userId;
    /**
     * 剩余护发服务次数
     */
    @ApiModelProperty(value = "初级调理")
    private Integer hf_count=0;

    /**
     * 剩余养发服务次数
     */
    @ApiModelProperty(value = "中级调理")
    private Integer yf_count=0;

    /**
     * 剩余发质服务次数
     */
    @ApiModelProperty(value = "高级调理")
    private Integer fz_count=0;

    /**
     * 顶级调理
     */
    @ApiModelProperty(value = "顶级调理")
    private Integer dj_count=0;

    /**
     * 头部调理
     */
    @ApiModelProperty(value = "头部调理")
    private Integer tb_count=0;

    /**
     * 优惠调理
     */
    @ApiModelProperty(value = "优惠调理")
    private Integer yh_count=0;
}
