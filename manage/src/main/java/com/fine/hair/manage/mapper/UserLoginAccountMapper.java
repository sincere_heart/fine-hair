package com.fine.hair.manage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fine.hair.comm.model.UserLoginAccount;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>${Description}</p>
 *
 * @author mouseyCat
 * @date 2020/10/26 21:39
 */
@Mapper
public interface UserLoginAccountMapper extends BaseMapper<UserLoginAccount> {

    /**
     * @param wxAccount
     * @param userId
     */
    void updateUIdByUsername(@Param("wxAccount") String wxAccount, @Param("userId") String userId);

}
