package com.fine.hair.manage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fine.hair.comm.model.SecPermission;
import com.fine.hair.comm.vo.rbac.PermissionInfoVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>${Description}</p>
 *
 * @author mouseyCat
 * @date 2020/10/26 21:39
 */
@Mapper
public interface SecPermissionMapper extends BaseMapper<SecPermission> {
    /**
     * @param roleIds
     * @return
     */
    List<SecPermission> selectByRoleIdList(@Param("ids") List<Long> roleIds);

    /**
     * @param permissionId
     * @return
     */
    List<PermissionInfoVo> selectByPermissionIds(@Param("permissionId") Long permissionId);
}
