package com.fine.hair.manage.controller;

/**
 * <p>管理员相关操作接口</p>
 *
 * @author mouseyCat
 * @date 2020/11/17 11:11
 */

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.fine.hair.comm.dto.BasePage;
import com.fine.hair.comm.dto.ModifyPhoneDTO;
import com.fine.hair.comm.dto.rbac.AddAdminDto;
import com.fine.hair.comm.dto.rbac.ModifyAdminDto;
import com.fine.hair.comm.dto.rbac.ResetPasswordDto;
import com.fine.hair.comm.dto.rbac.ShutAdminDto;
import com.fine.hair.comm.exception.BusinessException;
import com.fine.hair.comm.utils.ApiResponse;
import com.fine.hair.comm.utils.PageInfo;
import com.fine.hair.comm.vo.AdminVo;
import com.fine.hair.manage.config.AuthService;
import com.fine.hair.comm.model.SecUser;
import com.fine.hair.manage.mapper.SecRoleMapper;
import com.fine.hair.manage.mapper.SecUserMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(tags = {"管理员相关操作接口（只有超管才能操作）"})
@RestController
@RequestMapping("/admin/")
public class AdminController {

    @Autowired
    private AuthService authService;

    @Autowired
    private SecRoleMapper secRoleMapper;

    @Autowired
    private SecUserMapper secUserMapper;

    @ApiOperation("添加管理员")
    @PostMapping("add-admin")
    public ApiResponse addAdmin(@RequestBody AddAdminDto dto) {
        return authService.addAdmin(dto);
    }

    @ApiOperation("超管新增管理员之前获得所有角色")
    @GetMapping("acco-and-perm/before-add-admin-get-perm")
    public ApiResponse beforeAddAdminGetPerm() {
        return ApiResponse.ofSuccess(secRoleMapper.selectRoleList());
    }


    @ApiOperation("点击编辑时获取管理员信息")
    @GetMapping("click-edit-get-info/{userId}")
    public ApiResponse<SecUser> clickEditGetInfo(@PathVariable Long userId) {
        return authService.clickEditGetInfo(userId);
    }

    @ApiOperation("超管编辑管理员信息(只编辑权限)")
    @PostMapping("acco-and-perm/modify-admin")
    public ApiResponse modifyAdmin(@RequestBody ModifyAdminDto dto) {
        return authService.modifyAdmin(dto);
    }

    @ApiOperation("超管重置密码")
    @PostMapping("acco-and-perm/reset-password")
    public ApiResponse resetPassword(@RequestBody ResetPasswordDto dto){
        return authService.resetPassword(dto);
    }

    @ApiOperation("超管移除管理员")
    @PostMapping("acco-and-perm/remove-admin")
    public ApiResponse removeAdmin(@RequestBody ShutAdminDto dto){
        return authService.shutAdmin(dto);
    }

    @ApiOperation("管理员列表")
    @PostMapping("acco-and-perm/admin-list")
    public ApiResponse<PageInfo<AdminVo>> adminList(@RequestBody BasePage basePage) {
        return ApiResponse.ofSuccess(authService.adminList(basePage));
    }

    @ApiOperation("修改当前账号手机号-new")
    @PostMapping("modify-phone")
    public ApiResponse modifyPhone(@RequestBody ModifyPhoneDTO modifyPhoneDTO) {
        SecUser secUser = secUserMapper.selectById(modifyPhoneDTO.getUserId());
        if (ObjectUtil.isNull(secUser)) {
            throw new BusinessException("当前管理员不存在");
        }
        SecUser phoneSecUser  = secUserMapper.selectOne(new LambdaQueryWrapper<SecUser>().eq(SecUser::getPhone,
                modifyPhoneDTO.getPhone()));
        if (ObjectUtil.isNotNull(phoneSecUser)) {
            throw new BusinessException("新手机号在系统中已存在");
        }
        secUser.setPhone(modifyPhoneDTO.getPhone());
        secUserMapper.updateById(secUser);
        return ApiResponse.ofSuccess();


    }
}
