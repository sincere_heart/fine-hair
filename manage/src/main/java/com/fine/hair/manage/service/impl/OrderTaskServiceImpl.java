package com.fine.hair.manage.service.impl;

import cn.hutool.core.lang.Snowflake;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.fine.hair.comm.enums.OrderStatus;
import com.fine.hair.comm.exception.BusinessException;
import com.fine.hair.comm.model.Consume;
import com.fine.hair.comm.model.Order;
import com.fine.hair.comm.model.SysConfig;
import com.fine.hair.comm.model.User;
import com.fine.hair.manage.mapper.ConsumeMapper;
import com.fine.hair.manage.mapper.OrderMapper;
import com.fine.hair.manage.mapper.SysConfigMapper;
import com.fine.hair.manage.mapper.UserMapper;
import com.fine.hair.manage.service.CommissionService;
import com.fine.hair.manage.service.OrderTaskService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2020/10/27 21:38
 */
@Slf4j
@Service
public class OrderTaskServiceImpl implements OrderTaskService {

    @Resource
    private OrderMapper orderMapper;

    @Resource
    private SysConfigMapper sysConfigMapper;

    @Resource
    private UserMapper userMapper;

    @Autowired
    private CommissionService commissionService;

    @Autowired
    private ConsumeMapper consumeMapper;

    @Autowired
    private Snowflake snowflake;

    public void syncOrderInfo() {
        SysConfig sysConfig = sysConfigMapper.selectOne(new LambdaQueryWrapper<>());
        // 查找未完成的订单
        List<Order> orderList = orderMapper.selectList(new LambdaQueryWrapper<Order>().eq(Order::getOrderStatus,
                OrderStatus.WILL_COMPLETE.getCode()));
        // 比较订单时间 是否超过配置的时间
        for (Order order : orderList) {
            long orderTime = order.getExecuteTime().getTime() / 1000;
            long nowTime = System.currentTimeMillis() / 1000;
            if (nowTime - orderTime > sysConfig.getOrderAutoConfirmTime() * 60) {
                // 是，自动确认
                //order.setOrderStatus(OrderStatus.COMPLETED.getCode().toString());
                User user = userMapper.selectById(order.getUserId());
                if (user == null) {
                    continue;
                }
                // 同步用户次数
                //taskUpdateUserServiceCount(user,order);
                // TODO 过期后，取消订单
                order.setOrderStatus(OrderStatus.CANCELED.getCode().toString());
                orderMapper.updateById(order);
            }
            // 不是，不做处理
        }
    }

    @Override
    public void updateUserServiceCount(User user, Order order) {
        Consume consume = new Consume();
        int count = 0;
        if ("1".equals(order.getItemType())) {
            if (user.getHfCount() < 1) {
                throw new BusinessException("初级调理次数不足，确认订单失败");
            }
            consume.setSeviceType(1);
            user.setHfCount(user.getHfCount() - 1);
            count = user.getHfCount();
        }
        if ("2".equals(order.getItemType())) {
            if (user.getYfCount() < 1) {
                throw new BusinessException("中级调理剩余次数不足，确认订单失败");
            }
            consume.setSeviceType(2);
            user.setYfCount(user.getYfCount() - 1);
            count = user.getYfCount();
        }
        if ("3".equals(order.getItemType())) {
            if (user.getFzCount() < 1) {
                throw new BusinessException("高级调理剩余次数不足，确认订单失败");
            }
            consume.setSeviceType(3);
            user.setFzCount(user.getFzCount() - 1);
            count = user.getFzCount();
        }
        if ("4".equals(order.getItemType())) {
            if (user.getDjCount() < 1) {
                throw new BusinessException("顶级调理剩余次数不足，确认订单失败");
            }
            consume.setSeviceType(4);
            user.setDjCount(user.getDjCount() - 1);
            count = user.getDjCount();
        }
        if ("5".equals(order.getItemType())) {
            if (user.getTbCount() < 1) {
                throw new BusinessException("头肩调理剩余次数不足，确认订单失败");
            }
            consume.setSeviceType(5);
            user.setTbCount(user.getTbCount() - 1);
            count = user.getTbCount();
        }
        if ("6".equals(order.getItemType())) {
            if (user.getYhCount() < 1) {
                throw new BusinessException("其他调理剩余次数不足，确认订单失败");
            }
            consume.setSeviceType(6);
            user.setYhCount(user.getYhCount() - 1);
            count = user.getYhCount();
        }
        // 同步用户信息
        user.setPoint(user.getPoint().add(order.getItemPrice()));
        userMapper.updateById(user);
        // 同步用户消费
        consume.setConsumeType(0);
        consume.setCount(1);
        consume.setItemId(Long.valueOf(order.getItemId()));
        consume.setId(snowflake.nextId());
        consume.setUserId(Long.valueOf(user.getId()));
        consume.setSurplusCount(count);
        consumeMapper.insert(consume);
        // 同步订单信息
        orderMapper.updateById(order);
        // 同步提成
        commissionService.syncCommission(order);
    }


    public void taskUpdateUserServiceCount(User user, Order order) {
        if ("1".equals(order.getItemType())) {
            if (user.getHfCount() < 1) {
                log.info("用户 {} 护发剩余次数不足，确认订单 {} 失败", user.getPhoneNumber(), order.getId());
                return;
            }
            user.setHfCount(user.getHfCount() - 1);
        }
        if ("2".equals(order.getItemType())) {
            if (user.getYfCount() < 1) {
                log.info("用户 {} 养发剩余次数不足，确认订单 {} 失败", user.getPhoneNumber(), order.getId());
                return;
            }
            user.setYfCount(user.getYfCount() - 1);
        }
        if ("3".equals(order.getItemType())) {
            if (user.getFzCount() < 1) {
                log.info("用户 {} 发质剩余次数不足，确认订单 {} 失败", user.getPhoneNumber(), order.getId());
                return;
            }
            user.setFzCount(user.getFzCount() - 1);
        }
        // 同步用户信息
        user.setPoint(user.getPoint().add(order.getItemPrice()));
        userMapper.updateById(user);
        // 同步订单信息
        orderMapper.updateById(order);
        // 同步提成
        commissionService.syncCommission(order);
    }


}
