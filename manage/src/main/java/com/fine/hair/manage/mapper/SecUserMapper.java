package com.fine.hair.manage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fine.hair.comm.model.SecUser;
import com.fine.hair.comm.utils.PageInfo;
import com.fine.hair.comm.vo.AdminVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>${Description}</p>
 *
 * @author mouseyCat
 * @date 2020/10/26 21:39
 */
@Mapper
public interface SecUserMapper extends BaseMapper<SecUser> {
    /**
     * @param username
     * @param phone
     * @return
     */
    SecUser findByUsernameOrNameOrPhone(@Param("username") String username, @Param("phone") String phone);

    /**
     * @param phone
     * @param username
     * @return
     */
    SecUser selectByPhoneAndUsername(@Param("phone") String phone, @Param("username") String username);

    /**
     * @return
     */
    List<AdminVo> selectSecUserList(@Param("pageInfo") PageInfo<AdminVo> pageInfo);

    /**
     * @param username
     * @return
     */
    SecUser selectByUsername(@Param("username") String username);

    /**
     * @param secUserId
     * @return
     */
    SecUser selectSecUserById(@Param("secUserId") Long secUserId);
}
