package com.fine.hair.manage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fine.hair.comm.model.ServiceTypeCharge;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author liheng
 * @since 2021-07-29
 */
public interface ServiceTypeChargeMapper extends BaseMapper<ServiceTypeCharge> {

}
