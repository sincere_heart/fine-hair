package com.fine.hair.manage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fine.hair.comm.dto.AddReservationBatchDto;
import com.fine.hair.comm.dto.AutoAddReservationBatchDto;
import com.fine.hair.comm.model.Order;
import com.fine.hair.comm.utils.ApiResponse;

/**
 * <p>${Description}</p>
 *
 * @author mouseyCat
 * @date 2020/10/4 15:12
 */
public interface OrderService extends IService<Order> {

    /**
     *
     * @param dto
     * @return
     */
    ApiResponse addReservationBatch(AddReservationBatchDto dto);

    /**
     *
     * @param dto
     * @return
     */
    ApiResponse autoAddReservationBatch(AutoAddReservationBatchDto dto);
}



