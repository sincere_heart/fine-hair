package com.fine.hair.manage.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.fine.hair.comm.dto.SearchStaffBranchVo;
import com.fine.hair.comm.utils.ApiResponse;
import com.fine.hair.comm.utils.TypePool;
import com.fine.hair.manage.mapper.BranchMapper;
import com.fine.hair.manage.mapper.SecUserMapper;
import com.fine.hair.manage.mapper.UserMapper;
import com.fine.hair.manage.util.JwtUtil;
import com.fine.hair.comm.model.Branch;
import com.fine.hair.comm.model.SecUser;
import com.fine.hair.comm.model.User;
import com.google.common.collect.Lists;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>字典相关控制层</p>
 *
 * @author mouseyCat
 * @date 2020/10/25 20:27
 */
@Api(tags = {"字典相关接口"})
@RestController
@RequestMapping("/dict/")
public class DictController {

    @Resource
    private BranchMapper branchMapper;
    @Resource
    private UserMapper userMapper;
    @Autowired
    private JwtUtil jwtUtil;
    @Resource
    private SecUserMapper secUserMapper;

    @GetMapping("order-status")
    @ApiOperation("订单状态")
    public ApiResponse orderStatus() {
        return ApiResponse.ofSuccess(TypePool.ORDER_STATUS);
    }

    @GetMapping("apply-status")
    @ApiOperation("审核状态")
    public ApiResponse applyStatus() {
        return ApiResponse.ofSuccess(TypePool.APPLY_STATUS);
    }

    @GetMapping("consumeType")
    @ApiOperation("消费类型")
    public ApiResponse consumeType() {
        return ApiResponse.ofSuccess(TypePool.CONSUME_TYPE);
    }

    @GetMapping("rewards-punishment-type")
    @ApiOperation("奖惩类型")
    public ApiResponse rewardsPunishmentType() {
        return ApiResponse.ofSuccess(TypePool.REWARDS_PUNISHMENT_TYPE);
    }

    @GetMapping("all-branch")
    @ApiOperation("所有门店")
    public ApiResponse<List<Branch>> allBranch(HttpServletRequest request) {
        List<Branch> branches = branchMapper.selectList(new LambdaQueryWrapper<Branch>().select(Branch::getId,
                Branch::getTitle));
        // 判断用户是不是门店管理员
        String usernameFromJwt = jwtUtil.getUsernameFromJWT(jwtUtil.getJwtFromRequest(request));
        SecUser secUser = secUserMapper.selectOne(new LambdaQueryWrapper<SecUser>().eq(SecUser::getUsername,
                usernameFromJwt));

        if (secUser.getBranchId() == null) {
            return ApiResponse.ofSuccess(branches);
        } else {
            List<Branch> result =
                    branches.stream().filter(b -> b.getId().equals(secUser.getBranchId())).collect(Collectors.toList());
            return ApiResponse.ofSuccess(result);
        }

    }

    @GetMapping("searc-staff-branch/{staffId}")
    @ApiOperation("获取员工所在的所有门店(包括挂店的门店)")
    public ApiResponse<List<SearchStaffBranchVo>> searchStaffBranch(@PathVariable String staffId) {
        User user = userMapper.selectById(staffId);
        List<String> result = Lists.newArrayList();
        if (StrUtil.isNotBlank(user.getPutBranchId())) {
            result = Arrays.asList(user.getPutBranchId().split(","));
        }
        result.add(user.getBranchId().toString());
        List<Branch> branches = branchMapper.selectBatchIds(result);
        List<SearchStaffBranchVo> vos = Lists.newArrayList();
        SearchStaffBranchVo vo;
        for (Branch branch : branches) {
            vo = new SearchStaffBranchVo();
            vo.setCurrBranch(user.getBranchId().equals(branch.getId()));
            vo.setBranchId(branch.getId().toString());
            vo.setBranchTitle(branch.getTitle());
            vos.add(vo);
        }
        return ApiResponse.ofSuccess(vos);
    }
}
