package com.fine.hair.manage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fine.hair.comm.dto.SalarytjDTO;
import com.fine.hair.comm.dto.UserListDTO;
import com.fine.hair.comm.model.User;
import com.fine.hair.comm.pojo.StaffSimpleInfo;
import com.fine.hair.comm.utils.PageInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>${Description}</p>
 *
 * @author mouseyCat
 * @date 2020/10/26 21:39
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {


    /**
     * @param userPage
     * @return
     */
    List<User> selectUserList(@Param("userPage") PageInfo<User> userPage, @Param("dto") UserListDTO dto);

    /**
     * @param phone
     * @param wechatNickName
     * @return
     */
    List<User> selectByPhoneOrWechatName(@Param("phone") String phone, @Param("wechatNickName") String wechatNickName);



    List<User> selectStaffList(@Param("userPage") Page<User> userPage, @Param("branchId") Long branchId,@Param("phoneOrName")String phoneOrName);


    /**
     * @param userPage
     * @param dto
     * @return
     */
    List<User> selectUserListAll(@Param("userPage") PageInfo<User> userPage, @Param("dto") UserListDTO dto);

    List<User> selectYesterdayBranch();

    List<User> selectTodayBranch();

    List<User> selectLastMonthBranch();

    List<User> selectThisYearBranch();

    List<Map<String, Object>> selectSalarytj(@Param("dto")SalarytjDTO dto,@Param("pageInfo")  PageInfo<Map<String, Object>> pageInfo);
}
