package com.fine.hair.manage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fine.hair.comm.dto.*;
import com.fine.hair.comm.model.Order;
import com.fine.hair.comm.utils.PageInfo;
import com.fine.hair.comm.vo.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>${Description}</p>
 *
 * @author mouseyCat
 * @date 2020/10/26 21:39
 */
@Mapper
public interface OrderMapper extends BaseMapper<Order> {
    /**
     * 预约信息
     *
     * @param orderId 订单id
     * @return 预约信息
     */
    OrderInfoVo selectOrderInfoByOrderId(@Param("orderId") Long orderId);

    /**
     * @param dto
     * @param pageInfo
     * @return
     */
    List<Order> selectStaffOrderInfoByCondition(@Param("dto") StaffOrderInfoDto dto,
                                                @Param("pageInfo") PageInfo<Order> pageInfo);

    /**
     * @param dto
     * @return
     */
    List<IncomeStat> incomeStatByDateAndBranchId(@Param("dto") IncomeStatDto dto);

    /**
     * @param date
     * @return
     */
    List<Order> selectOrderByDate(@Param("date") String date, @Param("dto") DateDto dto);

    /**
     * 查询用户积分
     * @param userId
     * @return
     */
    BigDecimal selectUserIntegral(@Param("userId") String userId);

    /**
     * @param date
     * @return
     */
    List<Order> selectOrderByDateAndUserIdAndBranchId(@Param("date") String date, @Param("userId") String userId,
                                                      @Param("branchId") String branchId);

    /**
     * @param userId
     * @return
     */
    Integer selectCompletedOrderCount(@Param("userId") String userId);


    /**
     * @param pageInfo
     * @return
     */
    List<Order> selectReservationList(@Param("pageInfo") PageInfo<Order> pageInfo,
                                      @Param("branchId") String branchId, @Param("dto") ReservationListDto dto);

    /**
     * @param pageInfo
     * @return
     */
    List<Order> selectReservationListNoneBranchIds(@Param("pageInfo") PageInfo<Order> pageInfo,
                                                   @Param("dto") ReservationListDto dto);


    /**
     * @param dto
     * @return
     */
    List<IncomeInfo> incomeByDateAndBranchId(@Param("dto") IncomeDto dto);



    List<Order> selectAllOrderByDate(@Param("date") String date);



    List<Order> selectYesterdayBranch(Long branchId);

    List<Order> selectTodayBranch(Long branchId);

    List<Order> selectLastMonthBranch(Long branchId);

    List<Order> selectThisYearBranch(Long branchId);


    List<Order> selectYesterdayBranchAll(Long branchId);

    List<Order> selectTodayBranchAll(Long branchId);

    List<Order> selectLastMonthBranchAll(Long branchId);

    List<Order> selectThisYearBranchAll(Long branchId);

    List<Order> selectThisQuarterBranchAll(@Param("branchId") String branchId);

    List<Order> selectThisMonthBranchAll(@Param("branchId") Long branchId);

    Integer selectFlowStat(@Param("branchId") String branchId, @Param("startTime") String startTime,
                           @Param("endTime") String endTime);

    List<Order> selectByYear(@Param("i") Integer i, @Param("branchId") String branchId);


    int selectBusyCountByOrderTime(@Param("orderTime") Date orderTime, @Param("technicianId") String technicianId);

    List<Map<String, Object>> selectGdsrtj(@Param("dto") GdzctjDTO dto, @Param("pageInfo") PageInfo<Map<String, Object>> pageInfo);

    List<Map<String, Object>> selectOrderInfoByUserId(@Param("userId") Long userId, @Param("pageInfo") PageInfo<Map<String, Object>> pageInfo);
}
