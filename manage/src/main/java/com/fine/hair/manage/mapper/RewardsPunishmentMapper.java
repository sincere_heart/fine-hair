package com.fine.hair.manage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fine.hair.comm.dto.DateDto;
import com.fine.hair.comm.dto.StaffRewardsPunishmentInfoDto;
import com.fine.hair.comm.model.RewardsPunishment;
import com.fine.hair.comm.utils.PageInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>${Description}</p>
 *
 * @author mouseyCat
 * @date 2020/10/26 21:39
 */
@Mapper
public interface RewardsPunishmentMapper extends BaseMapper<RewardsPunishment> {

    /**
     * @param date
     * @return
     */
    List<RewardsPunishment> selectByDate(@Param("date") String date, @Param("dto")DateDto dto);

    /**
     * @param date
     * @return
     */
    List<RewardsPunishment> selectByDateAndUserId(@Param("date") String date, @Param("userId") String userId);

    /**
     * @param dto
     * @param pageInfo
     * @return
     */
    List<RewardsPunishment> selectStaffRewardsPunishmentInfoByCondition(@Param("dto") StaffRewardsPunishmentInfoDto dto, @Param("pageInfo") PageInfo<RewardsPunishment> pageInfo);


    List<RewardsPunishment> selectYesterdayBranch(Long branchId);

    List<RewardsPunishment> selectTodayBranch(Long branchId);

    List<RewardsPunishment> selectLastMonthBranch(Long branchId);

    List<RewardsPunishment> selectThisYearBranch(Long branchId);


    List<RewardsPunishment> selectThisMonthBranchAll(Long branchId);

    List<RewardsPunishment> selectThisQuarterBranchAll(String branchId);

    List<RewardsPunishment> selectByStartAndEndTime(@Param("startTime") String startTime,
                                                    @Param("endTime") String endTime,
                                                    @Param("branchId") String branchId);
}
