SET NAMES utf8mb4;
# 禁用外键约束
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sec_permission
-- ----------------------------
DROP TABLE IF EXISTS `sec_permission`;
CREATE TABLE `sec_permission`
(
    `id`         bigint(64)  NOT NULL COMMENT '主键',
    `icon`       varchar(50) COMMENT '图标',
    `title`      varchar(50) NOT NULL COMMENT '名称',
    `url`        varchar(1000) DEFAULT NULL COMMENT '类型为页面时，代表前端路由地址，类型为按钮时，代表后端接口地址',
    `type`       int(2)      NOT NULL COMMENT '权限类型，页面-1，按钮-2',
    `permission` varchar(50)   DEFAULT NULL COMMENT '权限表达式',
    `method`     varchar(50)   DEFAULT NULL COMMENT '后端接口访问方式',
    `sort`       int(11)     NOT NULL COMMENT '排序',
    `parent_id`  bigint(64)  NOT NULL COMMENT '父级id',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8 COMMENT ='权限表';

-- ----------------------------
-- Records of sec_permission
-- ----------------------------
BEGIN;
INSERT INTO `sec_permission`
VALUES (1072806379288399872, '测试页面', '/test', 1, 'page:test', NULL, 1, 0);
INSERT INTO `sec_permission`
VALUES (1072806379313565696, '测试页面-查询', '/**/test', 2, 'btn:test:query', 'GET', 1, 1072806379288399872);
INSERT INTO `sec_permission`
VALUES (1072806379330342912, '测试页面-添加', '/**/test', 2, 'btn:test:insert', 'POST', 2, 1072806379288399872);
INSERT INTO `sec_permission`
VALUES (1072806379342925824, '监控在线用户页面', '/monitor', 1, 'page:monitor:online', NULL, 2, 0);
INSERT INTO `sec_permission`
VALUES (1072806379363897344, '在线用户页面-查询', '/**/api/monitor/online/user', 2, 'btn:monitor:online:query', 'GET', 1,
        1072806379342925824);
INSERT INTO `sec_permission`
VALUES (1072806379384868864, '在线用户页面-踢出', '/**/api/monitor/online/user/kickout', 2, 'btn:monitor:online:kickout',
        'DELETE', 2, 1072806379342925824);
COMMIT;

-- ----------------------------
-- Table structure for sec_role
-- ----------------------------
DROP TABLE IF EXISTS `sec_role`;
CREATE TABLE `sec_role`
(
    `id`               bigint(64)  NOT NULL COMMENT '主键',
    `name`             varchar(50) NOT NULL COMMENT '角色名',
    `description`      varchar(100)         DEFAULT NULL COMMENT '描述',
    `create_time`      datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '(申请时间)创建时间',
    `last_update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '最近一次更新时间',
    PRIMARY KEY (`id`),
    UNIQUE KEY `name` (`name`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8 COMMENT ='角色表';

-- ----------------------------
-- Records of sec_role
-- ----------------------------
BEGIN;
INSERT INTO `sec_role`
VALUES (1072806379208708096, '管理员', '超级管理员', now(), now());
INSERT INTO `sec_role`
VALUES (1072806379238068224, '普通用户', '普通用户', now(), now());
COMMIT;

-- ----------------------------
-- Table structure for sec_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `sec_role_permission`;
CREATE TABLE `sec_role_permission`
(
    `role_id`       bigint(64) NOT NULL COMMENT '角色主键',
    `permission_id` bigint(64) NOT NULL COMMENT '权限主键',
    PRIMARY KEY (`role_id`, `permission_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8 COMMENT ='角色权限关系表';

-- ----------------------------
-- Records of sec_role_permission
-- ----------------------------
BEGIN;
INSERT INTO `sec_role_permission`
VALUES (1072806379208708096, 1072806379288399872);
INSERT INTO `sec_role_permission`
VALUES (1072806379208708096, 1072806379313565696);
INSERT INTO `sec_role_permission`
VALUES (1072806379208708096, 1072806379330342912);
INSERT INTO `sec_role_permission`
VALUES (1072806379208708096, 1072806379342925824);
INSERT INTO `sec_role_permission`
VALUES (1072806379208708096, 1072806379363897344);
INSERT INTO `sec_role_permission`
VALUES (1072806379208708096, 1072806379384868864);
INSERT INTO `sec_role_permission`
VALUES (1072806379238068224, 1072806379288399872);
INSERT INTO `sec_role_permission`
VALUES (1072806379238068224, 1072806379313565696);
COMMIT;

-- ----------------------------
-- Table structure for sec_user
-- ----------------------------
DROP TABLE IF EXISTS `sec_user`;
CREATE TABLE `sec_user`
(
    `id`               bigint(64)  NOT NULL COMMENT '主键',
    `branch_id`        bigint(64) COMMENT '门店管理员管理的门店id,为空则不是门店管理员',
    `username`         varchar(50) NOT NULL COMMENT '用户名',
    `password`         varchar(60) NOT NULL COMMENT '密码',
    `phone`            varchar(11)          DEFAULT NULL COMMENT '手机',
    `create_time`      datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '(申请时间)创建时间',
    `last_update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '最近一次更新时间',
    `status`           int         NOT NULL DEFAULT 0 COMMENT '是否删除 0否 1是',
    PRIMARY KEY (`id`),
    UNIQUE KEY `username` (`username`),
    UNIQUE KEY `phone` (`phone`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4 COMMENT ='管理员用户表';

-- ----------------------------
-- Records of sec_user
-- ----------------------------
BEGIN;
INSERT INTO `sec_user`
VALUES (1072806377661009920, 'admin', '$2a$10$64iuSLkKNhpTN19jGHs7xePvFsub7ZCcCmBqEYw8fbACGTE3XetYq', '管理员',
        '17300000000', 'admin@mouseyCat.com', 785433600000, 1, 1, now(), now());
INSERT INTO `sec_user`
VALUES (1072806378780889088, 'user', '$2a$10$OUDl4thpcHfs7WZ1kMUOb.ZO5eD4QANW5E.cexBLiKDIzDNt87QbO', '普通用户',
        '17300001111', 'user@mouseyCat.com', 785433600000, 1, 1, now(), now());
COMMIT;

-- ----------------------------
-- Table structure for sec_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sec_user_role`;
CREATE TABLE `sec_user_role`
(
    `user_id` bigint(64) NOT NULL COMMENT '用户主键',
    `role_id` bigint(64) NOT NULL COMMENT '角色主键',
    PRIMARY KEY (`user_id`, `role_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8 COMMENT ='用户角色关系表';

-- ----------------------------
-- Records of sec_user_role
-- ----------------------------
BEGIN;
INSERT INTO `sec_user_role`
VALUES (1072806377661009920, 1072806379208708096);
INSERT INTO `sec_user_role`
VALUES (1072806378780889088, 1072806379238068224);
COMMIT;



create database hqs;
use hqs;


-- ----------------------------
-- Table structure for user_login_account
-- ----------------------------
DROP TABLE IF EXISTS `user_login_account`;
CREATE TABLE IF NOT EXISTS `user_login_account`
(
    `user_id`          bigint NOT NULL AUTO_INCREMENT COMMENT 'user id',
    `wx_account_type`  int COMMENT '帐号类型. 0, 未知; 1, 微信小程序openId 2, 微信unionId; 3, 微信公众号openId;',
    `wx_account`       varchar(64) COMMENT '账号',
    `session_key`      varchar(255) COMMENT '小程序sessionKey',
    `sex`              varchar(10) COMMENT '性别',
    `address`          varchar(255) COMMENT '地区',
    `create_time`      datetime        DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `last_update_time` datetime        DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最近一次更新时间',
    `status`           int    NOT NULL DEFAULT 0 COMMENT '是否删除 0否 1是',
    index (user_id)
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_unicode_ci COMMENT = '用户登陆表';

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user`
(
    `id`               bigint(20)   NOT NULL COMMENT 'id',
    `branch_id`        bigint(20) COMMENT '门店id(员工所属门店)',
    `phone_number`     varchar(20)  NOT NULL COMMENT '手机号',
    `nick_name`        varchar(200) NOT NULL COMMENT '用户昵称',
    `head_photo`       varchar(255) NOT NULL COMMENT '用户头像',
    `birthday`         date COMMENT '用户生日',
    `hf_count`         varchar(255) NOT NULL COMMENT '剩余护发服务次数',
    `yf_count`         varchar(255) NOT NULL COMMENT '剩余养发服务次数',
    `fz_count`         varchar(255) NOT NULL COMMENT '剩余发质服务次数',
    `resume`           varchar(500) COMMENT '个人简介',
    `sex`              varchar(10) COMMENT '性别',
    `address`          varchar(255) COMMENT '地区',
    `create_time`      datetime              DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `last_update_time` datetime              DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最近一次更新时间',
    `status`           int          NOT NULL DEFAULT 0 COMMENT '是否删除 0否 1是',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_unicode_ci COMMENT = '用户表，技师与用户在同一张表';

-- ----------------------------
-- Table structure for order
-- ----------------------------
DROP TABLE IF EXISTS `order`;
CREATE TABLE IF NOT EXISTS `order`
(
    `id`               bigint NOT NULL COMMENT 'id',
    `user_id`          bigint NOT NULL COMMENT '订单所属用户',
    `order_status`     int    NOT NULL DEFAULT 1 COMMENT '订单状态 1未开始 2已完成 3已取消',
    `branch_id`        int    NOT NULL DEFAULT 1 COMMENT '门店id',
    `technician_id`    bigint NOT NULL COMMENT '技师id',
    `item_id`          bigint COMMENT '项目id',
    `execute_time`     datetime COMMENT '服务开始时间',
    `remark`           datetime COMMENT '用户备注',
    `create_time`      datetime        DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `last_update_time` datetime        DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最近一次更新时间',
    `status`           int    NOT NULL DEFAULT 0 COMMENT '是否删除 0否 1是',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_unicode_ci COMMENT = '订单表';

-- ----------------------------
-- Table structure for evaluate
-- ----------------------------
DROP TABLE IF EXISTS `evaluate`;
CREATE TABLE IF NOT EXISTS `evaluate`
(
    `id`               bigint NOT NULL COMMENT 'id',
    `technician_id`    bigint NOT NULL COMMENT '评价对象id',
    `evaluate_id`      bigint NOT NULL COMMENT '评价者id',
    `content`          bigint NOT NULL COMMENT '评价内容',
    `score`            int    NOT NULL COMMENT '综合满意 1-2-3-4-5 颗星',
    `create_time`      datetime        DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `last_update_time` datetime        DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最近一次更新时间',
    `status`           int    NOT NULL DEFAULT 0 COMMENT '是否删除 0否 1是',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_unicode_ci COMMENT = '评价表';

-- ----------------------------
-- Table structure for branch
-- ----------------------------
DROP TABLE IF EXISTS `branch`;
CREATE TABLE IF NOT EXISTS `branch`
(
    `id`               bigint        NOT NULL COMMENT 'id',
    `title`            bigint        NOT NULL COMMENT '门店名称',
    `images`           varchar(1000) NOT NULL COMMENT '门店图片,多图片则逗号隔开',
    `address`          varchar(255)  NOT NULL COMMENT '门店地址',
    `tel`              varchar(255)  NOT NULL COMMENT '门店热线',
    `business_time`    varchar(100)  NOT NULL COMMENT '营业时间',
    `lng`              double(10, 6) NOT NULL COMMENT '经度',
    `lat`              double(10, 6) NOT NULL COMMENT '维度',
    `create_time`      datetime               DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `last_update_time` datetime               DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最近一次更新时间',
    `status`           int           NOT NULL DEFAULT 0 COMMENT '是否删除 0否 1是',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_unicode_ci COMMENT = '门店表';

-- ----------------------------
-- Table structure for banner
-- ----------------------------
DROP TABLE IF EXISTS `banner`;
CREATE TABLE IF NOT EXISTS `banner`
(
    `id`               bigint       NOT NULL COMMENT 'id',
    `url`              varchar(500) NOT NULL COMMENT '图片地址',
    `priority`         int          NOT NULL COMMENT '优先级，1>2>3>4',
    `create_time`      datetime              DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `last_update_time` datetime              DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最近一次更新时间',
    `status`           int          NOT NULL DEFAULT 0 COMMENT '是否删除 0否 1是',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_unicode_ci COMMENT = '轮播图表';

-- ----------------------------
-- Table structure for item
-- ----------------------------
DROP TABLE IF EXISTS `item`;
CREATE TABLE IF NOT EXISTS `item`
(
    `id`               bigint         NOT NULL COMMENT 'id',
    `image`            varchar(500)   NOT NULL COMMENT '项目图片',
    `title`            varchar(255)   NOT NULL COMMENT '项目名称',
    `price`            decimal(10, 2) NOT NULL COMMENT '项目价格',
    `content`          varchar(500)   NOT NULL COMMENT '项目描述',
    `time`             datetime       NOT NULL COMMENT '项目执行时间',
    `create_time`      datetime                DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `last_update_time` datetime                DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最近一次更新时间',
    `status`           int            NOT NULL DEFAULT 0 COMMENT '是否删除 0否 1是',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_unicode_ci COMMENT = '项目表';

-- ----------------------------
-- Table structure for consume
-- ----------------------------
DROP TABLE IF EXISTS `consume`;
CREATE TABLE IF NOT EXISTS `consume`
(
    `id`               bigint   NOT NULL COMMENT 'id',
    `user_id`          bigint   NOT NULL COMMENT '用户id',
    `consume_type`     int      NOT NULL COMMENT '0消费 1充值',
    `count`            int      NOT NULL COMMENT '次数(充值为+ 消费为-)',
    `item_id`          bigint   NOT NULL COMMENT '项目id',
    `time`             datetime NOT NULL COMMENT '消费时间',
    `create_time`      datetime          DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `last_update_time` datetime          DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最近一次更新时间',
    `status`           int      NOT NULL DEFAULT 0 COMMENT '是否删除 0否 1是',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_unicode_ci COMMENT = '消费明细表';

-- ----------------------------
-- Table structure for apply
-- ----------------------------
DROP TABLE IF EXISTS `apply`;
CREATE TABLE IF NOT EXISTS `apply`
(
    `id`               bigint       NOT NULL COMMENT 'id',
    `user_id`          bigint       NOT NULL COMMENT '用户id',
    `nick_name`        varchar(255) NOT NULL COMMENT '姓名',
    `phone_number`     varchar(255) NOT NULL COMMENT '手机号',
    `branch_id`        bigint       NOT NULL COMMENT '门店id',
    `apply_status`     int                   DEFAULT 0 COMMENT '0待同意 1同意 2拒绝',
    `create_time`      datetime              DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `last_update_time` datetime              DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最近一次更新时间',
    `status`           int          NOT NULL DEFAULT 0 COMMENT '是否删除 0否 1是',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_unicode_ci COMMENT = '申请表';

-- ----------------------------
-- Table structure for rewards_punishment
-- ----------------------------
DROP TABLE IF EXISTS `rewards_punishment`;
CREATE TABLE IF NOT EXISTS `rewards_punishment`
(
    `id`               bigint         NOT NULL COMMENT 'id',
    `user_id`          bigint         NOT NULL COMMENT '用户id',
    `type`             int            NOT NULL COMMENT '奖惩类型 0奖励 1惩罚',
    `amount`           decimal(10, 2) NOT NULL COMMENT '金额',
    `remark`           varchar(255) COMMENT '备注',
    `operator_id`      bigint COMMENT '录入信息的管理员id',
    `create_time`      datetime                DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `last_update_time` datetime                DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最近一次更新时间',
    `status`           int            NOT NULL DEFAULT 0 COMMENT '是否删除 0否 1是',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_unicode_ci COMMENT = '奖惩表';

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE IF NOT EXISTS `sys_config`
(
    `id`                      bigint NOT NULL COMMENT 'id',
    `order_auto_confirm_time` bigint NOT NULL DEFAULT 10 COMMENT '订单自动确认时间',
    `create_time`             datetime        DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `last_update_time`        datetime        DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最近一次更新时间',
    `status`                  int    NOT NULL DEFAULT 0 COMMENT '是否删除 0否 1是',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_unicode_ci COMMENT = '系统配置表';

-- ----------------------------
-- Table structure for commission
-- ----------------------------
DROP TABLE IF EXISTS `commission`;
CREATE TABLE IF NOT EXISTS `commission`
(
    `id`               bigint         NOT NULL COMMENT 'id',
    `section_start`    decimal(10, 2) NOT NULL DEFAULT 10 COMMENT '区间-开始，一个区间包含开始与结束,比如 1-100，101-200',
    `section_end`      decimal(10, 2) NOT NULL DEFAULT 10 COMMENT '区间-结束',
    `proportion`       bigint         NOT NULL DEFAULT 10 COMMENT '每单提成比例 填 10 就是 每单的 10%',
    `create_time`      datetime                DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `last_update_time` datetime                DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最近一次更新时间',
    `status`           int            NOT NULL DEFAULT 0 COMMENT '是否删除 0否 1是',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_unicode_ci COMMENT = '订单提成表';

-- ----------------------------
-- Table structure for feedback
-- ----------------------------
DROP TABLE IF EXISTS `feedback`;
CREATE TABLE IF NOT EXISTS `feedback`
(
    `id`               bigint         NOT NULL COMMENT 'id',
    `user_id`          bigint         NOT NULL COMMENT '用户id',
    `description`      varchar(500)         NOT NULL  COMMENT '描述详情',
    `create_time`      datetime                DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `last_update_time` datetime                DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最近一次更新时间',
    `status`           int            NOT NULL DEFAULT 0 COMMENT '是否删除 0否 1是',
    PRIMARY KEY (`id`) USING BTREE,
    index (user_id)
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_unicode_ci COMMENT = '用户反馈表';