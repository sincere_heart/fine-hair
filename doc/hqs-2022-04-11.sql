/*
 Navicat Premium Data Transfer

 Source Server         : haoqingsi
 Source Server Type    : MySQL
 Source Server Version : 80022
 Source Host           : 132.232.100.229:3306
 Source Schema         : hqs

 Target Server Type    : MySQL
 Target Server Version : 80022
 File Encoding         : 65001

 Date: 11/04/2022 14:42:29
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for apply
-- ----------------------------
DROP TABLE IF EXISTS `apply`;
CREATE TABLE `apply`  (
  `id` bigint(0) NOT NULL COMMENT 'id',
  `user_id` bigint(0) NOT NULL COMMENT '用户id',
  `nick_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '姓名',
  `phone_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '手机号',
  `branch_id` bigint(0) NOT NULL COMMENT '门店id',
  `apply_status` int(0) NULL DEFAULT 0 COMMENT '0待同意 1同意 2拒绝',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `last_update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '最近一次更新时间',
  `status` int(0) NOT NULL DEFAULT 0 COMMENT '是否删除 0否 1是',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '用户申请成为员工的申请表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for banner
-- ----------------------------
DROP TABLE IF EXISTS `banner`;
CREATE TABLE `banner`  (
  `id` bigint(0) NOT NULL COMMENT 'id',
  `url` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '图片链接',
  `priority` bigint(0) NOT NULL COMMENT '优先级，1>2>3>4',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `last_update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '最近一次更新时间',
  `status` int(0) NOT NULL DEFAULT 0 COMMENT '是否删除 0否 1是',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '轮播图表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of banner
-- ----------------------------
INSERT INTO `banner` VALUES (1469878027742744576, 'https://api.haoqingsi.cn/images/2021-12-12/36CRcHEqfa58U1Uk.png', 1, '2021-12-12 11:53:25', '2021-12-12 11:53:25', 0);
INSERT INTO `banner` VALUES (1469878084734947328, 'https://api.haoqingsi.cn/images/2021-12-12/nKV9mjFqOpfhRJn0.png', 3, '2021-12-12 11:53:39', '2021-12-12 11:53:39', 0);

-- ----------------------------
-- Table structure for branch
-- ----------------------------
DROP TABLE IF EXISTS `branch`;
CREATE TABLE `branch`  (
  `id` bigint(0) NOT NULL COMMENT 'id',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '门店名称',
  `images` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '门店图片,多图片则逗号隔开',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '门店地址',
  `tel` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '门店热线',
  `business_time` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '营业时间',
  `lng` double(10, 6) NOT NULL COMMENT '经度',
  `lat` double(10, 6) NOT NULL COMMENT '维度',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `last_update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '最近一次更新时间',
  `status` int(0) NOT NULL DEFAULT 0 COMMENT '是否删除 0否 1是',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '门店表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of branch
-- ----------------------------
INSERT INTO `branch` VALUES (1469895024828354560, '南河花园店', 'https://api.haoqingsi.cn/images/2022-02-05/raKo1KiqeBpU1dFU.jpg', '四川省绵阳市涪城区南河东街11-5号南河花园旁', '0816-2267803', '08:30 - 20:30', 104.762689, 31.453157, '2021-12-12 13:00:57', '2022-02-05 22:47:58', 0);

-- ----------------------------
-- Table structure for calendar
-- ----------------------------
DROP TABLE IF EXISTS `calendar`;
CREATE TABLE `calendar`  (
  `id` bigint(0) NOT NULL COMMENT 'id',
  `count` bigint(0) NOT NULL COMMENT '几天一次',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '描述',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL,
  `last_update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '最近一次更新时间',
  `status` int(0) NOT NULL DEFAULT 0 COMMENT '是否删除 0否 1是',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '周期配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of calendar
-- ----------------------------
INSERT INTO `calendar` VALUES (1, 1, '一天一次', NULL, NULL, '2021-01-29 10:40:55', 1);
INSERT INTO `calendar` VALUES (2, 2, '两天一次', NULL, NULL, '2021-02-04 14:59:26', 1);
INSERT INTO `calendar` VALUES (3, 3, '三天一次', NULL, NULL, '2021-02-07 23:39:56', 1);
INSERT INTO `calendar` VALUES (4, 4, '四天一次', NULL, NULL, '2021-01-28 19:55:45', 0);
INSERT INTO `calendar` VALUES (1354759932897005568, 6, '6天一次', '2021-01-28 19:57:02', NULL, '2021-01-29 10:41:36', 1);
INSERT INTO `calendar` VALUES (1354991030398750720, 1, '一天一次', '2021-01-29 11:13:30', NULL, '2021-01-29 11:15:11', 1);
INSERT INTO `calendar` VALUES (1354991465683619840, 1, '一天一次', '2021-01-29 11:15:14', NULL, '2021-01-29 11:15:14', 0);
INSERT INTO `calendar` VALUES (1354991826087579648, 5, '五天一次', '2021-01-29 11:16:40', NULL, '2021-01-29 11:16:40', 0);

-- ----------------------------
-- Table structure for commission
-- ----------------------------
DROP TABLE IF EXISTS `commission`;
CREATE TABLE `commission`  (
  `id` bigint(0) NOT NULL COMMENT 'id',
  `section_start` bigint(0) NOT NULL DEFAULT 1 COMMENT '区间-开始，一个区间包含开始与结束,比如 1-100，101-200',
  `section_end` bigint(0) NOT NULL DEFAULT 10 COMMENT '区间-结束',
  `proportion` decimal(10, 2) NOT NULL DEFAULT 10.00 COMMENT '每单提成比例 填 10 就是 每单的 10%',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `last_update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '最近一次更新时间',
  `status` int(0) NOT NULL DEFAULT 0 COMMENT '是否删除 0否 1是',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '订单提成表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of commission
-- ----------------------------
INSERT INTO `commission` VALUES (0, 1, 5, 10.00, '2020-11-12 21:31:35', '2021-01-28 20:10:09', 0);
INSERT INTO `commission` VALUES (1325322388598034432, 7, 11, 30.00, '2020-11-08 14:21:15', '2021-02-19 21:53:04', 1);
INSERT INTO `commission` VALUES (1362765516946673664, 6, 6, 20.00, '2021-02-19 22:06:32', '2021-02-19 22:06:32', 0);
INSERT INTO `commission` VALUES (1362765597506670592, 7, 7, 30.00, '2021-02-19 22:06:52', '2021-02-19 22:06:52', 0);

-- ----------------------------
-- Table structure for consume
-- ----------------------------
DROP TABLE IF EXISTS `consume`;
CREATE TABLE `consume`  (
  `id` bigint(0) NOT NULL COMMENT 'id',
  `user_id` bigint(0) NOT NULL COMMENT '用户id',
  `consume_type` int(0) NOT NULL COMMENT '0消费 1充值',
  `count` int(0) NOT NULL COMMENT '次数(充值为+ 消费为-)',
  `item_id` bigint(0) NOT NULL COMMENT '项目id 为0时表示充值',
  `sevice_type` int(0) NOT NULL DEFAULT 1 COMMENT '服务类型 123456',
  `time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '消费时间',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `last_update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '最近一次更新时间',
  `status` int(0) NOT NULL DEFAULT 0 COMMENT '是否删除 0否 1是',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '消费明细表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for evaluate
-- ----------------------------
DROP TABLE IF EXISTS `evaluate`;
CREATE TABLE `evaluate`  (
  `id` bigint(0) NOT NULL COMMENT 'id',
  `technician_id` bigint(0) NOT NULL COMMENT '评价对象(技师)id',
  `evaluate_id` bigint(0) NOT NULL COMMENT '评价者(用户)id',
  `content` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '评价内容',
  `score` decimal(10, 2) NOT NULL COMMENT '综合满意 1-2-3-4-5 颗星',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `last_update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '最近一次更新时间',
  `status` int(0) NOT NULL DEFAULT 0 COMMENT '是否删除 0否 1是',
  `label` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '标签id，逗号隔开 1效果显著 2技术专业 3认真负责 4耐心负责 5环境优越',
  `order_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '订单id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '评价表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for feedback
-- ----------------------------
DROP TABLE IF EXISTS `feedback`;
CREATE TABLE `feedback`  (
  `id` bigint(0) NOT NULL COMMENT 'id',
  `user_id` bigint(0) NOT NULL COMMENT '用户id',
  `description` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '描述详情',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `last_update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '最近一次更新时间',
  `status` int(0) NOT NULL DEFAULT 0 COMMENT '是否删除 0否 1是',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '用户反馈表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for inventory
-- ----------------------------
DROP TABLE IF EXISTS `inventory`;
CREATE TABLE `inventory`  (
  `id` bigint(0) NOT NULL COMMENT 'id',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '商品名称',
  `img` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '商品图片',
  `specification` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '规格',
  `sum` int(0) NOT NULL COMMENT '总数',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `last_update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '最近一次更新时间',
  `status` int(0) NOT NULL DEFAULT 0 COMMENT '是否删除 0否 1是',
  `branch_id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '门店id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '库存表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of inventory
-- ----------------------------
INSERT INTO `inventory` VALUES (1478896672548458496, '飘柔洗发水', 'https://api.haoqingsi.cn/images/2022-01-06/htQ7rOwrWN0rXsOO.png', '瓶', 100, '2022-01-06 09:10:17', '2022-01-06 09:10:17', 0, '1469895024828354560');

-- ----------------------------
-- Table structure for inventory_put_record
-- ----------------------------
DROP TABLE IF EXISTS `inventory_put_record`;
CREATE TABLE `inventory_put_record`  (
  `id` bigint(0) NOT NULL COMMENT 'id',
  `count` int(0) NOT NULL COMMENT '本次入库数量',
  `branch_id` bigint(0) NOT NULL COMMENT '本次入库的门店id',
  `inventory_id` bigint(0) NOT NULL COMMENT '本次入库的商品id',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '本次入库的备注',
  `total_money` decimal(10, 2) NOT NULL COMMENT '本次入库的总价格',
  `user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '领用人用户名',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `last_update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '最近一次更新时间',
  `status` int(0) NOT NULL DEFAULT 0 COMMENT '是否删除 0否 1是',
  `money` decimal(10, 2) NULL DEFAULT NULL COMMENT '单价',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '库存入库记录表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of inventory_put_record
-- ----------------------------
INSERT INTO `inventory_put_record` VALUES (1478896672565235712, 100, 1469895024828354560, 1478896672548458496, '500ml', 600.00, 'mouseyCat', '2022-01-06 09:10:17', '2022-01-06 09:10:17', 0, 6.00);

-- ----------------------------
-- Table structure for inventory_receive_record
-- ----------------------------
DROP TABLE IF EXISTS `inventory_receive_record`;
CREATE TABLE `inventory_receive_record`  (
  `id` bigint(0) NOT NULL COMMENT 'id',
  `branch_id` bigint(0) NOT NULL COMMENT '本次领用的门店id',
  `inventory_id` bigint(0) NOT NULL COMMENT '本次领用的商品id',
  `count` int(0) NOT NULL COMMENT '本次领用数量',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '本次入库的备注',
  `user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '领用人用户名',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `last_update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '最近一次更新时间',
  `status` int(0) NOT NULL DEFAULT 0 COMMENT '是否删除 0否 1是',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '库存领取记录表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for item
-- ----------------------------
DROP TABLE IF EXISTS `item`;
CREATE TABLE `item`  (
  `id` bigint(0) NOT NULL COMMENT 'id',
  `image` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '项目图片',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '项目名称',
  `price` decimal(10, 2) NOT NULL COMMENT '项目价格',
  `content` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '项目描述',
  `time` int(0) NOT NULL COMMENT '项目执行所需时间 单位分钟',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `last_update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '最近一次更新时间',
  `status` int(0) NOT NULL DEFAULT 0 COMMENT '是否删除 0否 1是',
  `type` int(0) NULL DEFAULT NULL COMMENT '服务类型 1护发服务 2养发服务 3发质服务',
  `branch_id` bigint(0) NOT NULL DEFAULT 0 COMMENT '门店id',
  `stock` int(0) NULL DEFAULT 0 COMMENT '库存',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '项目表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of item
-- ----------------------------
INSERT INTO `item` VALUES (1478895949022629888, 'https://api.haoqingsi.cn/images/2022-01-06/BjAY9IJzQ5QH2O74.png', '头皮调理', 159.00, '头皮调理60分钟   159元              头皮瘙痒、油腻、发炎、皮屑、角质堆积等人士\n调理项目描述：养发师放松头部后对头部深层清洁，采用野姜精华活血，根据头皮不同问题辩证调理，辅以经络按摩，改善头部生长环境.', 60, '2022-01-06 09:07:25', '2022-01-06 09:07:44', 0, 1, 1469895024828354560, 0);
INSERT INTO `item` VALUES (1478896133756555264, 'https://api.haoqingsi.cn/images/2022-01-06/xK2HPW0kLgzavAmx.png', '发质修复', 159.00, '发质修复60分钟   159元              发质干枯、分叉等人士\n发质修复项目描述：养发师放松头部后对头部深层清洁，采用茶麸熏蒸为毛补充水分，配合毛鳞片精华，修复受损毛发.', 60, '2022-01-06 09:08:09', '2022-01-06 09:08:09', 0, 1, 1469895024828354560, 0);

-- ----------------------------
-- Table structure for leave
-- ----------------------------
DROP TABLE IF EXISTS `leave`;
CREATE TABLE `leave`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT,
  `operate_uid` bigint(0) NOT NULL COMMENT '请假操作人',
  `technician_id` bigint(0) NOT NULL COMMENT '(技师)id',
  `leave_day` int(0) NOT NULL COMMENT '请假时长，单位天',
  `start_time` datetime(0) NOT NULL COMMENT '请假开始日期',
  `end_time` datetime(0) NOT NULL COMMENT '请假结束日期',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `last_update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '最近一次更新时间',
  `status` int(0) NOT NULL DEFAULT 0 COMMENT '是否删除 0否 1是',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1413347802599264257 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '请假表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for order
-- ----------------------------
DROP TABLE IF EXISTS `order`;
CREATE TABLE `order`  (
  `id` bigint(0) NOT NULL COMMENT 'id',
  `user_id` bigint(0) NOT NULL COMMENT '订单所属用户',
  `order_status` int(0) NOT NULL DEFAULT 1 COMMENT '订单状态 1未开始 2已完成 3已取消 4已开始',
  `branch_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '门店id',
  `technician_id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '技师id',
  `item_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '项目id',
  `execute_time` datetime(0) NULL DEFAULT NULL COMMENT '服务开始时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '用户备注',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `last_update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '最近一次更新时间',
  `status` int(0) NOT NULL DEFAULT 0 COMMENT '是否删除 0否 1是',
  `item_image` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '项目图片',
  `item_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '项目名称',
  `item_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '订单价格',
  `item_content` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '项目描述',
  `item_type` int(0) NULL DEFAULT NULL COMMENT '项目所属服务类型 1护发服务 2养发服务 3发质服务',
  `item_time` int(0) NULL DEFAULT NULL COMMENT '项目执行所需时间 单位分钟',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '订单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order
-- ----------------------------
INSERT INTO `order` VALUES (1487757933684068352, 1473496766816063488, 1, '1469895024828354560', '1478920757223493632', '1478895949022629888', '2022-10-31 23:50:00', NULL, '2022-01-30 20:01:47', '2022-01-30 20:01:47', 0, 'https://api.haoqingsi.cn/images/2022-01-06/BjAY9IJzQ5QH2O74.png', '头皮调理', 159.00, '头皮调理60分钟   159元              头皮瘙痒、油腻、发炎、皮屑、角质堆积等人士\n调理项目描述：养发师放松头部后对头部深层清洁，采用野姜精华活血，根据头皮不同问题辩证调理，辅以经络按摩，改善头部生长环境.', 1, 60);
INSERT INTO `order` VALUES (1498929430779793408, 1469878292147474432, 1, '1469895024828354560', '1469878292147474432', '1478895949022629888', '2022-03-02 17:30:00', NULL, '2022-03-02 15:53:19', '2022-03-02 15:53:19', 0, 'https://api.haoqingsi.cn/images/2022-01-06/BjAY9IJzQ5QH2O74.png', '头皮调理', 159.00, '头皮调理60分钟   159元              头皮瘙痒、油腻、发炎、皮屑、角质堆积等人士\n调理项目描述：养发师放松头部后对头部深层清洁，采用野姜精华活血，根据头皮不同问题辩证调理，辅以经络按摩，改善头部生长环境.', 1, 60);

-- ----------------------------
-- Table structure for rewards_punishment
-- ----------------------------
DROP TABLE IF EXISTS `rewards_punishment`;
CREATE TABLE `rewards_punishment`  (
  `id` bigint(0) NOT NULL COMMENT 'id',
  `user_id` bigint(0) NOT NULL COMMENT '用户id',
  `type` int(0) NOT NULL COMMENT '奖惩类型 0奖励 1惩罚 2提成 3现金提成',
  `amount` decimal(10, 2) NOT NULL COMMENT '金额',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '备注',
  `operator_id` bigint(0) NULL DEFAULT NULL COMMENT '录入信息的管理员id',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `last_update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '最近一次更新时间',
  `status` int(0) NOT NULL DEFAULT 0 COMMENT '是否删除 0否 1是',
  `branch_id` bigint(0) NULL DEFAULT NULL COMMENT '门店id',
  `charge_amount` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '充值金额',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '奖惩表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sec_permission
-- ----------------------------
DROP TABLE IF EXISTS `sec_permission`;
CREATE TABLE `sec_permission`  (
  `id` bigint(0) NOT NULL COMMENT '主键',
  `icon` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图标',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称',
  `url` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型为页面时，代表前端路由地址，类型为按钮时，代表后端接口地址',
  `type` int(0) NOT NULL COMMENT '权限类型，页面-1，按钮-2',
  `permission` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限表达式',
  `method` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '后端接口访问方式',
  `sort` int(0) NOT NULL COMMENT '排序',
  `parent_id` bigint(0) NOT NULL COMMENT '父级id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sec_permission
-- ----------------------------
INSERT INTO `sec_permission` VALUES (1, '1', '1', '1', 1, '1', 'GET', 1, 0);

-- ----------------------------
-- Table structure for sec_role
-- ----------------------------
DROP TABLE IF EXISTS `sec_role`;
CREATE TABLE `sec_role`  (
  `id` bigint(0) NOT NULL COMMENT '主键',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色名',
  `description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '(申请时间)创建时间',
  `last_update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '最近一次更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sec_role
-- ----------------------------
INSERT INTO `sec_role` VALUES (1, '超级管理员', '超管', '2020-10-22 15:48:03', '2020-10-22 15:48:03');
INSERT INTO `sec_role` VALUES (2, '财务管理员', '财管', '2020-10-28 23:03:12', '2020-10-28 23:03:12');
INSERT INTO `sec_role` VALUES (3, '门店管理员', '店长', '2020-10-28 23:03:12', '2020-10-28 23:03:12');

-- ----------------------------
-- Table structure for sec_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `sec_role_permission`;
CREATE TABLE `sec_role_permission`  (
  `role_id` bigint(0) NOT NULL COMMENT '角色主键',
  `permission_id` bigint(0) NOT NULL COMMENT '权限主键',
  PRIMARY KEY (`role_id`, `permission_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色权限关系表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sec_role_permission
-- ----------------------------
INSERT INTO `sec_role_permission` VALUES (1, 1);

-- ----------------------------
-- Table structure for sec_user
-- ----------------------------
DROP TABLE IF EXISTS `sec_user`;
CREATE TABLE `sec_user`  (
  `id` bigint(0) NOT NULL COMMENT '主键',
  `branch_id` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '门店管理员管理的门店id,为空则不是门店管理员,多门的时候逗号隔开',
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户名',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '(申请时间)创建时间',
  `phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '手机',
  `password` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '密码',
  `last_update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '最近一次更新时间',
  `status` int(0) NOT NULL DEFAULT 0 COMMENT '是否删除 0否 1是',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE,
  UNIQUE INDEX `phone`(`phone`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '管理员用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sec_user
-- ----------------------------
INSERT INTO `sec_user` VALUES (1, NULL, 'mouseyCat', '2020-10-22 15:43:19', '17360096223', '$2a$10$0KImE8ISJLfHoHDchkC.WumgwcVMUFOqTRu6Och8LDCde2XCleYI2', '2021-04-19 10:55:04', 0);
INSERT INTO `sec_user` VALUES (2, NULL, 'financeAdmin', '2020-10-28 23:04:21', '13118395232', '$2a$10$7bVXNFMLgmQUinBG6614UuRGZyKCfexT4Vv4LbqlqvLCIKR4Binse', '2021-02-25 23:09:53', 0);
INSERT INTO `sec_user` VALUES (3, ' ', 'branchAdmin', '2020-10-28 22:35:06', '15984355075', '$2a$10$7bVXNFMLgmQUinBG6614UuRGZyKCfexT4Vv4LbqlqvLCIKR4Binse', '2021-03-26 14:09:48', 0);
INSERT INTO `sec_user` VALUES (4, '2', 'branchAdmin2', '2020-10-28 23:21:14', '15583654495', '$2a$10$MR9zdmVh5noIdVIIbhC2..TtdgqcYud/7SXh75SwxeoykzVbILAHq', '2020-12-01 17:58:02', 0);
INSERT INTO `sec_user` VALUES (1334357301334773760, '1334342407499681792', '123', '2020-12-03 12:42:26', '12332112321', '$2a$10$sacEy0zgQLnXzrJJ7300CeGtTVR4Nuo5KAp7JwfColke9j.bhLzsO', '2021-02-17 21:49:06', 0);
INSERT INTO `sec_user` VALUES (1362301499069304832, '2', 'lijian', '2021-02-18 15:22:42', '17360096224', '$2a$10$uycpfHqa9Mska7O15pdd5ek4mW4tdRmCqOdqHEcDycHLZNbhMDob6', '2021-04-10 19:51:30', 0);
INSERT INTO `sec_user` VALUES (1362760986729254912, NULL, 'lijian1', '2021-02-19 21:48:32', '17360096219', '$2a$10$6lzSjTYhLen7m1.6KXYi2OjSGqZMRZJPxqLqbojjZCaWttmxzrsga', '2021-02-25 14:15:52', 0);
INSERT INTO `sec_user` VALUES (1364595637752041472, '2', 'lijian2', '2021-02-24 23:18:47', '17360096225', '$2a$10$pttDEdy9ftY1YrcKxg3.sOLnXgLLIpsF43kk7.Unwg7GJp9igRR6.', '2021-03-11 22:05:30', 0);
INSERT INTO `sec_user` VALUES (1364821710934773760, '1334173120998805504', 'yugen', '2021-02-25 14:17:07', '15282614940', '$2a$10$lN/P9.rqQgHXh63bH5TbbOkn8.4s1s9yAYijFNuYVkoH6NyuKDh6S', '2021-02-25 21:02:53', 0);
INSERT INTO `sec_user` VALUES (1370013789981904896, NULL, 'lijian3', '2021-03-11 22:08:35', '17360096229', '$2a$10$3nrlQpDTdSR6WVy9vQCrPOIGCiuwh5AeWaJSq/Gp2lZiRl3uwxLju', '2021-03-11 22:08:35', 0);
INSERT INTO `sec_user` VALUES (1372548359658803200, '2', 'lijian4', '2021-03-18 22:00:04', '17360096235', '$2a$10$E2.ThXGwEO7cW4y9UvZnlu0PnQHbPSIecXfu1fRAUsv3y1F4InIwi', '2021-03-18 22:00:22', 0);

-- ----------------------------
-- Table structure for sec_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sec_user_role`;
CREATE TABLE `sec_user_role`  (
  `user_id` bigint(0) NOT NULL COMMENT '用户主键',
  `role_id` bigint(0) NOT NULL COMMENT '角色主键',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户角色关系表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sec_user_role
-- ----------------------------
INSERT INTO `sec_user_role` VALUES (1, 1);
INSERT INTO `sec_user_role` VALUES (2, 2);
INSERT INTO `sec_user_role` VALUES (3, 2);
INSERT INTO `sec_user_role` VALUES (4, 3);
INSERT INTO `sec_user_role` VALUES (1333964891928793088, 2);
INSERT INTO `sec_user_role` VALUES (1334145705509523456, 3);
INSERT INTO `sec_user_role` VALUES (1334357301334773760, 1);
INSERT INTO `sec_user_role` VALUES (1340994748105756672, 1);
INSERT INTO `sec_user_role` VALUES (1362300931265400832, 3);
INSERT INTO `sec_user_role` VALUES (1362301499069304832, 3);
INSERT INTO `sec_user_role` VALUES (1362760986729254912, 1);
INSERT INTO `sec_user_role` VALUES (1364595637752041472, 2);
INSERT INTO `sec_user_role` VALUES (1364821710934773760, 3);
INSERT INTO `sec_user_role` VALUES (1370013594867077120, 2);
INSERT INTO `sec_user_role` VALUES (1370013789981904896, 2);
INSERT INTO `sec_user_role` VALUES (1372548359658803200, 2);

-- ----------------------------
-- Table structure for service_type
-- ----------------------------
DROP TABLE IF EXISTS `service_type`;
CREATE TABLE `service_type`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `service_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `imgae_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `experience` tinyint(1) NOT NULL DEFAULT 0,
  `key` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of service_type
-- ----------------------------
INSERT INTO `service_type` VALUES (1, '初级调理', 'https://api.haoqingsi.cn/images/2021-12-12/nGVOYhMFmpX6a1ED.png', 0, 'hf_count');
INSERT INTO `service_type` VALUES (2, '中级调理127', 'https://api.haoqingsi.cn/images/2021-12-12/2jAFT6J0lXJxyBOP.png', 0, 'yf_count');
INSERT INTO `service_type` VALUES (3, '高级调理', 'https://api.haoqingsi.cn/images/2021-08-02/DOWDeNUwDm1hf6Z9.png', 0, 'fz_count');
INSERT INTO `service_type` VALUES (4, '顶级调理', 'https://api.haoqingsi.cn/images/2021-08-02/BvsTLFyxZgZ6fVXb.png', 0, 'dj_count');
INSERT INTO `service_type` VALUES (5, '头肩调理', 'https://api.haoqingsi.cn/images/2021-08-02/4sy4eONlVxaA3fNE.png', 0, 'tb_count');
INSERT INTO `service_type` VALUES (6, '其他调理', 'https://api.haoqingsi.cn/images/2021-08-02/6Jc2hiT7HdgWiJan.png', 1, 'yh_count');

-- ----------------------------
-- Table structure for service_type_charge
-- ----------------------------
DROP TABLE IF EXISTS `service_type_charge`;
CREATE TABLE `service_type_charge`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `sec_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '充值人',
  `service_type_id` int(0) NOT NULL,
  `user_id` bigint(0) NOT NULL COMMENT '被充值人',
  `charge_count` int(0) NOT NULL DEFAULT 0,
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '(申请时间)创建时间',
  `last_update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '最近一次更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `id` bigint(0) NOT NULL COMMENT 'id',
  `order_auto_confirm_time` bigint(0) NOT NULL DEFAULT 10 COMMENT '订单自动确认时间',
  `config_first` int(0) NULL DEFAULT 2 COMMENT '预约配置1',
  `config_second` int(0) NULL DEFAULT 4 COMMENT '预约配置2',
  `config_third` int(0) NULL DEFAULT 6 COMMENT '预约配置3',
  `config_fourth` int(0) NULL DEFAULT 8 COMMENT '预约配置4',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `last_update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '最近一次更新时间',
  `status` int(0) NOT NULL DEFAULT 0 COMMENT '是否删除 0否 1是',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '系统配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, 10, 1, 2, 3, 4, '2020-11-08 14:13:34', '2021-03-06 15:04:34', 0);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` bigint(0) NOT NULL COMMENT 'id',
  `phone_number` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '手机号',
  `nick_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '用户昵称',
  `head_photo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '用户头像',
  `birth` date NULL DEFAULT NULL COMMENT '用户生日',
  `hf_count` int(0) NOT NULL DEFAULT 0 COMMENT '剩余护发服务次数  初级调理',
  `yf_count` int(0) NOT NULL DEFAULT 0 COMMENT '剩余养发服务次数  中级调理',
  `fz_count` int(0) NOT NULL DEFAULT 0 COMMENT '剩余发质服务次数  高级调理',
  `resume` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '个人简介',
  `sex` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '性别',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '地区',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `last_update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '最近一次更新时间',
  `update_by` varchar(0) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `experience` tinyint(1) NOT NULL DEFAULT 0 COMMENT '体验 0否 1是',
  `member` tinyint(1) NOT NULL DEFAULT 0 COMMENT '会员 0否 1是',
  `status` int(0) NOT NULL DEFAULT 0 COMMENT '是否删除 0否 1是',
  `branch_id` bigint(0) NULL DEFAULT NULL COMMENT '门店id(技师所属门店)',
  `sort` int(0) NULL DEFAULT 1 COMMENT '挂店排序，默认 1 本店员工逻辑配置为0',
  `put_branch_id` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '所挂店id，逗号隔开',
  `open_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `hair_status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '头发状况',
  `point` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '积分，与消费金额的比例为 1:1',
  `remark` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '用户备注',
  `on_work` int(0) NULL DEFAULT 0 COMMENT '0-无状态 1-上班 2-下班',
  `label_type` bigint(0) NULL DEFAULT 0 COMMENT 'calendar周期配置id',
  `call_time` datetime(0) NULL DEFAULT NULL COMMENT '打电话时间',
  `dj_count` int(0) NULL DEFAULT 0 COMMENT '顶级调理',
  `tb_count` int(0) NULL DEFAULT 0 COMMENT '头部调理',
  `yh_count` int(0) NULL DEFAULT 0 COMMENT '优惠调理',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '用户表，技师与用户在同一张表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1498959881036763136, NULL, '.', 'https://thirdwx.qlogo.cn/mmopen/vi_32/C80sH4h11RsqC7QN7ra5lCuAiadlhyHVqlmXJOE26gX9Ne1ejKPncwCQ1WBEuOd9ZlQ7wnZEbnVibWffEAGvX07g/132', NULL, 0, 0, 0, NULL, '女', NULL, '2022-04-11 14:37:46', '2022-04-11 14:37:46', NULL, 0, 0, 0, NULL, 1, NULL, 'ofAl-5fg-Vc1XylE3_hIYfH43heM', NULL, 0.00, NULL, 0, 0, NULL, 0, 0, 0);

-- ----------------------------
-- Table structure for user_login_account
-- ----------------------------
DROP TABLE IF EXISTS `user_login_account`;
CREATE TABLE `user_login_account`  (
  `user_id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT 'user id',
  `wx_account_type` int(0) NULL DEFAULT NULL COMMENT '帐号类型. 0, 未知; 1, 微信小程序openId 2, 微信unionId; 3, 微信公众号openId;',
  `wx_account` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '账号',
  `session_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '小程序sessionKey',
  `sex` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '性别',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '地区',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `last_update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '最近一次更新时间',
  `status` int(0) NOT NULL DEFAULT 0 COMMENT '是否删除 0否 1是',
  INDEX `user_id`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1513370832120975361 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '用户登陆表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_login_account
-- ----------------------------
INSERT INTO `user_login_account` VALUES (1498959881036763136, 2, 'ofAl-5fg-Vc1XylE3_hIYfH43heM', 'f/qsasRbMJYy7R2x2B6DzA==', NULL, NULL, '2022-03-02 17:54:19', '2022-04-11 14:37:46', 0);
INSERT INTO `user_login_account` VALUES (1498959992072572928, 2, 'ofAl-5d6vO0I0IQg59DZYMit3xt4', '7xfGRPxMj+jEVGIck6Ur6A==', NULL, NULL, '2022-03-02 17:54:46', '2022-03-02 18:24:28', 0);
INSERT INTO `user_login_account` VALUES (1498962706290577408, 2, 'ofAl-5Un79Zk1-do5zMlBLzOPGKk', 'zzHfg73nKAaDxp6NcbphYg==', NULL, NULL, '2022-03-02 18:05:33', '2022-03-02 18:05:33', 0);
INSERT INTO `user_login_account` VALUES (1498969403964919808, 2, 'ofAl-5Rukc3wAIv4qjVj3oJaMW_M', 'r/lTJuS6aT/0z+Ogjpxwew==', NULL, NULL, '2022-03-02 18:32:10', '2022-03-02 18:32:10', 0);
INSERT INTO `user_login_account` VALUES (1499253299558354944, 2, 'ofAl-5awnTfiTVs6TR_QJH2W3VPU', 'XQ+4Y0fvpMlN9Sw3gs/Psw==', NULL, NULL, '2022-03-03 13:20:16', '2022-03-03 13:20:16', 0);
INSERT INTO `user_login_account` VALUES (1499740917022724096, 2, 'ofAl-5c_WwxLRWW6Tfip12s_6ZeQ', 'b0oLfujeJQDmDViPu+L33Q==', NULL, NULL, '2022-03-04 21:37:53', '2022-03-05 12:23:33', 0);
INSERT INTO `user_login_account` VALUES (1499741815899820032, 2, 'ofAl-5SaKbnrM5O2GJHDeITn6aWk', 'UAAPPCd8UTTYzEe2LhEMXw==', NULL, NULL, '2022-03-04 21:41:27', '2022-03-04 21:41:27', 0);
INSERT INTO `user_login_account` VALUES (1499743970677362688, 2, 'ofAl-5WBmgZWXPMqIbMBmswhiMIg', 'FM2kmKro3332UiEhj2Tpzw==', NULL, NULL, '2022-03-04 21:50:01', '2022-03-04 21:50:02', 0);
INSERT INTO `user_login_account` VALUES (1513370570660646912, 2, 'ofAl-5aM3oH_xs20_pea9WbLIVBs', 'kIWNizxo+6ccEqEFlJT13g==', NULL, NULL, '2022-04-11 12:17:15', '2022-04-11 12:17:15', 0);
INSERT INTO `user_login_account` VALUES (1513370832120975360, 2, 'ofAl-5WoeCyey6Igjiaa7MTXIuNY', 'CzcLqP5+u848+pjGnfK8RQ==', NULL, NULL, '2022-04-11 12:18:18', '2022-04-11 14:20:36', 0);

-- ----------------------------
-- View structure for chart_view_branch_consume
-- ----------------------------
DROP VIEW IF EXISTS `chart_view_branch_consume`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `chart_view_branch_consume` AS select `o`.`id` AS `orderId`,`o`.`branch_id` AS `branchId`,`b`.`title` AS `branchName`,`o`.`item_price` AS `price`,date_format(`o`.`create_time`,'%Y-%m-%d') AS `dataByTime` from (`order` `o` join `branch` `b` on((`o`.`branch_id` = `b`.`id`))) where ((`o`.`status` = 0) and (`o`.`order_status` = 2));

-- ----------------------------
-- View structure for chart_view_branch_id_contrast
-- ----------------------------
DROP VIEW IF EXISTS `chart_view_branch_id_contrast`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `chart_view_branch_id_contrast` AS select `b`.`id` AS `branchId`,`b`.`title` AS `branchName`,date_format(`rp`.`create_time`,'%Y-%m-%d') AS `dataByTime`,ifnull(`rp`.`charge_amount`,0.00) AS `dateByMoney`,1 AS `type` from (`branch` `b` left join `rewards_punishment` `rp` on(((`rp`.`branch_id` = `b`.`id`) and (`rp`.`status` = 0)))) union all select `b`.`id` AS `branchId`,`b`.`title` AS `branchName`,date_format(`rp`.`create_time`,'%Y-%m-%d') AS `dataByTime`,ifnull(`rp`.`amount`,0.00) AS `dateByMoney`,2 AS `type` from (`branch` `b` left join `rewards_punishment` `rp` on(((`b`.`id` = `rp`.`branch_id`) and (`rp`.`status` = 0) and (`rp`.`type` <> 1)))) union all select `b`.`id` AS `branchId`,`b`.`title` AS `branchName`,date_format(`ipr`.`create_time`,'%Y-%m-%d') AS `dataByTime`,ifnull(`ipr`.`total_money`,0.00) AS `dateByMoney`,2 AS `type` from (`branch` `b` left join `inventory_put_record` `ipr` on(((`b`.`id` = `ipr`.`branch_id`) and (`ipr`.`status` = 0))));

-- ----------------------------
-- View structure for chart_view_branch_item_consume
-- ----------------------------
DROP VIEW IF EXISTS `chart_view_branch_item_consume`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `chart_view_branch_item_consume` AS select `i`.`id` AS `itemId`,`o`.`id` AS `orderId`,`o`.`branch_id` AS `branchId`,`i`.`title` AS `itemName`,`o`.`item_price` AS `price`,date_format(`o`.`create_time`,'%Y-%m-%d') AS `dataByTime` from (`order` `o` join `item` `i` on((`o`.`item_id` = `i`.`id`))) where ((`o`.`status` = 0) and (`o`.`order_status` = 2));

-- ----------------------------
-- View structure for chart_view_branch_item_flow
-- ----------------------------
DROP VIEW IF EXISTS `chart_view_branch_item_flow`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `chart_view_branch_item_flow` AS select `o`.`id` AS `orderId`,`o`.`branch_id` AS `branchId`,`o`.`item_id` AS `itemId`,`i`.`title` AS `itemName`,date_format(`o`.`create_time`,'%Y-%m-%d') AS `dataByTime` from (`order` `o` join `item` `i` on((`o`.`item_id` = `i`.`id`))) where ((`o`.`status` = 0) and (`o`.`order_status` = 2));

-- ----------------------------
-- View structure for view_branch_consume
-- ----------------------------
DROP VIEW IF EXISTS `view_branch_consume`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `view_branch_consume` AS select `b`.`id` AS `branchId`,`b`.`title` AS `branchName`,sum((case when (to_days(`o`.`create_time`) = to_days(now())) then `o`.`item_price` else 0 end)) AS `dayMoney`,sum((case when (date_format(`o`.`create_time`,'%Y%m') = date_format(curdate(),'%Y%m')) then `o`.`item_price` else 0 end)) AS `monthMoney`,sum((case when (quarter(`o`.`create_time`) = quarter(now())) then `o`.`item_price` else 0 end)) AS `quarterMoney`,sum((case when (year(`o`.`create_time`) = year(now())) then `o`.`item_price` else 0 end)) AS `yearMoney` from (`branch` `b` left join `order` `o` on(((`b`.`id` = `o`.`branch_id`) and (`o`.`status` = 0)))) group by `b`.`id` order by `yearMoney` desc;

-- ----------------------------
-- View structure for view_branch_disburse
-- ----------------------------
DROP VIEW IF EXISTS `view_branch_disburse`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `view_branch_disburse` AS select `v1`.`branchId` AS `branchId`,`v1`.`branchName` AS `branchName`,(`v1`.`dayMoney` + `v2`.`dayMoney`) AS `dayMoney`,(`v1`.`monthMoney` + `v2`.`monthMoney`) AS `monthMoney`,(`v1`.`quarterMoney` + `v2`.`quarterMoney`) AS `quarterMoney`,(`v1`.`yearMoney` + `v2`.`yearMoney`) AS `yearMoney` from (`view_branch_inventory_disburse` `v1` join `view_branch_push_disburse` `v2` on((`v1`.`branchId` = `v2`.`branchId`))) group by `v1`.`branchId` order by `yearMoney`;

-- ----------------------------
-- View structure for view_branch_income
-- ----------------------------
DROP VIEW IF EXISTS `view_branch_income`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `view_branch_income` AS select `b`.`id` AS `branchId`,`b`.`title` AS `branchName`,sum((case when (to_days(`rp`.`create_time`) = to_days(now())) then `rp`.`charge_amount` else 0 end)) AS `dayMoney`,sum((case when (date_format(`rp`.`create_time`,'%Y%m') = date_format(curdate(),'%Y%m')) then `rp`.`charge_amount` else 0 end)) AS `monthMoney`,sum((case when (quarter(`rp`.`create_time`) = quarter(now())) then `rp`.`charge_amount` else 0 end)) AS `quarterMoney`,sum((case when (year(`rp`.`create_time`) = year(now())) then `rp`.`charge_amount` else 0 end)) AS `yearMoney` from (`branch` `b` left join `rewards_punishment` `rp` on(((`rp`.`branch_id` = `b`.`id`) and (`rp`.`status` = 0)))) group by `b`.`id` order by `yearMoney` desc;

-- ----------------------------
-- View structure for view_branch_inventory_disburse
-- ----------------------------
DROP VIEW IF EXISTS `view_branch_inventory_disburse`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `view_branch_inventory_disburse` AS select `b`.`id` AS `branchId`,`b`.`title` AS `branchName`,ifnull(sum((case when (to_days(`ipr`.`create_time`) = to_days(now())) then `ipr`.`total_money` else 0 end)),0.00) AS `dayMoney`,ifnull(sum((case when (date_format(`ipr`.`create_time`,'%Y%m') = date_format(curdate(),'%Y%m')) then `ipr`.`total_money` else 0 end)),0.00) AS `monthMoney`,ifnull(sum((case when (quarter(`ipr`.`create_time`) = quarter(now())) then `ipr`.`total_money` else 0 end)),0.00) AS `quarterMoney`,ifnull(sum((case when (year(`ipr`.`create_time`) = year(now())) then `ipr`.`total_money` else 0 end)),0.00) AS `yearMoney` from (`branch` `b` left join `inventory_put_record` `ipr` on(((`b`.`id` = `ipr`.`branch_id`) and (`ipr`.`status` = 0)))) group by `b`.`id` order by `yearMoney` desc;

-- ----------------------------
-- View structure for view_branch_item_consume
-- ----------------------------
DROP VIEW IF EXISTS `view_branch_item_consume`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `view_branch_item_consume` AS select `i`.`id` AS `itemId`,`b`.`id` AS `branchId`,`b`.`title` AS `branchName`,`i`.`title` AS `itemName`,sum((case when (to_days(`o`.`create_time`) = to_days(now())) then `o`.`item_price` else 0 end)) AS `dayMoney`,sum((case when (date_format(`o`.`create_time`,'%Y%m') = date_format(curdate(),'%Y%m')) then `o`.`item_price` else 0 end)) AS `monthMoney`,sum((case when (quarter(`o`.`create_time`) = quarter(now())) then `o`.`item_price` else 0 end)) AS `quarterMoney`,sum((case when (year(`o`.`create_time`) = year(now())) then `o`.`item_price` else 0 end)) AS `yearMoney` from ((`item` `i` left join `order` `o` on(((`i`.`id` = `o`.`item_id`) and (`o`.`status` = 0)))) left join `branch` `b` on((`b`.`id` = `i`.`branch_id`))) group by `i`.`id` order by `yearMoney` desc;

-- ----------------------------
-- View structure for view_branch_push_disburse
-- ----------------------------
DROP VIEW IF EXISTS `view_branch_push_disburse`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `view_branch_push_disburse` AS select `b`.`id` AS `branchId`,`b`.`title` AS `branchName`,sum((case when (to_days(`rp`.`create_time`) = to_days(now())) then (case when ((0 <> `rp`.`amount`) and (`rp`.`type` <> 1)) then `rp`.`amount` else 0 end) else 0 end)) AS `dayMoney`,sum((case when (date_format(`rp`.`create_time`,'%Y%m') = date_format(curdate(),'%Y%m')) then (case when ((0 <> `rp`.`amount`) and (`rp`.`type` <> 1)) then `rp`.`amount` else 0 end) else 0 end)) AS `monthMoney`,sum((case when (quarter(`rp`.`create_time`) = quarter(now())) then (case when ((0 <> `rp`.`amount`) and (`rp`.`type` <> 1)) then `rp`.`amount` else 0 end) else 0 end)) AS `quarterMoney`,sum((case when (year(`rp`.`create_time`) = year(now())) then (case when ((0 <> `rp`.`amount`) and (`rp`.`type` <> 1)) then `rp`.`amount` else 0 end) else 0 end)) AS `yearMoney` from (`branch` `b` left join `rewards_punishment` `rp` on(((`b`.`id` = `rp`.`branch_id`) and (`rp`.`status` = 0)))) group by `b`.`id` order by `yearMoney` desc;

-- ----------------------------
-- View structure for view_branch_salary
-- ----------------------------
DROP VIEW IF EXISTS `view_branch_salary`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `view_branch_salary` AS select `u`.`nick_name` AS `nickName`,`b`.`title` AS `branchName`,`b`.`id` AS `branchId`,sum((case when ((0 <> `rp`.`amount`) and (`rp`.`type` = 0)) then `rp`.`amount` else 0 end)) AS `awardMoney`,sum((case when ((0 <> `rp`.`amount`) and (`rp`.`type` = 1)) then `rp`.`amount` else 0 end)) AS `punishmentMoney`,sum((case when ((0 <> `rp`.`amount`) and (`rp`.`type` = 2)) then `rp`.`amount` else 0 end)) AS `pushMoney`,sum((case when ((0 <> `rp`.`amount`) and (`rp`.`type` = 3)) then `rp`.`amount` else 0 end)) AS `cashPushMoney`,(((sum((case when ((0 <> `rp`.`amount`) and (`rp`.`type` = 0)) then `rp`.`amount` else 0 end)) + sum((case when ((0 <> `rp`.`amount`) and (`rp`.`type` = 2)) then `rp`.`amount` else 0 end))) + sum((case when ((0 <> `rp`.`amount`) and (`rp`.`type` = 3)) then `rp`.`amount` else 0 end))) - sum((case when ((0 <> `rp`.`amount`) and (`rp`.`type` = 1)) then `rp`.`amount` else 0 end))) AS `totalMoney` from ((`user` `u` join `branch` `b` on((`u`.`branch_id` = `b`.`id`))) left join `rewards_punishment` `rp` on((`u`.`id` = `rp`.`user_id`))) where (`u`.`status` = 0) group by `u`.`id`;

-- ----------------------------
-- View structure for view_branch_user_consume
-- ----------------------------
DROP VIEW IF EXISTS `view_branch_user_consume`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `view_branch_user_consume` AS select `u`.`id` AS `technicianId`,`u`.`nick_name` AS `nickName`,`b`.`id` AS `branchId`,sum((case when (to_days(`o`.`create_time`) = to_days(now())) then `o`.`item_price` else 0 end)) AS `dayMoney`,sum((case when (date_format(`o`.`create_time`,'%Y%m') = date_format(curdate(),'%Y%m')) then `o`.`item_price` else 0 end)) AS `monthMoney`,sum((case when (quarter(`o`.`create_time`) = quarter(now())) then `o`.`item_price` else 0 end)) AS `quarterMoney`,sum((case when (year(`o`.`create_time`) = year(now())) then `o`.`item_price` else 0 end)) AS `yearMoney` from ((`user` `u` left join `order` `o` on(((`u`.`id` = `o`.`technician_id`) and (`o`.`status` = 0)))) left join `branch` `b` on((`b`.`id` = `u`.`branch_id`))) where (`u`.`branch_id` is not null) group by `u`.`id` order by `yearMoney` desc;

SET FOREIGN_KEY_CHECKS = 1;
