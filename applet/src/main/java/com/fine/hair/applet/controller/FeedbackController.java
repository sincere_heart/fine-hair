package com.fine.hair.applet.controller;

import cn.hutool.core.lang.Snowflake;
import com.fine.hair.applet.mapper.FeedbackMapper;
import com.fine.hair.comm.dto.AddFeedbackDto;
import com.fine.hair.comm.utils.ApiResponse;
import com.fine.hair.comm.model.Feedback;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>用户反馈控制层</p>
 *
 * @author mouseyCat
 * @date 2020/11/18 14:43
 */
@Api(tags = {"用户反馈相关接口"})
@RestController
@RequestMapping("/feedback/")
public class FeedbackController {

    @Resource
    private FeedbackMapper feedbackMapper;
    @Autowired
    private Snowflake snowflake;

    @PostMapping("add-feedback")
    @ApiOperation("新增一条反馈信息")
    public ApiResponse addFeedback(@RequestBody AddFeedbackDto dto) {
        Feedback feedback = Feedback.builder().id(snowflake.nextId()).description(dto.getDescription()).userId(dto.getUserId()).build();
        feedbackMapper.insert(feedback);
        return ApiResponse.ofSuccess();
    }
}
