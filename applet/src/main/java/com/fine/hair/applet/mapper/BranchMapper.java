package com.fine.hair.applet.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fine.hair.comm.dto.GdxhtjDTO;
import com.fine.hair.comm.model.Branch;
import com.fine.hair.comm.utils.PageInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>${Description}</p>
 *
 * @author mouseyCat
 * @date 2020/10/26 21:39
 */
@Mapper
public interface BranchMapper extends BaseMapper<Branch> {

    /**
     * @param areaOrServiceOrBranchName
     * @return
     */
    List<Branch> selectByKey(@Param("areaOrServiceOrBranchName") String areaOrServiceOrBranchName);

    /**
     * @param branchName
     * @return
     */
    List<Branch> searchBranchByBranchName(@Param("branchName") String branchName);

    Branch selectByBranchName(@Param("branchName") String branchName, @Param("branchId") String branchId);

    Branch selectRandomOne();
}
