package com.fine.hair.applet.controller;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.fine.hair.applet.service.UserService;
import com.fine.hair.applet.wx.WxService;
import com.fine.hair.comm.dto.WxRegisterDto;
import com.fine.hair.comm.dto.wx.WxLoginDto;
import com.fine.hair.comm.pojo.wx.WxLiteUserPhoneNumber;
import com.fine.hair.comm.utils.ApiResponse;
import com.fine.hair.comm.utils.Consts;
import com.fine.hair.comm.vo.UserTypeVo;
import com.fine.hair.comm.vo.WxLiteUserInfo;
import com.fine.hair.comm.vo.WxLoginResultVo;
import com.fine.hair.comm.model.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * 登陆接口
 *
 * @author junelee
 * @date 2020/8/7 14:37
 */
@CrossOrigin
@RestController
@Slf4j
@RequestMapping("/login/")
@Api(tags = "登陆接口")
public class LoginController {

    @Autowired
    private WxService wxService;

    @Autowired
    private UserService userService;

    @ApiOperation(value = "获取用户类型", notes = "每个用户登陆后必调该接口，该接口决定登陆者是用户还是员工")
    @GetMapping("user-type/{userId}")
    public ApiResponse<UserTypeVo> userType(@PathVariable Long userId) {
        User user = userService.getOne(new LambdaQueryWrapper<User>().eq(User::getBranchId, userId));
        UserTypeVo vo = new UserTypeVo();
        vo.setIsStaff(user.getBranchId() != null);
        vo.setBranchId(user.getBranchId());
        return ApiResponse.ofSuccess(vo);
    }


    @ApiOperation("获取openId")
    @GetMapping("beforeWxLogin/{code}")
    public ApiResponse<String> beforeWxLogin(@PathVariable("code") String code) {
        return userService.getOpenId(code);
    }


    @ApiOperation(value = "微信登录", notes = "用户如果之前登录过且绑定过微信,后端会返回用户信息如uid, token等<br />如果微信用户从未登录过,后端仅返回status=3010, " +
            "微信unionId, unionId用于登录后绑定微信<br />如果请求微信服务器端失败, 则不返回unionId")
    @PostMapping(value = "wxLoginA")
    public ApiResponse<WxLoginResultVo> wxLoginPvA(@RequestBody WxLoginDto param) {
        log.info("param = {}", param);
        WxLiteUserInfo wxLiteUserInfo = this.wxService.decodeWxLiteUserInfo(param);
        WxLoginResultVo result = userService.wxLogin(wxLiteUserInfo,param);
        log.info(">>>>>>>>>>>>> 微信登陆返回的参数 = {}", result);
        if (result.getId() == null) {
            return ApiResponse.of(Consts.RES_CODE_WX_LOGIN_FAIL, "微信登录失败,请注册", result);
        }

        return ApiResponse.ofSuccess(result);
    }

    @ApiOperation(value = "微信注册/登录", notes = "使用微信小程序端获取到的加密后的手机号登录. 如果该手机号已绑定过别的微信账号, 后端返回status = 2000")
    @PostMapping(value = "wxLoginB")
    public ApiResponse<WxLoginResultVo> wxLoginPvB(@RequestBody WxRegisterDto param) {
        log.info(">>>>>>>>>>>>>> wxLoginB请求参数 = {}", param);
        WxLiteUserPhoneNumber wxLiteUserInfo = this.wxService.decodeWxLitePhoneNumber(param);

        WxLoginResultVo result = userService.wxLiteRegister(
                wxLiteUserInfo.getPurePhoneNumber(), param.getOpenId(),
                param.getUnionId(), wxLiteUserInfo.getNickName(),
                wxLiteUserInfo.getAvatarUrl(), wxLiteUserInfo.getGender());

        log.info(" userLoginInfo = {}", result);
        return ApiResponse.ofSuccess(result);
    }

    @ApiOperation("是否员工,true是 false否")
    @GetMapping("is-staff/{userId}")
    public ApiResponse isStaff(@PathVariable Long userId) {
        User user = userService.getById(userId);
        if (ObjectUtil.isNull(user)) {
            return ApiResponse.of(0,"该用户不存在",null);
        }
        if (user.getBranchId() == null) {
            return ApiResponse.ofSuccess(false);
        }
        return ApiResponse.ofSuccess(true);
    }

}
