package com.fine.hair.applet.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fine.hair.comm.model.Evaluate;
import com.fine.hair.comm.pojo.EvaluateList;
import com.fine.hair.comm.utils.PageInfo;
import com.fine.hair.comm.vo.FeedbackListVo;
import com.fine.hair.comm.vo.StaffEvaluateVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>${Description}</p>
 *
 * @author mouseyCat
 * @date 2020/10/26 21:39
 */
@Mapper
public interface EvaluateMapper extends BaseMapper<Evaluate> {
    /**
     * @param userId
     * @return
     */
    StaffEvaluateVo staffEvaluate(Long userId);

    /**
     * @param userId
     * @return
     */
    List<EvaluateList> selectEvaluateList(@Param("userId") Long userId, @Param("pageInfo") PageInfo<EvaluateList> pageInfo);

    /**
     *
     * @param pageInfo
     * @return
     */
    List<FeedbackListVo> selectFeedbackList(PageInfo<FeedbackListVo> pageInfo);
}
