package com.fine.hair.applet.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fine.hair.comm.dto.UserListDTO;
import com.fine.hair.comm.model.User;
import com.fine.hair.comm.pojo.StaffSimpleInfo;
import com.fine.hair.comm.utils.PageInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * <p>${Description}</p>
 *
 * @author mouseyCat
 * @date 2020/10/26 21:39
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {
    /**
     * @param newUser
     * @return
     */
    int insertOrUpdate(@Param("user") User newUser);

    /**
     * @param phone
     * @return
     */
    User selectUserByPhoneNumber(@Param("phone") String phone);

    /**
     * @param branchId
     * @param pageInfo
     * @return
     */
    List<StaffSimpleInfo> selectByBranchIdAndPageInfo(@Param("branchId") Long branchId,
                                                      @Param("pageInfo") PageInfo<StaffSimpleInfo> pageInfo);

    /**
     * @param branchId
     * @return
     */
    List<StaffSimpleInfo> selectByBranchId(@Param("branchId") Long branchId);

    /**
     * @param technicianIds
     * @return
     */
    List<StaffSimpleInfo> selectByTechnicianId(@Param("pageInfo") PageInfo<StaffSimpleInfo> pageInfo, @Param(
            "technicianId") List<String> technicianIds);

    /**
     * @param userPage
     * @param dto
     * @return
     */
    List<User> selectUserListAll(@Param("userPage") PageInfo<User> userPage, @Param("dto") UserListDTO dto);

}
