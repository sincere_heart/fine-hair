package com.fine.hair.applet.controller;


import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fine.hair.applet.mapper.ServiceTypeMapper;
import com.fine.hair.comm.model.ServiceType;
import com.fine.hair.comm.utils.ApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author liheng
 * @since 2021-07-29
 */
@Api(tags = {"服务类型相关接口"})
@RestController
@RequestMapping("/service-type/")
public class ServiceTypeController {
    @Autowired
    private ServiceTypeMapper serviceTypeMapper;

    @ApiOperation("获取服务类型")
    @GetMapping("dist")
    public ApiResponse<ServiceType> list() {
        return ApiResponse.ofSuccess(serviceTypeMapper.selectList(Wrappers.emptyWrapper()));
    }
}

