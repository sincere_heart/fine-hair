package com.fine.hair.applet.aop;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author junelee
 * @date 2020/5/28 14:18
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface UserApi {
    boolean login() default true;
}
