package com.fine.hair.applet.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fine.hair.comm.dto.*;
import com.fine.hair.comm.model.Order;
import com.fine.hair.comm.utils.PageInfo;
import com.fine.hair.comm.vo.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>${Description}</p>
 *
 * @author mouseyCat
 * @date 2020/10/26 21:39
 */
@Mapper
public interface OrderMapper extends BaseMapper<Order> {
    /**
     * 预约信息
     *
     * @param orderId 订单id
     * @return 预约信息
     */
    OrderInfoVo selectOrderInfoByOrderId(@Param("orderId") Long orderId);

    /**
     * @param dto
     * @param pageInfo
     * @return
     */
    List<Order> selectStaffOrderInfoByCondition(@Param("dto") StaffOrderInfoDto dto,
                                                @Param("pageInfo") PageInfo<Order> pageInfo);

    /**
     * @param date
     * @return
     */
    List<Order> selectOrderByDate(@Param("date") String date, @Param("dto") DateDto dto);


    /**
     * @param pageInfo
     * @param staffId
     * @return
     */
    List<Order> selectStaffOrder(@Param("pageInfo") PageInfo<StaffOrderVo> pageInfo, @Param("staffId") String staffId
            , @Param("orderStatus") Integer orderStatus, @Param("branchTitle") String branchTitle);

    /**
     * @param userId
     * @return
     */
    Integer selectCompletedOrderCount(@Param("userId") String userId);

    /**
     * @param pageInfo
     * @param mode
     * @return
     */
    List<StaffSearchClientOrderVo> staffSearchClientOrder(@Param("pageInfo") PageInfo<StaffSearchClientOrderVo> pageInfo, @Param("mode") Integer mode, @Param("staffId") String staffId);

    /**
     * @param orderId
     * @return
     */
    ClickRemarkVo clickRemark(@Param("orderId") Long orderId);


    /**
     * @param orderId
     * @return
     */
    DetailRemarkVo detailRemark(@Param("orderId") Long orderId);

    /**
     * @param pageInfo
     * @param dto
     * @return
     */
    List<Order> selectByPage(@Param("pageInfo") PageInfo<Order> pageInfo, @Param("dto") OrderListDto dto);

    /**
     * @param dto
     * @return
     */
    List<Order> selectOrderByMonth(@Param("dto") SearchCalendarDto dto);


    List<Order> selectListByUserIdAndDate(@Param("orderStatus") Integer orderStatus, @Param("date") String date,
                                          @Param("userId") String userId);

    List<Order> selectListByUserIdAndDate3(@Param("orderStatus") Integer orderStatus, @Param("date") String date,
                                           @Param("userId") String userId,
                                           @Param("pageInfo") PageInfo<StaffOrderVo> pageInfo);

    List<Order> selectUserOrderByMonth(@Param("dto") SearchCalendarDto dto);


    List<Order> selectYesterday(Long userId);

    List<Order> selectThisMonth(Long userId);

    List<Order> selectThisDay(Long userId);

    List<Order> selectThisYear(Long userId);

    List<Order> selectQuarterOne(Long userId);

    List<Order> selectQuarterTwo(Long userId);

    List<Order> selectQuarterThree(Long userId);

    List<Order> selectQuarterFour(Long userId);

    List<Order> selectListByUserIdAndDate2(@Param("date") String date, @Param("userId") String userId);


    List<Order> selectUserOrder(@Param("pageInfo") PageInfo<StaffOrderVo> pageInfo, @Param("userId") String userId,
                                @Param("orderStatus") Integer orderStatus);

    List<Order> selectUserOrderCount(@Param("id") String id, @Param("date") String date);

    List<Order> selectUserOrderByMonth2(@Param("dto") SearchCalendarDto dto);


    Order selectBusyCountByOrderTime(@Param("orderTime") Date orderTime, @Param("technicianId") String technicianId);

}
