package com.fine.hair.applet;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


/**
 * @author mouseyCat
 * @date 2020-08-07 11:20
 * SpringBootApplication 扫描包，因全局异常不生效，com.fine.hair
 * 解决 全局异常注解扫描问题 具体看 https://www.cnblogs.com/baojun/p/10750549.html
 */
@SpringBootApplication(scanBasePackages = {"com.fine.hair"})
@MapperScan(basePackages = {"com.fine.hair.applet.mapper"})
@EnableTransactionManagement
@EnableSwagger2
public class Appletlication {

    public static void main(String[] args) {
        SpringApplication.run(Appletlication.class, args);
    }

}
