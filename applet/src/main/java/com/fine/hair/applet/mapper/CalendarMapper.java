package com.fine.hair.applet.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fine.hair.comm.model.Calendar;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>${Description}</p>
 * @author mouseyCat
 * @date 2021/1/28 19:22
 */
@Mapper
public interface CalendarMapper extends BaseMapper<Calendar> {
}
