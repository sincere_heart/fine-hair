package com.fine.hair.applet.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fine.hair.comm.dto.DateDto;
import com.fine.hair.comm.dto.StaffRewardsPunishmentInfoDto;
import com.fine.hair.comm.model.RewardsPunishment;
import com.fine.hair.comm.utils.PageInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>${Description}</p>
 *
 * @author mouseyCat
 * @date 2020/10/26 21:39
 */
@Mapper
public interface RewardsPunishmentMapper extends BaseMapper<RewardsPunishment> {

    /**
     * @param date
     * @return
     */
    List<RewardsPunishment> selectByDate(@Param("date") String date, @Param("dto")DateDto dto);

    List<RewardsPunishment> selectYesterday(Long userId);

    List<RewardsPunishment> selectThisDay(Long userId);

    List<RewardsPunishment> selectThisMonth(Long userId);

    List<RewardsPunishment> selectThisYear(Long userId);

    List<RewardsPunishment> selectQuarterOne(Long userId);

    List<RewardsPunishment> selectQuarterTwo(Long userId);

    List<RewardsPunishment> selectQuarterThree(Long userId);

    List<RewardsPunishment> selectQuarterFour(Long userId);

}
