package com.fine.hair.applet.service;

import com.fine.hair.comm.model.Order;
import com.fine.hair.comm.model.User;

/**
 * <p>${Description}</p>
 *
 * @author mouseyCat
 * @date 2020/10/4 15:12
 */
public interface OrderTaskService {

    void updateUserServiceCount(User user, Order order);

}

