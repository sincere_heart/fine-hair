package com.fine.hair.applet.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fine.hair.comm.model.Item;
import com.fine.hair.comm.utils.PageInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>${Description}</p>
 *
 * @author mouseyCat
 * @date 2020/10/26 21:39
 */
@Mapper
public interface ItemMapper extends BaseMapper<Item> {

    List<Item> findItemByBranchId(@Param("itemType") Integer itemType, @Param("branchId") String branchId, @Param(
            "pageInfo") PageInfo<Item> pageInfo);

    /**
     *
     * @return
     */
    List<Item> selectListDistinct();
}
