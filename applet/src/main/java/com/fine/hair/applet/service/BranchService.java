package com.fine.hair.applet.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.fine.hair.comm.dto.BranchSimpleInfoDto;
import com.fine.hair.comm.dto.ServicedStaffDto;
import com.fine.hair.comm.dto.StaffListDto;
import com.fine.hair.comm.dto.StaffMienDto;
import com.fine.hair.comm.model.Branch;
import com.fine.hair.comm.pojo.BranchSimpleInfo;
import com.fine.hair.comm.pojo.StaffSimpleInfo;
import com.fine.hair.comm.utils.PageInfo;
import com.fine.hair.comm.vo.StaffMienVo;

import java.util.List;

/**
 * <p>${Description}</p>
 *
 * @author mouseyCat
 * @date 2020/10/4 15:12
 */
public interface BranchService extends IService<Branch> {

    List<StaffMienVo> staffMien(StaffMienDto dto);

    PageInfo<BranchSimpleInfo> branchSimpleInfo(BranchSimpleInfoDto dto);

    PageInfo<StaffSimpleInfo> staffList(StaffListDto dto);

    PageInfo<StaffSimpleInfo> servicedStaff(ServicedStaffDto dto);
}

