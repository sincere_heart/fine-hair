package com.fine.hair.applet.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.fine.hair.comm.model.Commission;
import com.fine.hair.comm.model.Order;

/**
 * <p>${Description}</p>
 *
 * @author mouseyCat
 * @date 2020/10/29 23:00
 */
public interface CommissionService extends IService<Commission> {

    /**
     * @param order
     */
    void syncCommission(Order order);
}


