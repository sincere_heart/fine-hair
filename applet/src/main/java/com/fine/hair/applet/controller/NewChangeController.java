package com.fine.hair.applet.controller;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.fine.hair.applet.mapper.*;
import com.fine.hair.comm.dto.*;
import com.fine.hair.comm.enums.OrderStatus;
import com.fine.hair.comm.enums.RewardsPunishmentType;
import com.fine.hair.comm.exception.BusinessException;
import com.fine.hair.comm.model.*;
import com.fine.hair.comm.utils.ApiResponse;
import com.fine.hair.comm.vo.EvaluatePageVo;
import com.fine.hair.comm.vo.LatestOrderVO;
import com.fine.hair.comm.vo.PerformanceVo;
import com.fine.hair.comm.vo.StaffWorkInfoVO;
import com.google.common.collect.Lists;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>2月份新增的接口</p>
 *
 * @author mouseyCat
 * @date 2020/10/9 22:59
 */
@Api(tags = {"2月份新增的接口"})
@RestController
@RequestMapping("/new/")
public class NewChangeController {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private CalendarMapper calendarMapper;

    @Autowired
    private BranchMapper branchMapper;

    @Autowired
    private RewardsPunishmentMapper rewardsPunishmentMapper;

    @ApiOperation("员工获取业绩")
    @PostMapping("performance")
    public ApiResponse performance(@RequestBody @Validated PerformanceDto dto) {
        // 下标含义 ： 0现金提成 1销售提成 2客次 3奖励 4惩罚 5合计
        PerformanceVo[] voArray = new PerformanceVo[6];
        for (int i = 0; i < voArray.length; i++) {
            voArray[i] = new PerformanceVo();
        }
        // TODO 本年看年月日   去年前面看季度
        if (dto.getYear().equals(LocalDateTime.now().getYear())) {
            // 查询客次
            // 昨日 今日 本月 本年
            List<Order> orderList1 = orderMapper.selectYesterday(dto.getUserId());
            List<Order> orderList2 = orderMapper.selectThisDay(dto.getUserId());
            List<Order> orderList3 = orderMapper.selectThisMonth(dto.getUserId());
            List<Order> orderList4 = orderMapper.selectThisYear(dto.getUserId());
            PerformanceVo vo = new PerformanceVo();
            vo.setCountOne(BigDecimal.valueOf(orderList1.size()));
            vo.setCountTwo(BigDecimal.valueOf(orderList2.size()));
            vo.setCountThree(BigDecimal.valueOf(orderList3.size()));
            vo.setCountFour(BigDecimal.valueOf(orderList4.size()));
            // 昨日
            List<RewardsPunishment> rewardList1 = rewardsPunishmentMapper.selectYesterday(dto.getUserId());
            // 今日
            List<RewardsPunishment> rewardList2 = rewardsPunishmentMapper.selectThisDay(dto.getUserId());
            // 本月
            List<RewardsPunishment> rewardList3 = rewardsPunishmentMapper.selectThisMonth(dto.getUserId());
            // 本年
            List<RewardsPunishment> rewardList4 = rewardsPunishmentMapper.selectThisYear(dto.getUserId());
            // 设置客次
            voArray[2] = vo;
            if (rewardList1.size() > 0) {
                // 昨日
                // 1奖励 2惩罚 3提成 4现金提成
                List<RewardsPunishment> collect1 =
                        rewardList1.stream().filter(r -> r.getType().equals(RewardsPunishmentType.AWARD.getCode())).collect(Collectors.toList());
                List<RewardsPunishment> collect2 =
                        rewardList1.stream().filter(r -> r.getType().equals(RewardsPunishmentType.PUNISHMENT.getCode())).collect(Collectors.toList());
                List<RewardsPunishment> collect3 =
                        rewardList1.stream().filter(r -> r.getType().equals(RewardsPunishmentType.PROPORTION.getCode())).collect(Collectors.toList());
                List<RewardsPunishment> collect4 =
                        rewardList1.stream().filter(r -> r.getType().equals(RewardsPunishmentType.REAL_MONEY_PROPORTION.getCode())).collect(Collectors.toList());
                voArray[0].setCountOne(collect4.stream().map(RewardsPunishment::getAmount).reduce(BigDecimal.ZERO,BigDecimal::add));
                voArray[1].setCountOne(collect3.stream().map(RewardsPunishment::getAmount).reduce(BigDecimal.ZERO,BigDecimal::add));
                voArray[3].setCountOne(collect1.stream().map(RewardsPunishment::getAmount).reduce(BigDecimal.ZERO,BigDecimal::add));
                voArray[4].setCountOne(collect2.stream().map(RewardsPunishment::getAmount).reduce(BigDecimal.ZERO,BigDecimal::add));
            } else {
                voArray[0].setCountOne(BigDecimal.ZERO);
                voArray[1].setCountOne(BigDecimal.ZERO);
                voArray[3].setCountOne(BigDecimal.ZERO);
                voArray[4].setCountOne(BigDecimal.ZERO);
            }
            if (rewardList2.size() > 0) {
                // 今日
                // 1奖励 2惩罚 3提成 4现金提成
                List<RewardsPunishment> collect1 =
                        rewardList2.stream().filter(r -> r.getType().equals(RewardsPunishmentType.AWARD.getCode())).collect(Collectors.toList());
                List<RewardsPunishment> collect2 =
                        rewardList2.stream().filter(r -> r.getType().equals(RewardsPunishmentType.PUNISHMENT.getCode())).collect(Collectors.toList());
                List<RewardsPunishment> collect3 =
                        rewardList2.stream().filter(r -> r.getType().equals(RewardsPunishmentType.PROPORTION.getCode())).collect(Collectors.toList());
                List<RewardsPunishment> collect4 =
                        rewardList2.stream().filter(r -> r.getType().equals(RewardsPunishmentType.REAL_MONEY_PROPORTION.getCode())).collect(Collectors.toList());
                voArray[0].setCountTwo(collect4.stream().map(RewardsPunishment::getAmount).reduce(BigDecimal.ZERO,BigDecimal::add));
                voArray[1].setCountTwo(collect3.stream().map(RewardsPunishment::getAmount).reduce(BigDecimal.ZERO,BigDecimal::add));
                voArray[3].setCountTwo(collect1.stream().map(RewardsPunishment::getAmount).reduce(BigDecimal.ZERO,BigDecimal::add));
                voArray[4].setCountTwo(collect2.stream().map(RewardsPunishment::getAmount).reduce(BigDecimal.ZERO,BigDecimal::add));
            } else {
                voArray[0].setCountTwo(BigDecimal.ZERO);
                voArray[1].setCountTwo(BigDecimal.ZERO);
                voArray[3].setCountTwo(BigDecimal.ZERO);
                voArray[4].setCountTwo(BigDecimal.ZERO);
            }
            if (rewardList3.size() > 0) {
                // 本月
                // 1奖励 2惩罚 3提成 4现金提成
                List<RewardsPunishment> collect1 =
                        rewardList3.stream().filter(r -> r.getType().equals(RewardsPunishmentType.AWARD.getCode())).collect(Collectors.toList());
                List<RewardsPunishment> collect2 =
                        rewardList3.stream().filter(r -> r.getType().equals(RewardsPunishmentType.PUNISHMENT.getCode())).collect(Collectors.toList());
                List<RewardsPunishment> collect3 =
                        rewardList3.stream().filter(r -> r.getType().equals(RewardsPunishmentType.PROPORTION.getCode())).collect(Collectors.toList());
                List<RewardsPunishment> collect4 =
                        rewardList3.stream().filter(r -> r.getType().equals(RewardsPunishmentType.REAL_MONEY_PROPORTION.getCode())).collect(Collectors.toList());
                voArray[0].setCountThree(collect4.stream().map(RewardsPunishment::getAmount).reduce(BigDecimal.ZERO,BigDecimal::add));
                voArray[1].setCountThree(collect3.stream().map(RewardsPunishment::getAmount).reduce(BigDecimal.ZERO,BigDecimal::add));
                voArray[3].setCountThree(collect1.stream().map(RewardsPunishment::getAmount).reduce(BigDecimal.ZERO,BigDecimal::add));
                voArray[4].setCountThree(collect2.stream().map(RewardsPunishment::getAmount).reduce(BigDecimal.ZERO,BigDecimal::add));
            } else {
                voArray[0].setCountThree(BigDecimal.ZERO);
                voArray[1].setCountThree(BigDecimal.ZERO);
                voArray[3].setCountThree(BigDecimal.ZERO);
                voArray[4].setCountThree(BigDecimal.ZERO);
            }
            if (rewardList4.size() > 0) {
                // 本年
                // 1奖励 2惩罚 3提成 4现金提成
                List<RewardsPunishment> collect1 =
                        rewardList4.stream().filter(r -> r.getType().equals(RewardsPunishmentType.AWARD.getCode())).collect(Collectors.toList());
                List<RewardsPunishment> collect2 =
                        rewardList4.stream().filter(r -> r.getType().equals(RewardsPunishmentType.PUNISHMENT.getCode())).collect(Collectors.toList());
                List<RewardsPunishment> collect3 =
                        rewardList4.stream().filter(r -> r.getType().equals(RewardsPunishmentType.PROPORTION.getCode())).collect(Collectors.toList());
                List<RewardsPunishment> collect4 =
                        rewardList4.stream().filter(r -> r.getType().equals(RewardsPunishmentType.REAL_MONEY_PROPORTION.getCode())).collect(Collectors.toList());
                voArray[0].setCountFour(collect4.stream().map(RewardsPunishment::getAmount).reduce(BigDecimal.ZERO,BigDecimal::add));
                voArray[1].setCountFour(collect3.stream().map(RewardsPunishment::getAmount).reduce(BigDecimal.ZERO,BigDecimal::add));
                voArray[3].setCountFour(collect1.stream().map(RewardsPunishment::getAmount).reduce(BigDecimal.ZERO,BigDecimal::add));
                voArray[4].setCountFour(collect2.stream().map(RewardsPunishment::getAmount).reduce(BigDecimal.ZERO,BigDecimal::add));
            } else {
                voArray[0].setCountFour(BigDecimal.ZERO);
                voArray[1].setCountFour(BigDecimal.ZERO);
                voArray[3].setCountFour(BigDecimal.ZERO);
                voArray[4].setCountFour(BigDecimal.ZERO);
            }
            voArray[5].setCountOne(voArray[0].getCountOne().add(voArray[1].getCountOne()).add(voArray[2].getCountOne().add(voArray[3].getCountOne())).add(voArray[4].getCountOne()));
            voArray[5].setCountTwo(voArray[0].getCountTwo().add(voArray[1].getCountTwo()).add(voArray[2].getCountTwo().add(voArray[3].getCountTwo())).add(voArray[4].getCountTwo()));
            voArray[5].setCountThree(voArray[0].getCountThree().add(voArray[1].getCountThree()).add(voArray[2].getCountThree().add(voArray[3].getCountThree())).add(voArray[4].getCountThree()));
            voArray[5].setCountFour(voArray[0].getCountFour().add(voArray[1].getCountFour()).add(voArray[2].getCountFour().add(voArray[3].getCountFour())).add(voArray[4].getCountFour()));
        } else {
            // 查季度
            // 查询客次
            // 1 2 3 4季度
            List<Order> orderList1 = orderMapper.selectQuarterOne(dto.getUserId());
            List<Order> orderList2 = orderMapper.selectQuarterTwo(dto.getUserId());
            List<Order> orderList3 = orderMapper.selectQuarterThree(dto.getUserId());
            List<Order> orderList4 = orderMapper.selectQuarterFour(dto.getUserId());
            PerformanceVo vo = new PerformanceVo();
            vo.setCountOne(BigDecimal.valueOf(orderList1.size()));
            vo.setCountTwo(BigDecimal.valueOf(orderList2.size()));
            vo.setCountThree(BigDecimal.valueOf(orderList3.size()));
            vo.setCountFour(BigDecimal.valueOf(orderList4.size()));
            // 1季度
            List<RewardsPunishment> rewardList1 = rewardsPunishmentMapper.selectQuarterOne(dto.getUserId());
            // 2季度
            List<RewardsPunishment> rewardList2 = rewardsPunishmentMapper.selectQuarterTwo(dto.getUserId());
            // 3季度
            List<RewardsPunishment> rewardList3 = rewardsPunishmentMapper.selectQuarterThree(dto.getUserId());
            // 4季度
            List<RewardsPunishment> rewardList4 = rewardsPunishmentMapper.selectQuarterFour(dto.getUserId());
            // 设置客次
            voArray[2] = vo;
            if (rewardList1.size() > 0) {
                // 昨日
                // 1奖励 2惩罚 3提成 4现金提成
                List<RewardsPunishment> collect1 =
                        rewardList1.stream().filter(r -> r.getType().equals(RewardsPunishmentType.AWARD.getCode())).collect(Collectors.toList());
                List<RewardsPunishment> collect2 =
                        rewardList1.stream().filter(r -> r.getType().equals(RewardsPunishmentType.PUNISHMENT.getCode())).collect(Collectors.toList());
                List<RewardsPunishment> collect3 =
                        rewardList1.stream().filter(r -> r.getType().equals(RewardsPunishmentType.PROPORTION.getCode())).collect(Collectors.toList());
                List<RewardsPunishment> collect4 =
                        rewardList1.stream().filter(r -> r.getType().equals(RewardsPunishmentType.REAL_MONEY_PROPORTION.getCode())).collect(Collectors.toList());
                voArray[0].setCountOne(collect4.stream().map(RewardsPunishment::getAmount).reduce(BigDecimal.ZERO,BigDecimal::add));
                voArray[1].setCountOne(collect3.stream().map(RewardsPunishment::getAmount).reduce(BigDecimal.ZERO,BigDecimal::add));
                voArray[3].setCountOne(collect1.stream().map(RewardsPunishment::getAmount).reduce(BigDecimal.ZERO,BigDecimal::add));
                voArray[4].setCountOne(collect2.stream().map(RewardsPunishment::getAmount).reduce(BigDecimal.ZERO,BigDecimal::add));
            } else {
                voArray[0].setCountOne(BigDecimal.ZERO);
                voArray[1].setCountOne(BigDecimal.ZERO);
                voArray[3].setCountOne(BigDecimal.ZERO);
                voArray[4].setCountOne(BigDecimal.ZERO);
            }
            if (rewardList2.size() > 0) {
                // 今日
                // 1奖励 2惩罚 3提成 4现金提成
                List<RewardsPunishment> collect1 =
                        rewardList2.stream().filter(r -> r.getType().equals(RewardsPunishmentType.AWARD.getCode())).collect(Collectors.toList());
                List<RewardsPunishment> collect2 =
                        rewardList2.stream().filter(r -> r.getType().equals(RewardsPunishmentType.PUNISHMENT.getCode())).collect(Collectors.toList());
                List<RewardsPunishment> collect3 =
                        rewardList2.stream().filter(r -> r.getType().equals(RewardsPunishmentType.PROPORTION.getCode())).collect(Collectors.toList());
                List<RewardsPunishment> collect4 =
                        rewardList2.stream().filter(r -> r.getType().equals(RewardsPunishmentType.REAL_MONEY_PROPORTION.getCode())).collect(Collectors.toList());
                voArray[0].setCountTwo(collect4.stream().map(RewardsPunishment::getAmount).reduce(BigDecimal.ZERO,BigDecimal::add));
                voArray[1].setCountTwo(collect3.stream().map(RewardsPunishment::getAmount).reduce(BigDecimal.ZERO,BigDecimal::add));
                voArray[3].setCountTwo(collect1.stream().map(RewardsPunishment::getAmount).reduce(BigDecimal.ZERO,BigDecimal::add));
                voArray[4].setCountTwo(collect2.stream().map(RewardsPunishment::getAmount).reduce(BigDecimal.ZERO,BigDecimal::add));
            } else {
                voArray[0].setCountTwo(BigDecimal.ZERO);
                voArray[1].setCountTwo(BigDecimal.ZERO);
                voArray[3].setCountTwo(BigDecimal.ZERO);
                voArray[4].setCountTwo(BigDecimal.ZERO);
            }
            if (rewardList3.size() > 0) {
                // 本月
                // 1奖励 2惩罚 3提成 4现金提成
                List<RewardsPunishment> collect1 =
                        rewardList3.stream().filter(r -> r.getType().equals(RewardsPunishmentType.AWARD.getCode())).collect(Collectors.toList());
                List<RewardsPunishment> collect2 =
                        rewardList3.stream().filter(r -> r.getType().equals(RewardsPunishmentType.PUNISHMENT.getCode())).collect(Collectors.toList());
                List<RewardsPunishment> collect3 =
                        rewardList3.stream().filter(r -> r.getType().equals(RewardsPunishmentType.PROPORTION.getCode())).collect(Collectors.toList());
                List<RewardsPunishment> collect4 =
                        rewardList3.stream().filter(r -> r.getType().equals(RewardsPunishmentType.REAL_MONEY_PROPORTION.getCode())).collect(Collectors.toList());
                voArray[0].setCountThree(collect4.stream().map(RewardsPunishment::getAmount).reduce(BigDecimal.ZERO,BigDecimal::add));
                voArray[1].setCountThree(collect3.stream().map(RewardsPunishment::getAmount).reduce(BigDecimal.ZERO,BigDecimal::add));
                voArray[3].setCountThree(collect1.stream().map(RewardsPunishment::getAmount).reduce(BigDecimal.ZERO,BigDecimal::add));
                voArray[4].setCountThree(collect2.stream().map(RewardsPunishment::getAmount).reduce(BigDecimal.ZERO,BigDecimal::add));
            } else {
                voArray[0].setCountThree(BigDecimal.ZERO);
                voArray[1].setCountThree(BigDecimal.ZERO);
                voArray[3].setCountThree(BigDecimal.ZERO);
                voArray[4].setCountThree(BigDecimal.ZERO);
            }
            if (rewardList4.size() > 0) {
                // 本年
                // 1奖励 2惩罚 3提成 4现金提成
                List<RewardsPunishment> collect1 =
                        rewardList4.stream().filter(r -> r.getType().equals(RewardsPunishmentType.AWARD.getCode())).collect(Collectors.toList());
                List<RewardsPunishment> collect2 =
                        rewardList4.stream().filter(r -> r.getType().equals(RewardsPunishmentType.PUNISHMENT.getCode())).collect(Collectors.toList());
                List<RewardsPunishment> collect3 =
                        rewardList4.stream().filter(r -> r.getType().equals(RewardsPunishmentType.PROPORTION.getCode())).collect(Collectors.toList());
                List<RewardsPunishment> collect4 =
                        rewardList4.stream().filter(r -> r.getType().equals(RewardsPunishmentType.REAL_MONEY_PROPORTION.getCode())).collect(Collectors.toList());
                voArray[0].setCountFour(collect4.stream().map(RewardsPunishment::getAmount).reduce(BigDecimal.ZERO,BigDecimal::add));
                voArray[1].setCountFour(collect3.stream().map(RewardsPunishment::getAmount).reduce(BigDecimal.ZERO,BigDecimal::add));
                voArray[3].setCountFour(collect1.stream().map(RewardsPunishment::getAmount).reduce(BigDecimal.ZERO,BigDecimal::add));
                voArray[4].setCountFour(collect2.stream().map(RewardsPunishment::getAmount).reduce(BigDecimal.ZERO,BigDecimal::add));
            } else {
                voArray[0].setCountFour(BigDecimal.ZERO);
                voArray[1].setCountFour(BigDecimal.ZERO);
                voArray[3].setCountFour(BigDecimal.ZERO);
                voArray[4].setCountFour(BigDecimal.ZERO);
            }
            voArray[5].setCountOne(voArray[0].getCountOne().add(voArray[1].getCountOne()).add(voArray[2].getCountOne().add(voArray[3].getCountOne())).add(voArray[4].getCountOne()));
            voArray[5].setCountTwo(voArray[0].getCountTwo().add(voArray[1].getCountTwo()).add(voArray[2].getCountTwo().add(voArray[3].getCountTwo())).add(voArray[4].getCountTwo()));
            voArray[5].setCountThree(voArray[0].getCountThree().add(voArray[1].getCountThree()).add(voArray[2].getCountThree().add(voArray[3].getCountThree())).add(voArray[4].getCountThree()));
            voArray[5].setCountFour(voArray[0].getCountFour().add(voArray[1].getCountFour()).add(voArray[2].getCountFour().add(voArray[3].getCountFour())).add(voArray[4].getCountFour()));
        }
        for (int i = 0; i < voArray.length; i++) {
            voArray[i].setCountFive(voArray[i].getCountOne().add(voArray[i].getCountTwo()).add(voArray[i].getCountThree()).add(voArray[i].getCountFour()));
        }

    return ApiResponse.ofSuccess(voArray);

    }

    @ApiOperation("获取当前员工的上下班状态")
    @PostMapping("workStatus")
    public ApiResponse name(@RequestBody   @Validated WorkStatusDto dto) {
        User user = userMapper.selectById(dto.getUserId());
        if (ObjectUtil.isNull(user)) {
            throw new BusinessException("当前用户不存在");
        }
        Integer onWork = user.getOnWork();
        if (onWork.equals(1)) {
            // 1 上班
            //List<Order> orders = orderMapper.selectOrderByDate(DateUtil.formatDate(new Date()));
            //if (orders.size() > 0) {
            //    throw new BusinessException("下班失败，今天有未完成订单");
            //}
            return ApiResponse.ofSuccess(true);
        } else {
            // 0，2 下班
            return ApiResponse.ofSuccess(false);
        }
    }

    @ApiOperation("上下班按钮")
    @PostMapping("workSwitch")
    public ApiResponse name(@RequestBody  @Validated WorkSwitchDto dto) {
        User user = userMapper.selectById(dto.getUserId());
        if (ObjectUtil.isNull(user)) {
            throw new BusinessException("当前用户不存在");
        }
        List<Order> orders = orderMapper.selectListByUserIdAndDate2(DateUtil.formatDate(new Date()),dto.getUserId());
        if (orders.size() > 0) {
            throw new BusinessException("下班失败，今天有未完成订单");
        }
        if (dto.getStatus() == 1) {
            //上班
            user.setOnWork(1);
        } else {
            //下班
            user.setOnWork(2);
        }
        userMapper.updateById(user);
        return ApiResponse.ofSuccess();
    }



    @ApiOperation("获取当前用户所属周期")
    @PostMapping("getCurrentUserCalendar")
    public ApiResponse name(@RequestBody  @Validated CurrentUserCalendarDto dto) {
        User user = userMapper.selectById(dto.getUserId());
        if (ObjectUtil.isNull(user)) {
            throw new BusinessException("当前用户不存在");
        }
        if (ObjectUtil.isNull(user.getLabelType())) {
            return ApiResponse.ofSuccess("暂无周期安排");
        } else {
            Calendar calendar = calendarMapper.selectById(user.getLabelType());
            if (ObjectUtil.isNull(calendar)) {
                throw new BusinessException("当前周期配置不存在");
            }
            return ApiResponse.ofSuccess(calendar.getCount() + "天一次");
        }
    }

    @ApiOperation(value = "将订单改为《已开始》状态",
            notes = "订单为未开始时，显示【开始订单】按钮；订单为已开始时，显示【完成订单】按钮；")
    @PostMapping("startOrder")
    public ApiResponse name(@RequestBody  @Validated StartOrderDto dto) {
        Order order = orderMapper.selectById(dto.getOrderId());
        if (ObjectUtil.isNull(order)) {
            throw new BusinessException("当前订单不存在");
        }
        order.setOrderStatus(OrderStatus.STARTING.getCode().toString());
        orderMapper.updateById(order);
        return ApiResponse.ofSuccess();
    }

    @ApiOperation("用户的最近一次养护记录")
    @PostMapping("latestOrder")
    public ApiResponse<List<LatestOrderVO>> name(@RequestBody @Validated LatestOrderDto dto) {
        User user = userMapper.selectById(dto.getUserId());
        if (ObjectUtil.isNull(user)) {
            throw new BusinessException("当前用户不存在");
        }
        List<Order> orders = orderMapper.selectList(new LambdaQueryWrapper<Order>().eq(Order::getUserId,
                user.getId()).orderByDesc(Order::getCreateTime));
        LatestOrderVO latestOrderVO = new LatestOrderVO();
        ArrayList<LatestOrderVO> latestOrderVOList = Lists.newArrayList();
        if (orders.size() > 0) {
            Order order = orders.get(orders.size() - 1);
            latestOrderVO.setCreateTime(DateUtil.formatDateTime(order.getCreateTime()));
            latestOrderVO.setExecuteTime(DateUtil.formatDateTime(order.getExecuteTime()));
            latestOrderVO.setItemImage(order.getItemImage());
            latestOrderVO.setItemPrice(order.getItemPrice().toString());
            latestOrderVO.setItemTitle(order.getItemTitle());
            latestOrderVOList.add(latestOrderVO);
            return ApiResponse.ofSuccess(latestOrderVOList);
        } else {
            return ApiResponse.ofSuccess(latestOrderVOList);
        }
    }
    @ApiOperation(value = "从日历分组里面删除用户 ",notes = "9-a")
    @PostMapping("delUserOnCalendar")
    public ApiResponse name(@RequestBody @Validated DelUserOnCalendarDto dto) {
        User user = userMapper.selectById(dto.getUserId());
        if (ObjectUtil.isNull(user)) {
            throw new BusinessException("当前用户不存在");
        }
        user.setLabelType(0L);
        userMapper.updateById(user);
        return ApiResponse.ofSuccess();
    }

    @ApiOperation(value = "将客户调整到其它周期", notes = "9-e")
    @PostMapping("changeUserCalendar")
    public ApiResponse name(@RequestBody @Validated ChangeUserCalendarDto dto) {
        User user = userMapper.selectById(dto.getUserId());
        if (ObjectUtil.isNull(user)) {
            throw new BusinessException("当前用户不存在");
        }
        Calendar calendar = calendarMapper.selectById(dto.getCalendarId());
        if (ObjectUtil.isNull(calendar)) {
            throw new BusinessException("当前周期不存在");
        }
        user.setLabelType(Long.valueOf(dto.getCalendarId()));
        userMapper.updateById(user);
        return ApiResponse.ofSuccess();
    }


    @ApiOperation("根据名字搜索门店列表")
    @PostMapping("searchBranchByBranchName")
    public ApiResponse<List<Branch>> name(@RequestBody @Validated SearchBranchByBranchNameDto dto) {
        List<Branch> list = branchMapper.searchBranchByBranchName(dto.getBranchName());
        return ApiResponse.ofSuccess(list);
    }









    @ApiOperation(value = "当前员工所属门店、当前挂店、工作状态",notes = "1-b")
    @PostMapping("staffWorkInfo")
    public ApiResponse<StaffWorkInfoVO> name(@RequestBody @Validated StaffWorkInfoDto dto) {
        User user = userMapper.selectById(dto.getUserId());
        StaffWorkInfoVO staffWorkInfoVO = new StaffWorkInfoVO();
        if (ObjectUtil.isNull(user)) {
            throw new BusinessException("当前用户不存在");
        }
        if (ObjectUtil.isNull(user.getBranchId())) {
            throw new BusinessException("当前用户不存在于任何门店");
        }
        Branch branch = branchMapper.selectById(user.getBranchId());
        if (ObjectUtil.isNull(branch)) {
            throw new BusinessException("当前门店不存在");
        }
        staffWorkInfoVO.setSelfBranchName(branch.getTitle());
        if (StrUtil.isNotBlank(user.getPutBranchId())) {
            Branch putBranch = branchMapper.selectById(user.getPutBranchId());
            staffWorkInfoVO.setCurrentPutBranchName(putBranch.getTitle());
        }
        if (user.getOnWork().equals(1)) {
            staffWorkInfoVO.setCurrentWorkStatus("接单");
        } else {
            staffWorkInfoVO.setCurrentWorkStatus("暂停接单");
        }
        return ApiResponse.ofSuccess(staffWorkInfoVO);
    }

    @Autowired
    private EvaluateMapper evaluateMapper;

    @ApiOperation("评价页面信息")
    @PostMapping("evaluatePage")
    public ApiResponse<EvaluatePageVo> evaluatePage(@RequestBody EvaluatePageDto dto) {
        List<Evaluate> evaluates = evaluateMapper.selectList(new LambdaQueryWrapper<Evaluate>().eq(Evaluate::getTechnicianId, dto.getUserId()));
        User user = userMapper.selectById(dto.getUserId());
        if (ObjectUtil.isNull(user)) {
            throw new BusinessException("当前用户不存在");
        }
        EvaluatePageVo vo = new EvaluatePageVo();
        vo.setAvatar(user.getHeadPhoto());
        vo.setName(user.getNickName());
        vo.setPhone(user.getPhoneNumber());
        if (evaluates.size() > 0) {
            BigDecimal avg = BigDecimal.ZERO;
            for (Evaluate evaluate : evaluates) {
                avg = avg.add(evaluate.getScore());
            }
            avg = avg.divide(new BigDecimal(evaluates.size()), BigDecimal.ROUND_HALF_UP);
            vo.setScore(avg);
        } else {
            vo.setScore(BigDecimal.ZERO);
        }
        return ApiResponse.ofSuccess(vo);
    }

    //@ApiOperation("")
    //@PostMapping("")
    //public ApiResponse name(@RequestBody Dto dto) {
    //
    //}
    //@ApiOperation("")
    //@PostMapping("")
    //public ApiResponse name(@RequestBody Dto dto) {
    //
    //}
    //@ApiOperation("")
    //@PostMapping("")
    //public ApiResponse name(@RequestBody Dto dto) {
    //
    //}

}
