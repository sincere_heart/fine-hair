package com.fine.hair.applet.controller;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.fine.hair.applet.mapper.BranchMapper;
import com.fine.hair.applet.mapper.EvaluateMapper;
import com.fine.hair.applet.mapper.OrderMapper;
import com.fine.hair.applet.mapper.UserMapper;
import com.fine.hair.comm.dto.NewStaffOrderListDTO;
import com.fine.hair.comm.dto.SearchCalendarDto;
import com.fine.hair.comm.utils.ApiResponse;
import com.fine.hair.comm.utils.DateUtils;
import com.fine.hair.comm.utils.PageInfo;
import com.fine.hair.comm.vo.SearchCalendarVO;
import com.fine.hair.comm.vo.StaffOrderVo;
import com.fine.hair.comm.model.Branch;
import com.fine.hair.comm.model.Evaluate;
import com.fine.hair.comm.model.Order;
import com.fine.hair.comm.model.User;
import com.google.common.collect.Lists;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * <p></p>
 *
 * @author mouseyCat
 * @date 2021/1/21 20:34
 */
@Slf4j
@Api(tags = {"新的日历相关接口"})
@RestController
@RequestMapping("/calendar/")
public class NewCalendarController {

    @Resource
    private UserMapper userMapper;

    @Resource
    private OrderMapper orderMapper;

    @Autowired
    private BranchMapper branchMapper;

    @PostMapping("search-calendar")
    @ApiOperation("1-查询日历")
    public ApiResponse<List<SearchCalendarVO>> searchCalendar(@RequestBody SearchCalendarDto dto) {
        dto.setDate(dto.getDate() + "-01");
        List<SearchCalendarVO> listSearchCalendarVO = Lists.newArrayList();
        List<Order> orderList;

        if (ObjectUtil.isNull(dto.getUserId())) {
            orderList = orderMapper.selectUserOrderByMonth(dto);
        } else {
            orderList = orderMapper.selectUserOrderByMonth2(dto);
        }
        for (Order order : orderList) {
            SearchCalendarVO searchCalendarVO = new SearchCalendarVO();
            searchCalendarVO.setDate(order.getExecuteTime().getDate());
            searchCalendarVO.setMonth(order.getExecuteTime().getMonth() + 1);
            searchCalendarVO.setYear(1900 + order.getExecuteTime().getYear());
            searchCalendarVO.setOrderStatus(Integer.parseInt(order.getOrderStatus()));
            listSearchCalendarVO.add(searchCalendarVO);
        }
        return ApiResponse.ofSuccess(listSearchCalendarVO);
    }

    @ApiOperation("2-查看日历上某天的订单列表")
    @PostMapping("staff-order-list")
    public ApiResponse<PageInfo<StaffOrderVo>> staffOrder(@RequestBody NewStaffOrderListDTO dto) {
        PageInfo<StaffOrderVo> pageInfo = new PageInfo<>(dto.getPageCurr(), dto.getPageSize());
        List<Order> orderList = orderMapper.selectListByUserIdAndDate3(dto.getOrderStatus(),dto.getDate(),dto.getUserId(), pageInfo);
        log.info(">>>>>>>>>>>>>>>> {}", orderList);
        List<StaffOrderVo> vos = Lists.newArrayList();
        for (Order record : orderList) {
            User staff = userMapper.selectById(record.getTechnicianId());
            User user = userMapper.selectById(record.getUserId());
            Branch branch = branchMapper.selectById(record.getBranchId());
            log.info(">>>>>>>>>>>>>>>user {}", user);
            StaffOrderVo vo = new StaffOrderVo();
            vo.setCustomerName(user.getNickName());
            vo.setCustomerPhone(user.getPhoneNumber());
            vo.setOrderStatus(Integer.valueOf(record.getOrderStatus()));
            vo.setTechnicianAvatar(staff.getHeadPhoto());
            vo.setTechnicianName(staff.getNickName());
            vo.setTechnicianId(staff.getId());
            vo.setTechnicianPhone(staff.getPhoneNumber());
            vo.setItemTitle(record.getItemTitle());
            vo.setTitle(branch.getTitle());
            vo.setCustomerId(user.getId());
            vo.setOrderId(record.getId());
            SimpleDateFormat df = new SimpleDateFormat("yyyy.MM.dd HH:mm");
            String format = df.format(record.getExecuteTime());
            String week = DateUtils.calculateWeekDay(Integer.parseInt(format.split(" ")[0].split("\\.")[0]), Integer.parseInt(format.split(" ")[0].split("\\.")[1]), Integer.parseInt(format.split(" ")[0].split("\\.")[2]));
            String dayTime = DateUtils.dayTime(Integer.parseInt(format.split(" ")[1].split(":")[0]));
            vo.setDate(format.split(" ")[0]);
            vo.setTime(week + dayTime + format.split(" ")[1]);
            Evaluate evaluate = evaluateMapper.selectOne(new LambdaQueryWrapper<Evaluate>().eq(Evaluate::getOrderId,
                    record.getId()));
            if (ObjectUtil.isNull(evaluate)) {
                vo.setIsComment(false);
            } else {
                vo.setIsComment(true);
            }
            vos.add(vo);
        }
        pageInfo.setRecords(vos);
        return ApiResponse.ofSuccess(pageInfo);
    }

    @Autowired
    private EvaluateMapper evaluateMapper;



}
