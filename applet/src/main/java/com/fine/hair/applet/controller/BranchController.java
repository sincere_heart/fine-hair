package com.fine.hair.applet.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fine.hair.applet.mapper.BannerMapper;
import com.fine.hair.applet.mapper.ItemMapper;
import com.fine.hair.applet.service.BranchService;
import com.fine.hair.comm.dto.BranchSimpleInfoDto;
import com.fine.hair.comm.dto.FrontItemListDto;
import com.fine.hair.comm.enums.StatusType;
import com.fine.hair.comm.pojo.BranchSimpleInfo;
import com.fine.hair.comm.utils.ApiResponse;
import com.fine.hair.comm.utils.PageInfo;
import com.fine.hair.comm.vo.ItemListVo;
import com.fine.hair.comm.model.Banner;
import com.fine.hair.comm.model.Branch;
import com.fine.hair.comm.model.Item;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * <p>门店相关接口</p>
 *
 * @author mouseyCat
 * @date 2020/10/9 22:59
 */
@Api(tags = {"门店相关接口"})
@RestController
@RequestMapping("/branch/")
public class BranchController {

    @Resource
    private BannerMapper bannerMapper;

    @Resource
    private BranchService branchService;

    @Resource
    private ItemMapper itemMapper;

    @ApiOperation("首页轮播图")
    @GetMapping("banner")
    public ApiResponse<List<Banner>> banner() {
        return ApiResponse.ofSuccess(bannerMapper.selectList(new LambdaQueryWrapper<Banner>().eq(Banner::getStatus, StatusType.NORMAL.getCode()).orderByAsc(Banner::getPriority)));
    }

    @ApiOperation("获取门店简单信息列表")
    @PostMapping("branch-simple-info")
    public ApiResponse<PageInfo<BranchSimpleInfo>> branchSimpleInfo(@RequestBody BranchSimpleInfoDto dto) {
        return ApiResponse.ofSuccess(branchService.branchSimpleInfo(dto));
    }

    @ApiOperation("挂店页面的门店列表")
    @GetMapping("put-branch-list/{staffId}")
    public ApiResponse<List<Branch>> putBranchList(@PathVariable Long staffId) {
        // User user = userMapper.selectById(staffId);
        List<Branch> list = branchService.list(new LambdaQueryWrapper<Branch>().select(Branch::getId,
                Branch::getAddress, Branch::getImages, Branch::getTitle));
        for (Branch branch : list) {
            if (branch.getImages().split(",").length > 0) {
                branch.setImages(branch.getImages().split(",")[0]);
            }
        }
        // 移除用户自己的门店，不能自己挂自己店
        //list.removeIf(b -> b.getId().equals(user.getBranchId()));
        return ApiResponse.ofSuccess(list);
    }

    @ApiOperation(value = "项目列表",notes = "只返回当前门店库存大于0的项目")
    @PostMapping("item-list")
    public ApiResponse<ItemListVo> itemList(@RequestBody FrontItemListDto dto) {
        PageInfo<Item> pageInfo = new PageInfo<>(dto.getPageCurr(), dto.getPageSize());
//        List<Item> list = itemMapper.findItemByBranchId(dto.getMode(), dto.getBranchId(), pageInfo);
        IPage<Item> itemPageInfo =  itemMapper.selectPage(pageInfo,new LambdaQueryWrapper<Item>().eq(Item::getType,dto.getMode()).eq(Item::getBranchId,dto.getBranchId()).ne(Item::getType, 6));
        pageInfo.setRecords(itemPageInfo.getRecords());
        ItemListVo vo = new ItemListVo();
        vo.setList(pageInfo);
        return ApiResponse.ofSuccess(vo);
    }

    @ApiOperation("项目类型")
    @GetMapping("item-type")
    public ApiResponse<List<Item>> itemType() {
        List<Item> itemList = itemMapper.selectListDistinct();

        List<Item> itemListDistincted = itemList
                .stream()
                .filter(distinctByKey(Item::getType))
                .collect(Collectors.toList());
        return ApiResponse.ofSuccess(itemListDistincted);
    }
    private <T> Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor) {
        Map<Object, Boolean> concurrentHashMap = new ConcurrentHashMap<>();
        return t -> concurrentHashMap.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }


}
