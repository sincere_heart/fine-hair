package com.fine.hair.applet.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fine.hair.comm.dto.wx.WxLoginDto;
import com.fine.hair.comm.model.User;
import com.fine.hair.comm.utils.ApiResponse;
import com.fine.hair.comm.vo.WxLiteUserInfo;
import com.fine.hair.comm.vo.WxLoginResultVo;

/**
 * <p>${Description}</p>
 *
 * @author mouseyCat
 * @date 2020/9/7 20:44
 */
public interface UserService extends IService<User> {


    ApiResponse getOpenId(String code);

    WxLoginResultVo wxLogin(WxLiteUserInfo wxLiteUserInfo, WxLoginDto param);

    WxLoginResultVo wxLiteRegister(String purePhoneNumber, String openId, String unionId, String nickname,
                                   String headIconUrl, Integer gender);
}




